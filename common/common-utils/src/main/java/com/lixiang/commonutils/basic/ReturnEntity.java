package com.lixiang.commonutils.basic;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.lixiang.commonutils.enums.ResultCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * Api统一返回结果
 *
 * @param <T> 泛型类
 */
@ApiModel("Api返回结果")
@Data
public final class ReturnEntity<T> implements Serializable {

    @ApiModelProperty(value = "时间戳")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime ts = LocalDateTime.now();

    @ApiModelProperty(value = "是否成功")
    private Boolean success;

    @ApiModelProperty(value = "状态码")
    private Integer code = 0;

    @ApiModelProperty(value = "说明")
    private String desc = "成功";

    @ApiModelProperty(value = "返回的数据")
    private Map<String, T> data = new HashMap<>();

    /**
     * 无参构造
     */
    private ReturnEntity() {
    }

    /**
     * 操作成功
     *
     * @return {@link ReturnEntity}
     */
    public static ReturnEntity success() {
        ReturnEntity returnEntity = new ReturnEntity();
        returnEntity.setSuccess(true);
        returnEntity.code = ResultCode.SUCCESS;
        returnEntity.desc = "成功";
        returnEntity.ts = LocalDateTime.now();
        returnEntity.data = new HashMap<>();
        return returnEntity;
    }

    /**
     * 操作失败
     *
     * @return {@link ReturnEntity}
     */
    public static ReturnEntity fail() {
        ReturnEntity returnEntity = new ReturnEntity();
        returnEntity.setSuccess(false);
        returnEntity.code = ResultCode.ERROR;
        returnEntity.desc = "失败";
        returnEntity.ts = LocalDateTime.now();
        returnEntity.data = new HashMap<>();
        return returnEntity;
    }

    /**
     * 设置返回的说明
     * @param desc 说明描述
     * @return {@link ReturnEntity}
     */
    public ReturnEntity desc(String desc){
        this.setDesc(desc);
        return this;
    }

    /**
     * 设置返回的code
     * @param code code
     * @return {@link ReturnEntity}
     */
    public ReturnEntity code(Integer code){
        this.setCode(code);
        return this;
    }

    /**
     * 设置返回的 Date
     *
     * @param key 键
     * @param value 值
     * @return {@link ReturnEntity}
     */
    public ReturnEntity data(String key, T value){
        this.data.put(key, value);
        return this;
    }

    /**
     * 设置返回的 Date
     *
     * @param map Map集合
     * @return {@link ReturnEntity}
     */
    public ReturnEntity data(Map<String, T> map){
        this.data = map;
        return this;
    }

}

