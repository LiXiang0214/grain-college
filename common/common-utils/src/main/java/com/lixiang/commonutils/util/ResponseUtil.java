package com.lixiang.commonutils.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lixiang.commonutils.basic.ReturnEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Response工具类
 */
public class ResponseUtil {

    /**
     * 返回
     * @param response HttpServletResponse
     * @param returnEntity 统一返回格式
     */
    public static void out(HttpServletResponse response, ReturnEntity returnEntity) {
        ObjectMapper mapper = new ObjectMapper();
        response.setStatus(HttpStatus.OK.value());
        response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
        try {
            mapper.writeValue(response.getWriter(), returnEntity);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
