package com.lixiang.commonutils.enums;

/**
 * 统一返回码
 */
public interface ResultCode {

    Integer SUCCESS = 20000; //成功

    Integer ERROR = 20001; //失败

}
