package com.lixiang.commonutils.enums;

/**
 * @Auther: lixiang
 * @Date: 2020/8/27
 */
public enum DelEnums {
	Normal(0, "未删除"),
	Delete(1, "删除");

	private final Integer value;
	private final String desc;

	DelEnums(Integer value, String desc) {
		this.value = value;
		this.desc = desc;
	}

    /**
     * 获取描述
     *
     * @param value 值
     * @return desc
     */
    public static String getDesc(Integer value) {
        for (DelEnums enumValue : values()) {
            if (enumValue.value().equals(value)) {
                return enumValue.desc();
            }
        }
        return null;
    }

	/**
	 * 获取值
	 *
	 * @return value
	 */
	public Integer value() {
		return value;
	}

    /**
     * 获取描述
     *
     * @return desc
     */
    public String desc() {
        return desc;
    }

}
