package com.lixiang.serurity.security;

import io.jsonwebtoken.CompressionCodecs;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * token管理
 */
@Component
public class TokenManager {

    private long tokenExpiration = 24 * 60 * 60 * 1000; // token过期时间
    private String tokenSignKey = "123456"; // 秘钥

    /**
     * 创建token
     * @param username 用户名
     * @return token
     */
    public String createToken(String username) {
        String token = Jwts.builder().setSubject(username)
                .setExpiration(new Date(System.currentTimeMillis() + tokenExpiration))
                .signWith(SignatureAlgorithm.HS512, tokenSignKey).compressWith(CompressionCodecs.GZIP).compact();
        return token;
    }

    /**
     * 根据token获取用户id
     *
     * @param token token
     * @return 用户id
     */
    public String getUserFromToken(String token) {
        String user = Jwts.parser().setSigningKey(tokenSignKey).parseClaimsJws(token).getBody().getSubject();
        return user;
    }

    /**
     * 删除token
     *
     * @param token token
     */
    public void removeToken(String token) {
        //jwttoken无需删除，客户端扔掉即可。
    }

}
