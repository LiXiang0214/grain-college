package com.lixiang.servicebase.basic;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 基础查询
 */
@Data
public class BaseQueryVO {

    @ApiModelProperty("页码")
    private Integer page = 1;

    @ApiModelProperty("分页大小")
    private Integer limit = 10;

    public BaseQueryVO() {
    }

}
