package com.lixiang.servicebase.exceptionhandler;

import com.lixiang.commonutils.basic.ReturnEntity;
import com.lixiang.commonutils.enums.ResultCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * MyExceptionHandler 全局捕捉异常
 */
@ControllerAdvice
@Slf4j
public class MyExceptionHandler {

    /**
     * 指定出现什么异常执行这个方法
     *
     * @param e {@link Exception}
     * @return {@link ReturnEntity}
     */
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ReturnEntity error(Exception e) {
        e.printStackTrace();
        return ReturnEntity.fail().desc(e.getMessage()).code(ResultCode.ERROR);
    }

    /**
     * 特定异常
     *
     * @param e {@link Exception}
     * @return {@link ReturnEntity}
     */
    @ExceptionHandler(ArithmeticException.class)
    @ResponseBody
    public ReturnEntity error(ArithmeticException e) {
        e.printStackTrace();
        return ReturnEntity.fail().desc(e.getMessage()).code(ResultCode.ERROR);
    }

    /**
     * 自定义异常
     *
     * @param e {@link Exception}
     * @return {@link ReturnEntity}
     */
    @ExceptionHandler(GuliException.class)
    @ResponseBody
    public ReturnEntity error(GuliException e) {
        log.error(e.getMessage());
        e.printStackTrace();
        return ReturnEntity.fail().desc(e.getMsg()).code(e.getCode());
    }

}
