package com.lixiang.orderservice.mapper;

import com.lixiang.orderservice.entity.PayLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 支付日志 DAO层
 *
 * @author lixiang
 * @since 2020-09-28
 */
public interface PayLogMapper extends BaseMapper<PayLog> {

}
