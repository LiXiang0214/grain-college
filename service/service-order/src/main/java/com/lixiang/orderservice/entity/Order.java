package com.lixiang.orderservice.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import com.lixiang.commonutils.enums.DelEnums;
import com.lixiang.orderservice.enums.OrderStatusEnums;
import com.lixiang.orderservice.enums.PayTypeEnums;
import com.lixiang.servicebase.basic.BasicEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 订单
 *
 * @author lixiang
 * @since 2020-09-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_order") // 注解[@TableName]指明对应的表
@Api(tags = "订单")
public class Order extends BasicEntity {

    @TableId(value = "id", type = IdType.ID_WORKER_STR)
    private String id;

    @ApiModelProperty(value = "订单号")
    private String orderNo;

    @ApiModelProperty(value = "课程id")
    private String courseId;

    @ApiModelProperty(value = "课程名称")
    private String courseTitle;

    @ApiModelProperty(value = "课程封面")
    private String courseCover;

    @ApiModelProperty(value = "讲师名称")
    private String teacherName;

    @ApiModelProperty(value = "会员id")
    private String memberId;

    @ApiModelProperty(value = "会员昵称")
    private String nickname;

    @ApiModelProperty(value = "会员手机号码")
    private String mobile;

    @ApiModelProperty(value = "订单金额（分）")
    private BigDecimal totalFee;

    @ApiModelProperty(value = "支付类型[微信-> 1，支付宝-> 2]")
    private Integer payType;

    @ApiModelProperty("支付类型说明")
    @TableField(exist = false)
    private String payTypeDesc;

    @ApiModelProperty(value = "订单状态[未支付-> 0，已支付-> 1]")
    private Integer status;

    @ApiModelProperty("订单状态说明")
    @TableField(exist = false)
    private String statusDesc;

    @ApiModelProperty(value = "逻辑删除[未删除-> 0，已删除-> 1]")
    private Integer isDeleted;

    @ApiModelProperty("删除状态说明")
    @TableField(exist = false)
    private String isDeletedDesc;

    public String getPayTypeDesc() {
        return PayTypeEnums.getDesc(this.payType);
    }

    public String getStatusDesc() {
        return OrderStatusEnums.getDesc(this.status);
    }

    public String getIsDeletedDesc() {
        return DelEnums.getDesc(this.isDeleted);
    }

}
