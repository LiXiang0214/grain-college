package com.lixiang.orderservice.service;

import com.lixiang.orderservice.entity.PayLog;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * 支付日志 业务层
 *
 * @author lixiang
 * @since 2020-09-28
 */
public interface PayLogService extends IService<PayLog> {

    /**
     * 生成微信支付二维码
     *
     * @param orderNo 订单号
     * @return 二维码等信息
     */
    Map createNatviePay(String orderNo);

    /**
     * 查询订单支付状态
     *
     * @param orderNo 订单号
     * @return 支付状态等信息
     */
    Map<String, String> queryPayStatus(String orderNo);

    /**
     * 添加支付日志，更新订单状态
     *
     * @param returnMap 订单支付状态等信息
     */
    void updateOrdersStatus(Map<String, String> returnMap);

}
