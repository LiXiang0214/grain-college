package com.lixiang.orderservice.enums;

/**
 * 支付类型 枚举
 *
 * @Auther: lixiang
 * @Date: 2020/9/29
 */
public enum PayTypeEnums {
	WeiPay(1, "微信支付"),
	Alipay(2, "支付宝");

	private final Integer value;
	private final String desc;

	PayTypeEnums(Integer value, String desc) {
		this.value = value;
		this.desc = desc;
	}

    /**
     * 获取描述
     *
     * @param value 值
     * @return desc
     */
    public static String getDesc(Integer value) {
        for (PayTypeEnums enumValue : values()) {
            if (enumValue.value().equals(value)) {
                return enumValue.desc();
            }
        }
        return null;
    }

	/**
	 * 获取值
	 *
	 * @return value
	 */
	public Integer value() {
		return value;
	}

    /**
     * 获取描述
     *
     * @return desc
     */
    public String desc() {
        return desc;
    }

}
