package com.lixiang.orderservice.client;

import com.lixiang.servicebase.entity.UcenterMemberOrder;
import io.swagger.annotations.ApiParam;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * 调用服务配置
 */
@Component
@FeignClient("service-ucenter")
public interface MemberClient {

    /**
     * 根据用户id获取用户信息
     *
     * @param id 用户id
     * @return 用户信息
     */
    @PostMapping("/api/ucenter/front/getMemberInfoToOrder/{id}")
    UcenterMemberOrder getMemberInfoToOrder(@ApiParam("用户id") @PathVariable("id") String id);

}
