package com.lixiang.orderservice.client;

import com.lixiang.servicebase.entity.dto.CourseInfoDTO;
import io.swagger.annotations.ApiParam;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * 调用服务配置
 */
@Component
@FeignClient("service-edu")
public interface CourseClient {

    /**
     * 根据课程id查询课程完整信息
     *
     * @param id 课程id
     * @return {@link CourseInfoDTO}
     */
    @GetMapping("/eduservice/edu-course/courseCompleteInfo/{id}")
    CourseInfoDTO courseCompleteInfo(@ApiParam("课程id") @PathVariable("id") String id);

}
