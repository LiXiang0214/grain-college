package com.lixiang.orderservice.controller;

import com.lixiang.commonutils.basic.ReturnEntity;
import com.lixiang.orderservice.enums.ApiPath;
import com.lixiang.orderservice.service.PayLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * 支付日志 控制器
 *
 * @author lixiang
 * @since 2020-09-28
 */
@Api(tags = "支付管理")
@RestController
@RequestMapping(ApiPath.PAYLOG)
@CrossOrigin // 解决跨域
public class PayLogController {

    @Autowired
    private PayLogService payLogService;

    /**
     * 生成微信支付二维码
     *
     * @param orderNo 订单号
     * @return {@link ReturnEntity}
     */
    @ApiOperation("生成微信支付二维码")
    @GetMapping("/createNatviePay/{orderNo}")
    public ReturnEntity createNatviePay(@ApiParam("订单号") @PathVariable("orderNo") String orderNo) {
        // 返回二维码地址，及其他需要的信息
        return ReturnEntity.success().data(payLogService.createNatviePay(orderNo));
    }

    /**
     * 查询订单支付状态
     * 添加支付记录
     * 更新订单状态
     *
     * @param orderNo 订单号
     * @return {@link ReturnEntity}
     */
    @ApiOperation("查询订单支付状态&添加支付记录&更新订单状态")
    @GetMapping("queryPayStatus/{orderNo}")
    public ReturnEntity queryPayStatus(@ApiParam("订单号") @PathVariable("orderNo") String orderNo) {
        Map<String,String> returnMap = payLogService.queryPayStatus(orderNo);
        if(returnMap == null) {
            return ReturnEntity.fail().desc("支付出错了，请刷新后重试！");
        }
        // 获取订单状态
        if(returnMap.get("trade_state").equals("SUCCESS")) { //支付成功
            // 添加记录到支付表，更新订单表订单状态
            payLogService.updateOrdersStatus(returnMap);
            return ReturnEntity.success().desc("支付成功！");
        }
        return ReturnEntity.success().code(25000).desc("支付中...");
    }

}
