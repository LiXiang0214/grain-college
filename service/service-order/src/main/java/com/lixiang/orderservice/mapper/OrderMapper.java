package com.lixiang.orderservice.mapper;

import com.lixiang.orderservice.entity.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 订单 DAO层
 *
 * @author lixiang
 * @since 2020-09-28
 */
public interface OrderMapper extends BaseMapper<Order> {

}
