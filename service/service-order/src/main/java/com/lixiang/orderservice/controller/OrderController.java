package com.lixiang.orderservice.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lixiang.commonutils.basic.ReturnEntity;
import com.lixiang.orderservice.enums.OrderStatusEnums;
import com.lixiang.commonutils.util.JwtUtils;
import com.lixiang.orderservice.entity.Order;
import com.lixiang.orderservice.enums.ApiPath;
import com.lixiang.orderservice.service.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * 订单管理 控制器
 *
 * @author lixiang
 * @since 2020-09-28
 */
@Api(tags = "订单管理")
@RestController
@RequestMapping(ApiPath.ORDER)
@CrossOrigin //解决跨域
public class OrderController {

    @Autowired
    private OrderService orderService;

    /**
     * 生成订单
     *
     * @param courseId 课程ID
     * @param request {@link HttpServletRequest}
     * @return 订单号
     */
    @ApiOperation("生成订单")
    @PostMapping("/{courseId}")
    public ReturnEntity createOrder(@ApiParam("课程ID") @PathVariable("courseId") String courseId,
                                    HttpServletRequest request) {
        String memberId = JwtUtils.getMemberIdByJwtToken(request);
        Order orderDB = orderService.getUserCourseOrderInfo(memberId, courseId);
        if (!StringUtils.isEmpty(orderDB) &&
                OrderStatusEnums.NOTPAY.value().equals(orderDB.getStatus())) { // 已存在订单但未支付支付
            return ReturnEntity.success().data("orderNo", orderDB.getOrderNo());
        }
        String orderNo = orderService.createOrder(courseId, memberId);
        return ReturnEntity.success().data("orderNo", orderNo);
    }

    /**
     * 根据订单号查询订单信息
     *
     * @param orderNo 订单号
     * @return 订单信息
     */
    @ApiOperation("查询订单信息")
    @GetMapping("getOrderInfo/{orderNo}")
    public ReturnEntity getOrderInfo(@ApiParam("订单号") @PathVariable("orderNo") String orderNo) {
        QueryWrapper<Order> orderWrapper = new QueryWrapper<>();
        orderWrapper.eq("order_no", orderNo);
        return ReturnEntity.success().data("order", orderService.getOne(orderWrapper));
    }

    /**
     * 根据用户ID和课程ID判断课程是否已购买
     *
     * @param memberId 用户ID
     * @param courseId 课程ID
     * @return 订单信息
     */
    @ApiOperation("根据用户ID和课程ID判断课程是否已购买")
    @GetMapping("isBuy/{memberId}/{courseId}")
    public Boolean isBuy(@ApiParam("用户ID") @PathVariable("memberId") String memberId,
                                               @ApiParam("课程ID") @PathVariable("courseId") String courseId) {
        Order orderDB = orderService.getUserCourseOrderInfo(memberId, courseId);
        return orderDB.getStatus().equals(OrderStatusEnums.PAID.value());
    }

}
