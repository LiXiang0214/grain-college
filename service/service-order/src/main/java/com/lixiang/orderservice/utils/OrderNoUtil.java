package com.lixiang.orderservice.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * 订单号工具类
 */
public class OrderNoUtil {

    /**
     * 获取订单号
     *
     * @return 订单号
     */
    public static String getOrderNo() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String newDate = dateFormat.format(new Date());
        String result = "";
        Random random = new Random();
        for (int i = 0; i < 3; i ++) {
            result += random.nextInt(10);
        }
        return newDate + result;
    }

}
