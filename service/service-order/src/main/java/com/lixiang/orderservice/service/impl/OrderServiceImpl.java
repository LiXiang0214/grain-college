package com.lixiang.orderservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lixiang.commonutils.enums.DelEnums;
import com.lixiang.orderservice.client.CourseClient;
import com.lixiang.orderservice.client.MemberClient;
import com.lixiang.orderservice.entity.Order;
import com.lixiang.orderservice.enums.OrderStatusEnums;
import com.lixiang.orderservice.enums.PayTypeEnums;
import com.lixiang.orderservice.mapper.OrderMapper;
import com.lixiang.orderservice.service.OrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lixiang.orderservice.utils.OrderNoUtil;
import com.lixiang.servicebase.entity.UcenterMemberOrder;
import com.lixiang.servicebase.entity.dto.CourseInfoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 订单 业务层实现
 *
 * @author lixiang
 * @since 2020-09-28
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements OrderService {

    @Autowired
    private MemberClient memberClient;
    @Autowired
    private CourseClient courseClient;

    /**
     * 生成订单
     *
     * @param courseId 课程ID
     * @param memberId 用户ID
     * @return 订单号
     */
    @Override
    public String createOrder(String courseId, String memberId) {
        // 1、获取用户信息
        UcenterMemberOrder memberInfo = memberClient.getMemberInfoToOrder(memberId);

        // 2、获取课信息
        CourseInfoDTO courseInfo = courseClient.courseCompleteInfo(courseId);

        // 3、生成订单
        Order order = new Order();
        order.setOrderNo(OrderNoUtil.getOrderNo());// 订单号
        order.setCourseId(courseId); // 课程id
        order.setCourseTitle(courseInfo.getTitle());
        order.setCourseCover(courseInfo.getCover());
        order.setTeacherName(courseInfo.getTeacherName());
        order.setTotalFee(courseInfo.getPrice());
        order.setMemberId(memberId);
        order.setMobile(memberInfo.getMobile());
        order.setNickname(memberInfo.getNickname());
        order.setStatus(OrderStatusEnums.NOTPAY.value());  // 订单状态
        order.setPayType(PayTypeEnums.WeiPay.value());  // 支付类型: 微信
        // 添加
        baseMapper.insert(order);

        // 返回订单号
        return order.getOrderNo();
    }

    /**
     * 根据用户ID和课程ID查询订单信息
     *
     * @param memberId 用户ID
     * @param courseId 课程ID
     * @return 订单信息
     */
    @Override
    public Order getUserCourseOrderInfo(String memberId, String courseId) {
        QueryWrapper<Order> orderWrapper = new QueryWrapper<>();
        orderWrapper.eq("member_id", memberId);
        orderWrapper.eq("course_id", courseId);
        orderWrapper.eq("is_deleted", DelEnums.Normal.value());
        return getOne(orderWrapper);
    }

}
