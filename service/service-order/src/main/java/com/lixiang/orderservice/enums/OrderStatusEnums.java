package com.lixiang.orderservice.enums;

/**
 * 订单状态 枚举
 *
 * @Auther: lixiang
 * @Date: 2020/9/29
 */
public enum OrderStatusEnums {
	NOTPAY(0, "未支付"),
	PAID(1, "已支付");

	private final Integer value;
	private final String desc;

	OrderStatusEnums(Integer value, String desc) {
		this.value = value;
		this.desc = desc;
	}

    /**
     * 获取描述
     *
     * @param value 值
     * @return desc
     */
    public static String getDesc(Integer value) {
        for (OrderStatusEnums enumValue : values()) {
            if (enumValue.value().equals(value)) {
                return enumValue.desc();
            }
        }
        return null;
    }

	/**
	 * 获取值
	 *
	 * @return value
	 */
	public Integer value() {
		return value;
	}

    /**
     * 获取描述
     *
     * @return desc
     */
    public String desc() {
        return desc;
    }

}
