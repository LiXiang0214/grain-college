package com.lixiang.orderservice.service;

import com.lixiang.orderservice.entity.Order;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 订单 业务层
 *
 * @author lixiang
 * @since 2020-09-28
 */
public interface OrderService extends IService<Order> {

    /**
     * 生成订单
     *
     * @param courseId 课程ID
     * @param memberId 用户ID
     * @return 订单号
     */
    String createOrder(String courseId, String memberId);

    /**
     * 根据用户ID和课程ID查询订单信息
     *
     * @param memberId 用户ID
     * @param courseId 课程ID
     * @return 订单信息
     */
    Order getUserCourseOrderInfo(String memberId, String courseId);

}
