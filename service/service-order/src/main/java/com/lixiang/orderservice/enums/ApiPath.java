package com.lixiang.orderservice.enums;

/**
 * 路径地址配置
 *
 * @author lixiang
 */
public class ApiPath {

	/**
	 * edu根地址
	 */
	public static final String ROOT = "/orderservice";

	/**
	 * 订单地址
	 */
	public static final String ORDER = ROOT + "/order";

	/**
	 * 支付日志
	 */
	public static final String PAYLOG = ROOT + "/pay-log";

	protected ApiPath() {

    }

}
