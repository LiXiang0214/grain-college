package com.lixiang.orderservice.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.wxpay.sdk.WXPayUtil;
import com.lixiang.orderservice.entity.Order;
import com.lixiang.orderservice.entity.PayLog;
import com.lixiang.orderservice.enums.OrderStatusEnums;
import com.lixiang.orderservice.enums.PayTypeEnums;
import com.lixiang.orderservice.mapper.PayLogMapper;
import com.lixiang.orderservice.service.OrderService;
import com.lixiang.orderservice.service.PayLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lixiang.orderservice.utils.HttpClient;
import com.lixiang.servicebase.exceptionhandler.GuliException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 支付日志 业务层实现
 *
 * @author lixiang
 * @since 2020-09-28
 */
@Service
public class PayLogServiceImpl extends ServiceImpl<PayLogMapper, PayLog> implements PayLogService {

    private static final String APPID = "wx74862e0dfcf69954"; // 公众账号ID
    private static final String MCH_ID = "1558950191"; // 微信支付分配的 商户号
    private static final String IP = "127.0.0.1"; // 项目域名
    private static final String NOTIFY_URL = "http://guli.shop/api/order/weixinPay/weixinNotify"; // 异步接收微信支付结果通知的 回调地址
    private static final String XMLPARAM_KEY = "T6m9iK73b0kn9g5v426MKfHQH7X8rKwb"; // 商户Key

    @Autowired
    private OrderService orderService;


    /**
     * 生成微信支付二维码
     *
     * @param orderNo 订单号
     * @return 二维码等信息
     */
    @Override
    public Map createNatviePay(String orderNo) {
        try {
            // 1、根据订单号查询订单信息
            QueryWrapper<Order> orderWrapper = new QueryWrapper<Order>().eq("order_no", orderNo);
            Order orderDB = orderService.getOne(orderWrapper);
            String totalFee = orderDB.getTotalFee().multiply(new BigDecimal("100")).longValue() + ""; // 标价金额，BigDecimal转String

            // 2、使用 map 设置生成二维码所需参数(键，即参数名固定)
            Map paramMap = new HashMap();
            paramMap.put("appid", APPID); // 公众账号ID
            paramMap.put("mch_id", MCH_ID); // 微信支付分配的 商户号
            paramMap.put("nonce_str", WXPayUtil.generateNonceStr()); // 随机字符串(使二维码不一样)
            paramMap.put("body", orderDB.getCourseTitle()); // 商品简单描述：标题
            paramMap.put("out_trade_no", orderNo); // 商户订单号(二维码标识)
            paramMap.put("total_fee", totalFee); // 标价金额
            paramMap.put("spbill_create_ip", IP); // 项目域名
            paramMap.put("notify_url", NOTIFY_URL); // 异步接收微信支付结果通知的 回调地址
            paramMap.put("trade_type", "NATIVE"); // 支付类型(根据价格生成二维码)

            // 3、请求微信支付提供的固定的地址，传递参数xml格式
            HttpClient client = new HttpClient("https://api.mch.weixin.qq.com/pay/unifiedorder");
            // 设置xml格式的参数
            client.setXmlParam(WXPayUtil.generateSignedXml(paramMap, XMLPARAM_KEY)); // 商户Key
            client.setHttps(true);
            client.post();

            // 4、返回结果
            Map<String, String> resultMap = WXPayUtil.xmlToMap(client.getContent()); // 把 xml格式 转换 map集合

            // 5、最终返回数据
            Map returnMap = new HashMap();
            returnMap.put("out_trade_no", orderNo);
            returnMap.put("course_id", orderDB.getCourseId());
            returnMap.put("total_fee", orderDB.getTotalFee()); // 价格
            returnMap.put("result_code", resultMap.get("result_code")); // 二维码操作状态码
            returnMap.put("code_url", resultMap.get("code_url")); // 二维码地址

            return returnMap;

        }catch(Exception e) {
            throw new GuliException(20001, "生成二维码失败，请刷新重试！");
        }
    }

    /**
     * 查询订单支付状态
     *
     * @param orderNo 订单号
     * @return 支付状态等信息
     */
    @Override
    public Map<String, String> queryPayStatus(String orderNo) {
        try {
            // 1、封装参数
            Map queryMap = new HashMap<>();
            queryMap.put("appid", APPID); // 公众账号ID
            queryMap.put("mch_id", MCH_ID); // 微信支付分配的 商户号
            queryMap.put("out_trade_no", orderNo); // 商户订单号
            queryMap.put("nonce_str", WXPayUtil.generateNonceStr());  // 随机字符串

            // 2、发送httpclient
            HttpClient client = new HttpClient("https://api.mch.weixin.qq.com/pay/orderquery");
            client.setXmlParam(WXPayUtil.generateSignedXml(queryMap, XMLPARAM_KEY));
            client.setHttps(true);
            client.post();

            // 3、得到请求返回内容，转成Map再返回
            return  WXPayUtil.xmlToMap(client.getContent());

        }catch(Exception e) {
            return null;
        }
    }

    /**
     * 添加支付日志，更新订单状态
     *
     * @param returnMap 订单支付状态等信息
     */
    @Override
    public void updateOrdersStatus(Map<String, String> returnMap) {
        // 1、从map获取订单号
        String orderNo = returnMap.get("out_trade_no");
        // 2、根据订单号查询订单信息
        QueryWrapper<Order> wrapper = new QueryWrapper<Order>().eq("order_no",orderNo);
        Order orderDB = orderService.getOne(wrapper);

        // 3、更新订单表订单状态
        if(OrderStatusEnums.PAID.value().equals(orderDB.getStatus().intValue())) {
            return;
        }
        orderDB.setStatus(OrderStatusEnums.PAID.value()); // 已支付
        orderService.updateById(orderDB);

        // 4、添加支付日志
        PayLog payLog = new PayLog();
        payLog.setOrderNo(orderNo);  // 订单号
        payLog.setPayTime(new Date()); // 订单完成时间
        payLog.setPayType(PayTypeEnums.WeiPay.value()); // 微信支付
        payLog.setTotalFee(orderDB.getTotalFee()); // 总金额(分)
        payLog.setTradeState(returnMap.get("trade_state")); // 支付状态
        payLog.setTransactionId(returnMap.get("transaction_id")); // 流水号
        payLog.setAttr(JSONObject.toJSONString(returnMap)); // 其他属性，存为JSON格式

        baseMapper.insert(payLog);
    }

}
