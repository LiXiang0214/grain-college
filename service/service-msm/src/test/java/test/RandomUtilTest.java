package test;

import com.lixiang.msmservice.utils.RandomUtil;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author
 * @since 2018/12/13
 */
public class RandomUtilTest {

    @Test
    public void run() {
        String fourBitRandom = RandomUtil.getFourBitRandom();
        System.out.println("RandomUtil.getFourBitRandom()：" + fourBitRandom);

        String sixBitRandom = RandomUtil.getSixBitRandom();
        System.out.println("RandomUtil.getSixBitRandom()：" + sixBitRandom);

        List list = new ArrayList();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        RandomUtil.getRandom(list, 3);
    }
}
