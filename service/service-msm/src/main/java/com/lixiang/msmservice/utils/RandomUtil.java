package com.lixiang.msmservice.utils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * 获取随机数
 */
public class RandomUtil {

	private static final Random RANDOM = new Random();
	private static final DecimalFormat FOURDF = new DecimalFormat("0000");
	private static final DecimalFormat SIXDF = new DecimalFormat("000000");

	/**
	 * 生成 4 位验证码
	 *
	 * @return 验证码
	 */
	public static String getFourBitRandom() {
		return FOURDF.format(RANDOM.nextInt(10000));
	}

	/**
	 * 生成 6 位验证码
	 *
	 * @return 验证码
	 */
	public static String getSixBitRandom() {
		return SIXDF.format(RANDOM.nextInt(1000000));
	}

	/**
	 * 在给定数组中，抽取指定个数字生成验证码
	 *
	 * @param list 给定数组
	 * @param count count个数据
	 * @return 验证码
	 */
	public static ArrayList getRandom(List list, int count) {
		HashMap<Object, Object> hashMap = new HashMap<>();

		// 生成随机数字并存入HashMap
		for (int i = 0; i < list.size(); i++) {
			int number = RANDOM.nextInt(100) + 1;
			hashMap.put(number, i);
		}

		// 从HashMap导入数组
		Object[] robjs = hashMap.values().toArray();
		ArrayList array = new ArrayList();

		// 遍历数组并打印数据
		for (int i = 0; i < count; i++) {
			array.add(list.get((int) robjs[i]));
		}
		return array;
	}

}
