package com.lixiang.msmservice.service;

import com.alibaba.fastjson.JSONObject;

import java.util.Map;

/**
 * 短信服务 业务层
 *
 * @author lixiang
 * @since 2020-09-16
 */
public interface MsmService {

    /**
     * 发送短信
     *
     * @param phone 电话号码
     * @param templateParam 消息模板参数
     * @return 阿里云短信服务返回信息
     */
    JSONObject send(String phone, Map<String, Object> templateParam);

}
