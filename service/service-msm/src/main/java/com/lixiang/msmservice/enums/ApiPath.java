package com.lixiang.msmservice.enums;

/**
 * 路径地址配置
 *
 * @author lixiang
 */
public class ApiPath {

	/**
	 * 短信根地址
	 */
	public static final String ROOT = "/msmservice";

	/**
	 * 短信
	 */
	public static final String MSM = ROOT + "/msm";

	protected ApiPath() {

    }

}
