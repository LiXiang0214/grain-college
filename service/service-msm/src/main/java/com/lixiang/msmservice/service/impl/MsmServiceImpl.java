package com.lixiang.msmservice.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.lixiang.msmservice.service.MsmService;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Map;


/**
 * 短信服务 业务层 实现
 *
 * @author lixiang
 * @since 2020-09-16
 */
@Service
public class MsmServiceImpl implements MsmService {

    @ApiModelProperty(value = "区域id")
    private String regionId = "cn-hangzhou";

    @ApiModelProperty(value = "用户AccessKey ID")
    @Value("${aliyun.oss.file.keyid}")
    private String accessKeyId;

    @ApiModelProperty(value = "用户AccessKey 密钥")
    @Value("${aliyun.oss.file.keysecret}")
    private String accessKeySecret;

    @ApiModelProperty(value = "消息模板编号")
    @Value("${aliyun.template-code}")
    private String templateCode;

    /**
     * 发送短信
     *
     * @param phone 电话号码
     * @param templateParam 消息模板参数
     * @return 阿里云短信服务返回信息
     */
    @Override
    public JSONObject send(String phone, Map<String, Object> templateParam) {
        // 空判断
        if(StringUtils.isEmpty(phone)) {
            return null;
        }

        // 初始化
        DefaultProfile profile = DefaultProfile.getProfile(regionId, accessKeyId, accessKeySecret);
        IAcsClient client = new DefaultAcsClient(profile);

        // 设置请求参数
        CommonRequest request = new CommonRequest();
        // 设置固定的参数
        request.setSysMethod(MethodType.POST); // 请求方式
        request.setSysDomain("dysmsapi.aliyuncs.com");
        request.setSysVersion("2017-05-25"); // 版本
        request.setSysAction("SendSms"); // 使用的方法
        // 设置发送相关的参数
        request.putQueryParameter("RegionId", regionId); // 区域id
        request.putQueryParameter("PhoneNumbers", phone); // 目标电话号码
        request.putQueryParameter("SignName", "翔哥的小店"); // 签名名称
        request.putQueryParameter("TemplateCode", templateCode); // 消息模板编号
        request.putQueryParameter("TemplateParam", JSONObject.toJSONString(templateParam)); // 消息模板参数，转为json格式

        try {
            // 发送短信
            CommonResponse response = client.getCommonResponse(request);
            // 返回信息
            return JSONObject.parseObject(response.getData());
        } catch (ServerException e) {
            e.printStackTrace();
        } catch (ClientException e) {
            e.printStackTrace();
        }
        return null;
    }

}
