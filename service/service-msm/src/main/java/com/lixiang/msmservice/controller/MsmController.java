package com.lixiang.msmservice.controller;

import com.alibaba.fastjson.JSONObject;
import com.lixiang.commonutils.basic.ReturnEntity;
import com.lixiang.msmservice.enums.ApiPath;
import com.lixiang.msmservice.service.MsmService;
import com.lixiang.msmservice.utils.RandomUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 短信服务控制器
 *
 * @author lixiang
 * @since 2020-08-10
 */
@Api(tags = "短信服务")
@RestController
@RequestMapping(ApiPath.MSM)
@CrossOrigin //解决跨域
@Slf4j
public class MsmController {

    @Autowired
    private MsmService msmService;

    @Autowired
    private RedisTemplate<String, String> redis;

    /**
     * 发送验证码
     *
     * @param phone 手机号
     * @return {@link ReturnEntity}
     */
    @ApiOperation("发送验证码")
    @GetMapping("/send/{phone}")
    public ReturnEntity send(@ApiParam("手机号") @PathVariable("phone") String phone) {

        // 查询Redis，检查是否以发送
        if(!StringUtils.isEmpty(redis.opsForValue().get(phone))) {
            return ReturnEntity.success().desc("已发送");
        }

        // 生成 4位 验证码
        String veriCode = RandomUtil.getFourBitRandom();
        // 消息模板参数(放入Map中一遍生成json格式)
        Map<String,Object> templateParam = new HashMap<>();
        templateParam.put("code", veriCode);

        // 发送短信
        JSONObject resultInfo = msmService.send(phone, templateParam);
        // Code返回 "OK" 代表成功
        if (!StringUtils.isEmpty(resultInfo)) {
            if ("OK".equals(resultInfo.get("Code"))) {
                // 保存到 Redis
                redis.opsForValue().set(phone, veriCode, 10, TimeUnit.MINUTES); // 10分钟过期
                return ReturnEntity.success().desc("已发送");
            }
            log.error((String) resultInfo.get("Message")); // 将信息打印
        }
        return ReturnEntity.fail().desc("短信发送失败，请重试！");

    }

}

