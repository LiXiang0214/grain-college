package com.lixiang.statisticsservice.client;

import com.lixiang.commonutils.basic.ReturnEntity;
import io.swagger.annotations.ApiParam;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


/**
 * 调用服务配置
 */
@FeignClient("service-ucenter") //调用的服务名称
@Component
public interface UcenterClient { //定义调用的方法路径

    /**
     * 查询某一天注册人数
     *
     * @param date 日期
     * @return {@link ReturnEntity}
     */
    @GetMapping("/api/ucenter/front/countRegister/{date}")
    Integer countRegister(@ApiParam("日期") @PathVariable("date") String date);

    /**
     * 查询系统课程数
     *
     * @return {@link ReturnEntity}
     */
    @GetMapping("/eduservice/edu-course/countCourse")
    Integer countCourse();

}
