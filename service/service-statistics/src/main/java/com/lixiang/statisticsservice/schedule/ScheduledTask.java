package com.lixiang.statisticsservice.schedule;

import com.lixiang.statisticsservice.service.StatisticsService;
import com.lixiang.statisticsservice.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 定时任务
 */
@Component
public class ScheduledTask {

    @Autowired
    private StatisticsService statisticsService;

    /**
     * 每天凌晨1点，把前一天数据进行数据查询添加
     */
    @Scheduled(cron = "0 0 1 * * ?")
    public void updateStatistics() {
        statisticsService.updateStatistics(DateUtil.formatDate(DateUtil.addDays(new Date(), -1)));
    }

}
