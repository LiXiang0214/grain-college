package com.lixiang.statisticsservice.mapper;

import com.lixiang.statisticsservice.entity.Statistics;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 每日数据统计 DAO层
 *
 * @author lixiang
 * @since 2020-09-30
 */
public interface StatisticsMapper extends BaseMapper<Statistics> {

}
