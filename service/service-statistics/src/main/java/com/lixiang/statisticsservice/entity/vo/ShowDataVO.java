package com.lixiang.statisticsservice.entity.vo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 图表显示
 */
@Data
@Api(tags = "图表显示VO")
public class ShowDataVO {

    @ApiModelProperty(value = "数据种类")
    private String type;

    @ApiModelProperty(value = "开始日期")
    private String begin;

    @ApiModelProperty(value = "结束日期")
    private String end;

}
