package com.lixiang.statisticsservice.enums;

/**
 * 路径地址配置
 *
 * @author lixiang
 */
public class ApiPath {

	/**
	 * edu根地址
	 */
	public static final String ROOT = "/statisticsservice";

	/**
	 * 统计
	 */
	public static final String STATISTICS = ROOT + "/statistics";


	protected ApiPath() { }

}
