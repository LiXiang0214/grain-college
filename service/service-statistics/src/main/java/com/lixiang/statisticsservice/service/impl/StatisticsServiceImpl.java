package com.lixiang.statisticsservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lixiang.statisticsservice.client.UcenterClient;
import com.lixiang.statisticsservice.entity.Statistics;
import com.lixiang.statisticsservice.entity.vo.ShowDataVO;
import com.lixiang.statisticsservice.enums.DataTypeEnums;
import com.lixiang.statisticsservice.mapper.StatisticsMapper;
import com.lixiang.statisticsservice.service.StatisticsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 每日数据统计 业务层实现
 *
 * @author lixiang
 * @since 2020-09-30
 */
@Service
public class StatisticsServiceImpl extends ServiceImpl<StatisticsMapper, Statistics> implements StatisticsService {

    @Autowired
    private UcenterClient ucenterClient;

    /**
     * 保存每日统计数据
     *
     * @param date 日期
     */
    @Override
    public void updateStatistics(String date) {
        // 1、添加记录之前删除表相同日期的数据
        QueryWrapper<Statistics> wrapper = new QueryWrapper<>();
        wrapper.eq("date_calculated", date); // 统计日期
        baseMapper.delete(wrapper);

        // 2、某一天注册人数
        Integer countRegister = ucenterClient.countRegister(date);

        // 3、课程数
        Integer countCourse = ucenterClient.countCourse();

        // 4、添加到统计分析表里
        Statistics statistics = new Statistics();
        statistics.setDateCalculated(date); // 统计日期
        statistics.setRegisterNum(countRegister); // 注册人数
        statistics.setCourseNum(countCourse);
        statistics.setVideoViewNum(RandomUtils.nextInt(100, 200));
        statistics.setLoginNum(RandomUtils.nextInt(100, 200));

        baseMapper.insert(statistics);
    }

    /**
     * 图表显示
     *
     * @param showDataVO {@link ShowDataVO}
     * @return 日期json数组，数量json数组
     */
    @Override
    public Map<String, Object> getShowData(ShowDataVO showDataVO) {
        // 日期list
        List<String> dateList = new ArrayList<>();
        // 数量list
        List<Integer> numDataList = new ArrayList<>();

        // 1、查询指定数据种类
        QueryWrapper<Statistics> staWrapper = new QueryWrapper<>();
        staWrapper.select("date_calculated", showDataVO.getType());
        staWrapper.between("date_calculated", showDataVO.getBegin(), showDataVO.getEnd());
        List<Statistics> statisticsList = baseMapper.selectList(staWrapper);

        statisticsList.forEach(statistics -> { // 遍历封装
            // 封装日期list集合
            dateList.add(statistics.getDateCalculated());
            // 封装对应数量
            switch (showDataVO.getType()) {
                case DataTypeEnums.LOGINNUM:
                    numDataList.add(statistics.getLoginNum());
                    break;
                case DataTypeEnums.REGISTERNUM:
                    numDataList.add(statistics.getRegisterNum());
                    break;
                case DataTypeEnums.VIDEOVIEWNUM:
                    numDataList.add(statistics.getVideoViewNum());
                    break;
                case DataTypeEnums.COURSENUM:
                    numDataList.add(statistics.getCourseNum());
                    break;
                default:
                    break;
            }
        });

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("dateList", dateList);
        resultMap.put("numDataList", numDataList);
        return resultMap;
    }

}
