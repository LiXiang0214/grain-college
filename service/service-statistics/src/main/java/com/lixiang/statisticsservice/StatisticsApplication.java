package com.lixiang.statisticsservice;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * Statistics服务启动类
 */
@SpringBootApplication
@EnableDiscoveryClient  //nacos注册
@EnableFeignClients
@MapperScan("com.lixiang.statisticsservice.mapper")
@ComponentScan("com.lixiang") // 扫描
public class StatisticsApplication {

    /**
     * Main 方法
     *
     * @param args 参数
     */
    public static void main(String[] args) {
        SpringApplication.run(StatisticsApplication.class, args);
    }

}
