package com.lixiang.statisticsservice.enums;

/**
 * 数据种类
 */
public interface DataTypeEnums {

    String LOGINNUM = "login_num"; // 用户登录数统计

    String REGISTERNUM = "register_num"; // 用户注册数统计

    String VIDEOVIEWNUM = "video_view_num"; // 课程播放数统计

    String COURSENUM = "course_num"; // 课程数统计

}
