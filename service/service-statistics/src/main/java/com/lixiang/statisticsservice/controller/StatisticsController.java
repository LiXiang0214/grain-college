package com.lixiang.statisticsservice.controller;


import com.lixiang.commonutils.basic.ReturnEntity;
import com.lixiang.statisticsservice.entity.vo.ShowDataVO;
import com.lixiang.statisticsservice.enums.ApiPath;
import com.lixiang.statisticsservice.service.StatisticsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 每日数据统计 控制器
 *
 * @author lixiang
 * @since 2020-09-30
 */
@Api(tags = "每日数据统计")
@RestController
@RequestMapping(ApiPath.STATISTICS)
@CrossOrigin // 解决跨域
public class StatisticsController {

    @Autowired
    private StatisticsService statisticsService;

    /**
     * 保存每日统计数据
     *
     * @param date 日期
     * @return {@link ReturnEntity}
     */
    @PostMapping("updateStatistics/{date}")
    public ReturnEntity updateStatistics(@ApiParam("日期") @PathVariable("date") String date) {
        statisticsService.updateStatistics(date);
        return ReturnEntity.success();
    }

    /**
     * 图表显示
     *
     * @param showDataVO {@link ShowDataVO}
     * @return 日期json数组，数量json数组
     */
    @PostMapping("showData")
    public ReturnEntity showData(@RequestBody ShowDataVO showDataVO) {
        Map<String,Object> map = statisticsService.getShowData(showDataVO);
        return ReturnEntity.success().data(map);
    }


}

