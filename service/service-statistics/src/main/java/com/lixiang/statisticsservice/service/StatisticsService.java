package com.lixiang.statisticsservice.service;

import com.lixiang.statisticsservice.entity.Statistics;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lixiang.statisticsservice.entity.vo.ShowDataVO;

import java.util.Map;

/**
 * 每日数据统计 业务层
 *
 * @author lixiang
 * @since 2020-09-30
 */
public interface StatisticsService extends IService<Statistics> {

    /**
     * 保存每日统计数据
     *
     * @param date 日期
     */
    void updateStatistics(String date);

    /**
     * 图表显示
     *
     * @param showDataVO {@link ShowDataVO}
     * @return 日期json数组，数量json数组
     */
    Map<String, Object> getShowData(ShowDataVO showDataVO);

}
