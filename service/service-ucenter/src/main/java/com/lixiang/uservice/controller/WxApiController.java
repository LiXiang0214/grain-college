package com.lixiang.uservice.controller;


import com.google.gson.Gson;
import com.lixiang.commonutils.util.JwtUtils;
import com.lixiang.uservice.entity.UcenterMember;
import com.lixiang.servicebase.exceptionhandler.GuliException;
import com.lixiang.uservice.enums.ApiPath;
import com.lixiang.uservice.service.UcenterService;
import com.lixiang.uservice.utils.ConstantWxUtils;
import com.lixiang.uservice.utils.HttpClientUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.net.URLEncoder;
import java.util.HashMap;

/**
 * 微信登录 控制器
 *
 * @author lixiang
 * @since 2020-09-16
 */
@Api(tags = "微信登录")
@Controller
@RequestMapping(ApiPath.WEIXIN)
@CrossOrigin //解决跨域
public class WxApiController {

    @Autowired
    private UcenterService ucenterService;

    /**
     * 生成微信登录二维码
     *
     * @return 重定向到请求微信地址
     */
    @ApiOperation("生成微信登录二维码")
    @GetMapping("/login")
    public String getWxCode() {
        // 1、微信开放平台授权 baseUrl，%s相当于?，代表占位符
        String baseUrl = "https://open.weixin.qq.com/connect/qrconnect" +
                "?appid=%s" +
                "&redirect_uri=%s" +
                "&response_type=code" +
                "&scope=snsapi_login" +
                "&state=%s" +
                "#wechat_redirect";

        // 2、对 redirect_url 进行 URLEncoder 编码
        String redirectUrl = ConstantWxUtils.WX_OPEN_REDIRECT_URL;
        try {
            redirectUrl = URLEncoder.encode(redirectUrl, "utf-8");
        }catch(Exception e) {
        }

        // 3、设置 %s 里面值
        String url = String.format(baseUrl, ConstantWxUtils.WX_OPEN_APP_ID, redirectUrl, "atguigu");

        // 4、重定向到请求微信地址里面
        return "redirect:" + url;
    }

    /**
     * 获取扫描人信息，添加数据
     *
     * @param code code值，临时票据，类似于验证码
     * @return 重定向到首页地址
     */
    @ApiOperation("获取扫描人信息")
    @GetMapping("/callback")
    public String callback(String code) {
        try {
            // 1、微信固定的地址
            String baseAccessTokenUrl = "https://api.weixin.qq.com/sns/oauth2/access_token" +
                    "?appid=%s" +
                    "&secret=%s" +
                    "&code=%s" +
                    "&grant_type=authorization_code";

            // 2、拼接三个参数：OPEN_APP_ID、OPEN_APP秘钥、code值
            String accessTokenUrl = String.format(baseAccessTokenUrl,
                    ConstantWxUtils.WX_OPEN_APP_ID, ConstantWxUtils.WX_OPEN_APP_SECRET, code
            );

            // 3、使用httpclient请求这个拼接好的地址
            String accessTokenInfo = HttpClientUtils.get(accessTokenUrl);

            // 4、从accessTokenInfo中取 accsess_token 和 openid
            HashMap mapAccessToken = new Gson().fromJson(accessTokenInfo, HashMap.class);
            String accessToken = (String)mapAccessToken.get("access_token");
            String openid = (String)mapAccessToken.get("openid");

            // 5、获取到扫描人信息
            // (1)根据 <accsess_token> 和 <openid> 访问微信的资源服务器
            String baseUserInfoUrl = "https://api.weixin.qq.com/sns/userinfo" + "?access_token=%s" + "&openid=%s";
            String userInfoUrl = String.format(baseUserInfoUrl, accessToken, openid); // 拼接两个参数
            // (2)发送请求
            String userInfo = HttpClientUtils.get(userInfoUrl);
            // (3)获取返回userinfo字符串扫描人信息
            HashMap userInfoMap = new Gson().fromJson(userInfo, HashMap.class);
            String nickname = (String)userInfoMap.get("nickname"); // 昵称
            String headimgurl = (String)userInfoMap.get("headimgurl"); // 头像
            Double wSex = (Double)userInfoMap.get("sex");
            Integer sex = Integer.valueOf(wSex.intValue()); // 性别

            // 6、把扫描人信息保存到数据库里面
            UcenterMember ucenterMember = ucenterService.getOpenIdMember(openid);
            if(StringUtils.isEmpty(ucenterMember)) {
                // (4)将扫描人信息保存到数据库
                ucenterMember = new UcenterMember();
                ucenterMember.setOpenid(openid);
                ucenterMember.setNickname(nickname);
                ucenterMember.setAvatar(headimgurl);
                ucenterMember.setSex(sex);
                ucenterService.save(ucenterMember);
            } else if (!nickname.equals(ucenterMember.getNickname())) {
                ucenterMember.setNickname(nickname);
                ucenterMember.setAvatar(headimgurl);
                ucenterMember.setSex(sex);
                ucenterService.updateById(ucenterMember);
            }

            // 6、使用 jwt 根据 member对象 生成 token 字符串
            // 因为端口号不同存在蛞蝓问题，cookie不能跨域，所以这里使用url重写
            String jwtToken = JwtUtils.getJwtToken(ucenterMember.getId(), ucenterMember.getNickname());
            // 7、返回首页，传递 token
            return "redirect:http://localhost:3000?token=" + jwtToken;

        }catch(Exception e) {
            throw new GuliException(20001, "登录失败，请重新扫描");
        }
    }

}

