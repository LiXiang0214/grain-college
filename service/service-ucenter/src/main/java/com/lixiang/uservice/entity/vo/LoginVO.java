package com.lixiang.uservice.entity.vo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * 用户登录
 */
@Data
@Api(tags = "用户登录")
public class LoginVO {

    @ApiModelProperty(value = "手机号")
    @NotEmpty(message = "请填写手机号")
    private String mobile;

    @ApiModelProperty(value = "密码")
    @NotEmpty(message = "请填写密码")
    private String password;

}
