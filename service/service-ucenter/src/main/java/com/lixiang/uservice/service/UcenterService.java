package com.lixiang.uservice.service;

import com.lixiang.uservice.entity.UcenterMember;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lixiang.uservice.entity.vo.LoginVO;
import com.lixiang.uservice.entity.vo.RegisterVO;

/**
 * 用户中心 业务层
 *
 * @author lixiang
 * @since 2020-09-15
 */
public interface UcenterService extends IService<UcenterMember> {

    /**
     * 登录
     *
     * @param loginVO {@link LoginVO}
     * @return token
     */
    String login(LoginVO loginVO);

    /**
     * 注册
     *
     * @param registerVO {@link RegisterVO}
     */
    void register(RegisterVO registerVO);

    /**
     * 根据openid查找用户
     * @param openid openid
     * @return 用户信息
     */
    UcenterMember getOpenIdMember(String openid);

    /**
     * 查询某一天注册人数
     *
     * @param date 日期
     * @return 注册人数
     */
    Integer countRegisterDay(String date);

}
