package com.lixiang.uservice.enums;

/**
 * 路径地址配置
 *
 * @author lixiang
 */
public class ApiPath {

	/**
	 * 根地址
	 */
	public static final String ROOT = "/api/ucenter";

	/**
	 * 后台登录
	 */
	public static final String ADMIN = ROOT + "/admin";

	/**
	 * 前台登录
	 */
	public static final String FRONT = ROOT + "/front";

	/**
	 * 微信登录
	 */
	public static final String WEIXIN = ROOT + "/wx";


	protected ApiPath() {

    }

}
