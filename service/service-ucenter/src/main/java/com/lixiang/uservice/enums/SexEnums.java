package com.lixiang.uservice.enums;

/**
 * 性别 枚举
 *
 * @Auther: lixiang
 * @Date: 2020/9/16
 */
public enum SexEnums {
	WOMAN(1, "女"),
	MAN(2, "男");

	private final Integer value;
	private final String desc;

	SexEnums(Integer value, String desc) {
		this.value = value;
		this.desc = desc;
	}

    /**
     * 获取描述
     *
     * @param value 值
     * @return desc
     */
    public static String getDesc(Integer value) {
        for (SexEnums enumValue : values()) {
            if (enumValue.value().equals(value)) {
                return enumValue.desc();
            }
        }
        return null;
    }

	/**
	 * 获取值
	 *
	 * @return value
	 */
	public Integer value() {
		return value;
	}

    /**
     * 获取描述
     *
     * @return desc
     */
    public String desc() {
        return desc;
    }

}
