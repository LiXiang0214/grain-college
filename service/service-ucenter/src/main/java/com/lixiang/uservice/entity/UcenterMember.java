package com.lixiang.uservice.entity;

import com.baomidou.mybatisplus.annotation.IdType;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import com.lixiang.commonutils.enums.DelEnums;
import com.lixiang.commonutils.enums.IsDisabledEnums;
import com.lixiang.servicebase.basic.BasicEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 用户信息
 *
 * @author lixiang
 * @since 2020-09-15
 */

/**
 * 使用mybatisPlus时，会确定实体类和数据的映射关系
 * 具体的映射方法有两种：
 * 1、默认：采用驼峰映射规则，例如MyUserTable 对应的数据库表为 my_user_table、TEMyUserTable 对应表名为t_e_my_user_table;
 * 2、注解“@TableName”在类名上方添加@TableName("my_user_table")
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Api(tags = "用户信息")
public class UcenterMember extends BasicEntity {

    @ApiModelProperty(value = "Id")
    @TableId(value = "id", type = IdType.ID_WORKER_STR)
    private String id;

    @ApiModelProperty(value = "微信openid")
    private String openid;

    @ApiModelProperty(value = "手机号")
    private String mobile;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "昵称")
    private String nickname;

    @ApiModelProperty(value = "性别 1 女，2 男")
    private Integer sex;

    @ApiModelProperty(value = "年龄")
    private Integer age;

    @ApiModelProperty(value = "用户头像")
    private String avatar;

    @ApiModelProperty(value = "用户签名")
    private String sign;

    @ApiModelProperty(value = "是否禁用 1（true）已禁用，  0（false）未禁用")
    private Integer isDisabled;

    @ApiModelProperty("是否禁用说明")
    @TableField(exist = false)
    private String isDisabledDesc;

    @ApiModelProperty(value = "逻辑删除 1（true）已删除， 0（false）未删除")
    private Integer isDeleted;

    @ApiModelProperty("删除状态说明")
    @TableField(exist = false)
    private String isDeletedDesc;

    public String getIsDeletedDesc() {
        return DelEnums.getDesc(this.isDeleted);
    }

    public String getIsDisabledDesc() {
        return IsDisabledEnums.getDesc(this.isDisabled);
    }

}
