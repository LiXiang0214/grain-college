package com.lixiang.uservice.entity.vo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * 用户注册
 */
@Data
@Api(tags = "用户注册")
public class RegisterVO {

    @ApiModelProperty(value = "昵称")
    @NotEmpty(message = "请填写昵称")
    private String nickname;

    @ApiModelProperty(value = "手机号")
    @NotEmpty(message = "请填写手机号")
    private String mobile;

    @ApiModelProperty(value = "密码")
    @NotEmpty(message = "请填写密码")
    private String password;

    @ApiModelProperty(value = "验证码")
    @NotEmpty(message = "请填写验证码")
    private String code;

    @ApiModelProperty(value = "性别 1 女，2 男")
    private Integer sex;

    @ApiModelProperty(value = "年龄")
    private Integer age;

    @ApiModelProperty(value = "用户头像")
    private String avatar;

    @ApiModelProperty(value = "用户签名")
    private String sign;

}
