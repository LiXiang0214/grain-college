package com.lixiang.uservice.mapper;

import com.lixiang.uservice.entity.UcenterMember;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 用户中心 Mapper
 *
 * @author lixiang
 * @since 2020-09-15
 */
public interface UcenterMapper extends BaseMapper<UcenterMember> {

    /**
     * 查询某一天注册人数
     *
     * @param date 日期
     * @return 注册人数
     */
    Integer countRegisterDay(String date);

}
