package com.lixiang.uservice.controller;


import com.lixiang.commonutils.basic.ReturnEntity;
import com.lixiang.commonutils.util.JwtUtils;
import com.lixiang.servicebase.entity.UcenterMemberOrder;
import com.lixiang.uservice.entity.UcenterMember;
import com.lixiang.uservice.entity.vo.LoginVO;
import com.lixiang.uservice.entity.vo.RegisterVO;
import com.lixiang.uservice.enums.ApiPath;
import com.lixiang.uservice.service.UcenterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * 前台登录 控制器
 *
 * @author lixiang
 * @since 2020-09-15
 */
@RestController
@RequestMapping(ApiPath.FRONT)
@Api(tags = "前台登录")
@CrossOrigin //解决跨域
public class UcenterController {

    @Autowired
    private UcenterService ucenterService;

    /**
     * 登录
     *
     * @param loginVO {@link LoginVO}
     * @return {@link ReturnEntity}
     */
    @ApiOperation("登录")
    @PostMapping("/login")
    public ReturnEntity login(@RequestBody LoginVO loginVO) {
        String token = ucenterService.login(loginVO);
        return ReturnEntity.success().data("token", token).desc("登录成功");
    }

    /**
     * 注册
     *
     * @param registerVO {@link RegisterVO}
     * @return {@link ReturnEntity}
     */
    @ApiOperation("注册")
    @PostMapping("/register")
    public ReturnEntity register(@RequestBody RegisterVO registerVO) {
        ucenterService.register(registerVO);
        return ReturnEntity.success().desc("注册成功");
    }

    /**
     * 根据token获取用户信息
     *
     * @param request {@link HttpServletRequest}
     * @return {@link ReturnEntity}
     */
    @ApiOperation("根据token获取用户信息")
    @GetMapping("memberInfo")
    public ReturnEntity getMemberInfo(HttpServletRequest request) {
        // 获取用户id
        String memberId = JwtUtils.getMemberIdByJwtToken(request);
        // 查询用户信息
        return ReturnEntity.success().data("userInfo", ucenterService.getById(memberId));
    }

    /**
     * 根据用户ID获取用户信息
     *
     * @param id 用户ID
     * @return 用户信息
     */
    @ApiOperation("根据用户ID获取用户信息")
    @PostMapping("getMemberInfoToOrder/{id}")
    public UcenterMemberOrder getMemberInfoToOrder(@ApiParam("用户ID") @PathVariable("id") String id) {
        UcenterMember memberDB = ucenterService.getById(id);
        UcenterMemberOrder ucenterMemberOrder = new UcenterMemberOrder();
        BeanUtils.copyProperties(memberDB, ucenterMemberOrder);
        return ucenterMemberOrder;
    }

    /**
     * 查询截止某一天注册人数
     *
     * @param date 日期
     * @return 注册人数
     */
    @ApiOperation("截止指定日期册人数")
    @GetMapping("countRegister/{date}")
    public Integer countRegister(@ApiParam("日期") @PathVariable("date") String date) {
        return ucenterService.countRegisterDay(date);
    }

}
