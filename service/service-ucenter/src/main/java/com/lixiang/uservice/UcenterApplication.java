package com.lixiang.uservice;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * Ucenter服务启动类
 */
@SpringBootApplication
@EnableDiscoveryClient  //nacos注册
@EnableFeignClients
@MapperScan("com.lixiang.uservice.mapper")
@ComponentScan("com.lixiang") // 扫描
public class UcenterApplication {

    /**
     * Main 方法
     *
     * @param args 参数
     */
    public static void main(String[] args) {
        SpringApplication.run(UcenterApplication.class, args);
    }

}
