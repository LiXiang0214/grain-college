package com.lixiang.uservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lixiang.commonutils.enums.ResultCode;
import com.lixiang.commonutils.util.JwtUtils;
import com.lixiang.uservice.entity.UcenterMember;
import com.lixiang.servicebase.exceptionhandler.GuliException;
import com.lixiang.uservice.entity.vo.LoginVO;
import com.lixiang.uservice.entity.vo.RegisterVO;
import com.lixiang.commonutils.enums.IsDisabledEnums;
import com.lixiang.uservice.mapper.UcenterMapper;
import com.lixiang.uservice.service.UcenterService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lixiang.uservice.utils.MD5;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * 用户中心 业务层 实现类
 *
 * @author lixiang
 * @since 2020-09-15
 */
@Service
public class UcenterServiceImpl extends ServiceImpl<UcenterMapper, UcenterMember> implements UcenterService {

    @Autowired
    private RedisTemplate<String, String> redis;

    /**
     * 登录
     *
     * @param loginVO {@link LoginVO}
     * @return token
     */
    @Override
    public String login(LoginVO loginVO) {
        // 获取登录信息，进行校验
        String mobile = loginVO.getMobile();
        String password = loginVO.getPassword();

        // 获取用户信息
        UcenterMember ucenterMember = baseMapper.selectOne(new QueryWrapper<UcenterMember>().eq("mobile", mobile));
        if (StringUtils.isEmpty(ucenterMember)) {
            throw new GuliException(ResultCode.ERROR, "用户不存在，请先进行注册喔！");
        }

        // 校验密码
        if (!MD5.encrypt(password).equals(ucenterMember.getPassword())) {
            throw new GuliException(ResultCode.ERROR, "密码输错啦！");
        }

        //校验是否被禁用
        if(IsDisabledEnums.Disabled.equals(ucenterMember.getIsDisabled())) {
            throw new GuliException(ResultCode.ERROR, "Sorry哦，你已经被禁用了！");
        }

        // 生成token
        return JwtUtils.getJwtToken(ucenterMember.getId(), ucenterMember.getNickname());

    }

    /**
     * 注册
     *
     * @param registerVO {@link RegisterVO}
     */
    @Override
    public void register(RegisterVO registerVO) {
        // 获取注册信息，进行校验
        String mobile = registerVO.getMobile();
        String code = registerVO.getCode();

        // 校验校验验证码
        String veriCode = redis.opsForValue().get(mobile); // 从redis获取发送的验证码
        if (!code.equals(veriCode)) {
            throw new GuliException(ResultCode.ERROR, "验证码错误，请重新输入！");
        }

        // 手机号码判重
        Integer count = baseMapper.selectCount(new QueryWrapper<UcenterMember>().eq("mobile", mobile));
        if (count > 0) {
            throw new GuliException(ResultCode.ERROR, "该手机号已经被注册啦！");
        }

        // 保存注册信息到数据库
        UcenterMember ucenterMember = new UcenterMember();
        BeanUtils.copyProperties(registerVO, ucenterMember);
        ucenterMember.setPassword(MD5.encrypt(registerVO.getPassword())); // 密码加密
        ucenterMember.setIsDisabled(IsDisabledEnums.NotDisabled.value());
        ucenterMember.setAvatar("https://lixiang0214.oss-cn-chengdu.aliyuncs.com/%E9%BB%98%E8%AE%A4%E5%A4%B4%E5%83%8F.jpg");
        this.save(ucenterMember);
    }

    /**
     * 根据openid查找用户
     * @param openid openid
     * @return 用户信息
     */
    @Override
    public UcenterMember getOpenIdMember(String openid) {
        QueryWrapper<UcenterMember> wrapper = new QueryWrapper<UcenterMember>().eq("openid", openid);
        UcenterMember ucenterMember = baseMapper.selectOne(wrapper);
        return ucenterMember;
    }

    /**
     * 查询某一天注册人数
     *
     * @param date 日期
     * @return 注册人数
     */
    @Override
    public Integer countRegisterDay(String date) {
        return baseMapper.countRegisterDay(date);
    }

}
