package com.lixiang.videoupload.enums;

/**
 * 路径地址配置
 *
 * @author lixiang
 */
public class ApiPath {

	/**
	 * 阿里Vod根地址
	 */
	public static final String VOD = "/vodservice";

	/**
	 * 视频上传
	 */
	public static final String VIDEO = VOD + "/video";

	protected ApiPath() {

    }

}
