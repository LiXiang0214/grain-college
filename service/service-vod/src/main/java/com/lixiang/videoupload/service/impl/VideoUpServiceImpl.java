package com.lixiang.videoupload.service.impl;

import com.aliyun.vod.upload.impl.UploadVideoImpl;
import com.aliyun.vod.upload.req.UploadStreamRequest;
import com.aliyun.vod.upload.resp.UploadStreamResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.vod.model.v20170321.DeleteVideoRequest;
import com.lixiang.commonutils.enums.ResultCode;
import com.lixiang.servicebase.exceptionhandler.GuliException;
import com.lixiang.videoupload.service.VideoUpService;
import com.lixiang.videoupload.utils.ConstantVodUtils;
import com.lixiang.videoupload.utils.InitVodCilent;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * 视频上传 业务层实现
 *
 * @author lixiang
 * @since 2020-08-27
 */
@Service
public class VideoUpServiceImpl implements VideoUpService {

    @Override
    public String uploadVideo(MultipartFile file) {
        try {
            // 上传文件原始名称
            String fileName = file.getOriginalFilename();
            // 上传之后显示名称
            String uploadName = fileName.substring(0, fileName.lastIndexOf("."));
            // 上传文件输入流
            InputStream fileIinputStream = file.getInputStream();

            // 上传
            UploadStreamRequest uploadRequest = new UploadStreamRequest(
                    ConstantVodUtils.ACCESS_KEY_ID, ConstantVodUtils.ACCESS_KEY_SECRET, uploadName, fileName, fileIinputStream);
            UploadStreamResponse uploadResponse = new UploadVideoImpl().uploadStream(uploadRequest);

            // 返回文件ID
            String videoId;
            if (uploadResponse.isSuccess()) {
                videoId = uploadResponse.getVideoId();
            } else { //如果设置回调URL无效，不影响视频上传，可以返回VideoId同时会返回错误码
                     // 其他情况上传失败时，VideoId为空，此时需要根据返回错误码分析具体错误原因
                videoId = uploadResponse.getVideoId();
            }
            return videoId;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void deleteVideo(String id) {
        try {
            //初始化对象
            DefaultAcsClient client = InitVodCilent.initVodClient(
                    ConstantVodUtils.ACCESS_KEY_ID, ConstantVodUtils.ACCESS_KEY_SECRET);
            //创建删除视频request对象
            DeleteVideoRequest request = new DeleteVideoRequest();
            //向request设置视频id
            request.setVideoIds(id);
            //调用初始化对象的方法实现删除
            client.getAcsResponse(request);
        }catch(Exception e) {
            e.printStackTrace();
            throw new GuliException(ResultCode.ERROR, "删除视频失败,");
        }
    }

    @Override
    public void deleteVideos(List<String> videoIdList) {
        try {
            //初始化对象
            DefaultAcsClient client = InitVodCilent.initVodClient(
                    ConstantVodUtils.ACCESS_KEY_ID, ConstantVodUtils.ACCESS_KEY_SECRET);
            //创建删除视频request对象
            DeleteVideoRequest request = new DeleteVideoRequest();

            //videoIdList值转换成 字符串
            String videoIds = StringUtils.join(videoIdList.toArray(), ",");
            //向request设置视频id
            request.setVideoIds(videoIds);

            //调用初始化对象的方法实现删除
            client.getAcsResponse(request);
        }catch(Exception e) {
            e.printStackTrace();
            throw new GuliException(ResultCode.ERROR, "删除视频失败,");
        }
    }

}
