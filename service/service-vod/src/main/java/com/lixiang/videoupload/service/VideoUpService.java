package com.lixiang.videoupload.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 视频上传 业务层
 *
 * @author lixiang
 * @since 2020-08-27
 */
public interface VideoUpService {

    /**
     * 上传视频到阿里云
     *
     * @param file 视频文件
     * @return 返回上传视频id
     */
    String uploadVideo(MultipartFile file);

    /**
     * 删除阿里云视频
     *
     * @param id 视频id
     */
    void deleteVideo(String id);

    /**
     * 删除多个阿里云视频
     *
     * @param videoIdList 多个视频id List
     */
    void deleteVideos(List<String> videoIdList);

}
