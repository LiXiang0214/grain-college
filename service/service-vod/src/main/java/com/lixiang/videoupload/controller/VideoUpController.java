package com.lixiang.videoupload.controller;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.vod.model.v20170321.GetVideoPlayAuthRequest;
import com.aliyuncs.vod.model.v20170321.GetVideoPlayAuthResponse;
import com.lixiang.commonutils.basic.ReturnEntity;
import com.lixiang.servicebase.exceptionhandler.GuliException;
import com.lixiang.videoupload.enums.ApiPath;
import com.lixiang.videoupload.service.VideoUpService;
import com.lixiang.videoupload.utils.ConstantVodUtils;
import com.lixiang.videoupload.utils.InitVodCilent;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 视频 控制器
 *
 * @author lixiang
 * @since 2020-08-27
 */
@Api(tags = "视频管理")
@RestController
@RequestMapping(ApiPath.VIDEO)
@CrossOrigin //解决跨域
public class VideoUpController {

    @Autowired
    private VideoUpService videoUpService;

    /**
     * 上传视频到阿里云
     *
     * @param file 视频文件
     * @return {@link ReturnEntity}
     */
    @ApiOperation("上传视频到阿里云")
    @PostMapping
    public ReturnEntity uploadVideo(MultipartFile file) {
        String videoId = videoUpService.uploadVideo(file);
        return ReturnEntity.success().data("videoId", videoId).desc("上传成功");
    }

    /**
     * 删除阿里云视频
     *
     * @param id 视频id
     * @return {@link ReturnEntity}
     */
    @ApiOperation("删除阿里云视频")
    @DeleteMapping("{id}")
    public ReturnEntity deleteVideo(@ApiParam("视频id") @PathVariable("id") String id) {
        videoUpService.deleteVideo(id);
        return ReturnEntity.success();
    }

    /**
     * 删除多个阿里云视频
     *
     * @param videoIdList 多个视频id List
     * @return {@link ReturnEntity}
     */
    @ApiOperation("删除多个阿里云视频")
    @DeleteMapping("deleteBatch")
    public ReturnEntity deleteVideos(@RequestParam("videoIdList") List<String> videoIdList) {
        videoUpService.deleteVideos(videoIdList);
        return ReturnEntity.success();
    }

    /**
     * 根据视频id获取视频凭证
     *
     * @param vodId 视频id
     * @return 视频凭证
     */
    @ApiOperation("获取视频凭证")
    @GetMapping("/playAuth/{vodId}")
    public ReturnEntity getPlayAuth(@ApiParam("视频id") @PathVariable("vodId") String vodId) {
        try {
            // 创建初始化对象
            DefaultAcsClient client = InitVodCilent.initVodClient(
                    ConstantVodUtils.ACCESS_KEY_ID, ConstantVodUtils.ACCESS_KEY_SECRET);
            // 创建 request 对象
            GetVideoPlayAuthRequest request = new GetVideoPlayAuthRequest();
            request.setVideoId(vodId); // 向request设置 视频ID
            // 创建 response 对象
            GetVideoPlayAuthResponse response = client.getAcsResponse(request);
            // 调用方法得到凭证
            String playAuth = response.getPlayAuth();
            return ReturnEntity.success().data("playAuth", playAuth);
        }catch(Exception e) {
            e.printStackTrace();
            throw new GuliException(20001, "视频播放失败，请刷新重试！");
        }
    }

}

