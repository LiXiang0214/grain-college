package com.lixiang.videoupload.utils;

import com.aliyun.oss.ClientException;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.profile.DefaultProfile;

/**
 * 初始化
 */
public class InitVodCilent {

    /**
     * 初始化
     *
     * @param accessKeyId ID
     * @param accessKeySecret 密钥
     * @return {@link DefaultAcsClient}
     * @throws ClientException {@link ClientException}
     */
    public static DefaultAcsClient initVodClient(String accessKeyId, String accessKeySecret) throws ClientException {
        String regionId = "cn-shanghai";  // 点播服务接入区域
        DefaultProfile profile = DefaultProfile.getProfile(regionId, accessKeyId, accessKeySecret);
        DefaultAcsClient client = new DefaultAcsClient(profile);
        return client;
    }

}
