package com.lixiang.ossservice.controller;

import com.lixiang.commonutils.basic.ReturnEntity;
import com.lixiang.ossservice.enums.ApiPath;
import com.lixiang.ossservice.service.OssFileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * OSS 控制器
 *
 * @author lixiang
 * @since 2020-08-27
 */
@Api(tags = "阿里OSS")
@RestController
@RequestMapping(ApiPath.OSSFILE)
@CrossOrigin
public class OssFileController {

    @Autowired
    private OssFileService ossFileService;

    /**
     * 上传文件
     *
     * @param file {@link MultipartFile}
     * @return 文件路径
     */
    @ApiOperation("上传文件")
    @PostMapping
    public ReturnEntity uploadFile(MultipartFile file) {
        // 返回上传到oss的路径
        String url = ossFileService.uploadFile(file);
        return ReturnEntity.success().data("url", url);
    }

}
