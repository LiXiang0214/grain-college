package com.lixiang.ossservice.enums;

/**
 * 路径地址配置
 *
 * @author lixiang
 */
public class ApiPath {

	/**
	 * 阿里OSS根地址
	 */
	public static final String OSS = "/ossservice";

	/**
	 * 文件上传
	 */
	public static final String OSSFILE = OSS + "/ossfile";

	protected ApiPath() {

    }

}
