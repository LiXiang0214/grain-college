package com.lixiang.ossservice.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * OOS文件上传 业务层
 */
public interface OssFileService {

    /**
     * 上传文件
     *
     * @param file {@link MultipartFile}
     * @return 文件路径
     */
    String uploadFile(MultipartFile file);

}
