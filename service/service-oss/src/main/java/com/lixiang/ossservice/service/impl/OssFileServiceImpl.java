package com.lixiang.ossservice.service.impl;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.lixiang.ossservice.service.OssFileService;
import com.lixiang.ossservice.utils.ConstantPropertiesUtils;
import org.joda.time.LocalDateTime;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.UUID;

/**
 * OOS文件上传 业务层 实现类
 */
@Service
public class OssFileServiceImpl implements OssFileService {

    @Override
    public String uploadFile(MultipartFile file) {

        // 工具类获取值
        String endpoint = ConstantPropertiesUtils.END_POIND;
        String accessKeyId = ConstantPropertiesUtils.ACCESS_KEY_ID;
        String accessKeySecret = ConstantPropertiesUtils.ACCESS_KEY_SECRET;
        String bucketName = ConstantPropertiesUtils.BUCKET_NAME;

        try {
            // 1、创建OSS实例
            OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

            // 2、获取上传文件输入流
            InputStream inputStream = file.getInputStream();
            // 2.1、获取文件名称
            String fileName = file.getOriginalFilename();

            // 2.2、在文件名称里面添加随机唯一的值
            fileName = UUID.randomUUID().toString().replaceAll("-","") + fileName;

            // 2.3、把文件按照日期进行分类
            fileName = LocalDateTime.now().toString("yyyy/MM/dd") + "/" + fileName;

            // 3、调用oss方法实现上传
            ossClient.putObject(bucketName, fileName , inputStream); // Bucket名称、上传路径和文件名、上传文件输入流

            // 4、关闭OSSClient。
            ossClient.shutdown();

            // 5、拼接上传后的文件路径返回
            return "https://" + bucketName + "." + endpoint + "/" + fileName;
        }catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
