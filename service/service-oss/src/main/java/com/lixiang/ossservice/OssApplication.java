package com.lixiang.ossservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * OSS服务启动类
 */
@SpringBootApplication(exclude= {DataSourceAutoConfiguration.class})
@EnableDiscoveryClient  //nacos注册
@ComponentScan(basePackages = {"com.lixiang"}) // 扫描
public class OssApplication {

    /**
     * Main 方法
     *
     * @param args 参数
     */
    public static void main(String[] args) {
        SpringApplication.run(OssApplication.class,args);
    }

}
