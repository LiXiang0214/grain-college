package com.lixiang.bannerservice.service;

import com.lixiang.bannerservice.entity.CrmBanner;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lixiang.bannerservice.entity.vo.BannerQueryVO;

import java.util.List;
import java.util.Map;

/**
 * 首页banner业务层
 *
 * @author lixiang
 * @since 2020-09-15
 */
public interface CrmBannerService extends IService<CrmBanner> {

    /**
     * 查询所有banner
     *
     * @return Banner集合
     */
    List<CrmBanner> selectAllBanner();

    /**
     * 查询banner列表
     *
     * @param bannerQueryVO {@link BannerQueryVO}
     * @return Banner集合
     */
    Map<String, Object> pageBanner(BannerQueryVO bannerQueryVO);

    /**
     * 上架Banner
     *
     * @param id Banner ID
     * @return 是否成功
     */
    boolean up(String id);

    /**
     * 下架Banner
     *
     * @param id Banner ID
     * @return 是否成功
     */
    boolean down(String id);

}
