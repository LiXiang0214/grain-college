package com.lixiang.bannerservice.enums;

/**
 * 路径地址配置
 *
 * @author lixiang
 */
public class ApiPath {

	/**
	 * 首页banner根地址
	 */
	public static final String ROOT = "/bannerservice";

	/**
	 * 后台
	 */
	public static final String ADMIN = ROOT + "/bannerAdmin";

	/**
	 * 前台
	 */
	public static final String FRONT = ROOT + "/front";

	protected ApiPath() {

    }

}
