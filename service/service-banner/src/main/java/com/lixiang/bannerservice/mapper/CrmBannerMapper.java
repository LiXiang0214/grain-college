package com.lixiang.bannerservice.mapper;

import com.lixiang.bannerservice.entity.CrmBanner;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 首页banner DAO层
 *
 * @author lixiang
 * @since 2020-09-15
 */
public interface CrmBannerMapper extends BaseMapper<CrmBanner> {

}
