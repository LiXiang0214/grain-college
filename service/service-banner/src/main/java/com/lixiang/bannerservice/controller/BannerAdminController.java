package com.lixiang.bannerservice.controller;

import com.lixiang.bannerservice.entity.CrmBanner;
import com.lixiang.bannerservice.entity.vo.BannerQueryVO;
import com.lixiang.bannerservice.entity.vo.BannerVO;
import com.lixiang.bannerservice.enums.ApiPath;
import com.lixiang.bannerservice.service.CrmBannerService;
import com.lixiang.commonutils.basic.ReturnEntity;
import com.lixiang.commonutils.enums.DelEnums;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 首页Banner后台 控制器
 *
 * @author lixiang
 * @since 2020-09-15
 */
@Api(tags = "首页Banner[后台]")
@RestController
@RequestMapping(ApiPath.ADMIN)
@CrossOrigin
public class BannerAdminController {

    @Autowired
    private CrmBannerService bannerService;

    /**
     * 查询banner列表
     *
     * @param bannerQueryVO {@link BannerQueryVO}
     * @return {@link ReturnEntity}
     */
    @ApiOperation("banner列表")
    @PutMapping
    public ReturnEntity pageBanner(@RequestBody BannerQueryVO bannerQueryVO) {
        return ReturnEntity.success().data(bannerService.pageBanner(bannerQueryVO));
    }

    /**
     * 添加banner
     *
     * @param bannerVO {@link BannerVO}
     * @return {@link ReturnEntity}
     */
    @ApiOperation("添加banner")
    @PostMapping
    public ReturnEntity addBanner(@RequestBody BannerVO bannerVO) {
        CrmBanner banner = new CrmBanner();
        BeanUtils.copyProperties(bannerVO, banner);
        banner.setIsDeleted(DelEnums.Normal.value());
        if(bannerService.save(banner)) {
            return ReturnEntity.success().desc("添加成功");
        } else {
            return ReturnEntity.fail().desc("添加失败");
        }
    }

    /**
     * 获取Banner
     *
     * @param id Banner ID
     * @return {@link ReturnEntity}
     */
    @ApiOperation("获取Banner")
    @GetMapping("{id}")
    public ReturnEntity<CrmBanner> get(@ApiParam("ID") @PathVariable("id") String id) {
        return ReturnEntity.success().data("banner", bannerService.getById(id));
    }

    /**
     * 更新Banner
     *
     * @param id Banner ID
     * @param bannerVO {@link BannerVO}
     * @return {@link ReturnEntity}
     */
    @ApiOperation("更新Banner")
    @PostMapping("{id}")
    public ReturnEntity updateBanner(@ApiParam("ID") @PathVariable("id") String id,
                                     @RequestBody BannerVO bannerVO) {
        CrmBanner bannerDB = bannerService.getById(id);
        BeanUtils.copyProperties(bannerVO, bannerDB);
        if(bannerService.updateById(bannerDB)) {
            return ReturnEntity.success().desc("更新成功");
        } else {
            return ReturnEntity.fail().desc("更新失败");
        }
    }

    /**
     * 下架Banner
     *
     * @param id Banner ID
     * @return {@link ReturnEntity}
     */
    @ApiOperation("下架Banner")
    @DeleteMapping("down/{id}")
    public ReturnEntity down(@ApiParam("ID") @PathVariable("id") String id) {
        if(bannerService.down(id)) {
            return ReturnEntity.success().desc("下架成功");
        } else {
            return ReturnEntity.fail().desc("下架失败");
        }
    }

    /**
     * 上架Banner
     *
     * @param id Banner ID
     * @return {@link ReturnEntity}
     */
    @ApiOperation("上架Banner")
    @PostMapping("up/{id}")
    public ReturnEntity up(@ApiParam("ID") @PathVariable("id") String id) {
        if(bannerService.up(id)) {
            return ReturnEntity.success().desc("上架成功");
        } else {
            return ReturnEntity.fail().desc("上架失败");
        }
    }

}

