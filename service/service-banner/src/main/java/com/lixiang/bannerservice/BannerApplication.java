package com.lixiang.bannerservice;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * Banner服务启动类
 */
@SpringBootApplication
@EnableDiscoveryClient  //nacos注册
@MapperScan("com.lixiang.bannerservice.mapper")
@ComponentScan("com.lixiang") // 扫描
public class BannerApplication {

    /**
     * Main 方法
     *
     * @param args 参数
     */
    public static void main(String[] args) {
        SpringApplication.run(BannerApplication.class, args);
    }

}
