package com.lixiang.bannerservice.entity.vo;

import com.lixiang.servicebase.basic.BaseQueryVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 查询Banner
 */
@Data
public class BannerQueryVO extends BaseQueryVO {

    @ApiModelProperty(value = "标题[模糊查询]")
    private String title;

    @ApiModelProperty(value = "查询开始时间", example = "2019-01-01 10:10:10")
    private String beginTime;

    @ApiModelProperty(value = "查询结束时间", example = "2019-12-01 10:10:10")
    private String endTime;

}
