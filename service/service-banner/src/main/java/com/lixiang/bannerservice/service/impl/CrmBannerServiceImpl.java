package com.lixiang.bannerservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lixiang.bannerservice.entity.CrmBanner;
import com.lixiang.bannerservice.entity.vo.BannerQueryVO;
import com.lixiang.bannerservice.mapper.CrmBannerMapper;
import com.lixiang.bannerservice.service.CrmBannerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lixiang.commonutils.enums.DelEnums;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 首页banner业务层 实现类
 *
 * @author lixiang
 * @since 2020-09-15
 */
@Service
public class CrmBannerServiceImpl extends ServiceImpl<CrmBannerMapper, CrmBanner> implements CrmBannerService {

    /**
     * 查询所有banner
     *
     * @return Banner集合
     */
    @Cacheable(value = "banner", key = "'allBanner'")
    @Override
    public List<CrmBanner> selectAllBanner() {
        //根据id进行降序排列，显示排列之后前两条记录
        QueryWrapper<CrmBanner> bannerWrapper = new QueryWrapper<>();
        bannerWrapper.orderByAsc("sort")
                     .last("limit 2");
        List<CrmBanner> bannerList = baseMapper.selectList(bannerWrapper);
        return bannerList;
    }

    /**
     * 查询banner列表
     *
     * @param bannerQueryVO {@link BannerQueryVO}
     * @return Banner集合
     */
    @Override
    public Map<String, Object> pageBanner(BannerQueryVO bannerQueryVO) {
        Map<String, Object> bannerMap = new HashMap<>();

        //创建page对象
        Page<CrmBanner> pageBanner = new Page<>(bannerQueryVO.getPage(), bannerQueryVO.getLimit());
        //构建条件
        QueryWrapper<CrmBanner> bannerWrapper = new QueryWrapper<>();
        if(!StringUtils.isEmpty(bannerQueryVO.getTitle())) {
            bannerWrapper.like("title", bannerQueryVO.getTitle());
        }
//        bannerWrapper.eq("is_deleted", DelEnums.Normal.value());
        // 排序
        bannerWrapper.orderByAsc("sort");
        //调用方法实现条件查询分页
        page(pageBanner, bannerWrapper);
        List<CrmBanner> bannerList = pageBanner.getRecords(); // 数据list集合
        Integer total = bannerList.size(); // 总记录数

        bannerMap.put("total", total);
        bannerMap.put("bannerList", bannerList);
        return bannerMap;
    }

    /**
     * 上架Banner
     *
     * @param id Banner ID
     * @return 是否成功
     */
    @Override
    public boolean up(String id) {
        CrmBanner bannerDB = baseMapper.selectById(id);
        bannerDB.setIsDeleted(DelEnums.Normal.value());
        return baseMapper.updateById(bannerDB) > 0;
    }

    /**
     * 下架Banner
     *
     * @param id Banner ID
     * @return 是否成功
     */
    @Override
    public boolean down(String id) {
        CrmBanner bannerDB = baseMapper.selectById(id);
        bannerDB.setIsDeleted(DelEnums.Delete.value());
        return baseMapper.updateById(bannerDB) > 0;
    }

}
