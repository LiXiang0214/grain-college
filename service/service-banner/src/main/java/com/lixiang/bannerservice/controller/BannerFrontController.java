package com.lixiang.bannerservice.controller;

import com.lixiang.bannerservice.enums.ApiPath;
import com.lixiang.bannerservice.service.CrmBannerService;
import com.lixiang.commonutils.basic.ReturnEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 首页Banner前台 控制器
 *
 * @author lixiang
 * @since 2020-09-15
 */
@Api(tags = "首页Banner[前台]")
@RestController
@RequestMapping(ApiPath.FRONT)
@CrossOrigin
public class BannerFrontController {

    @Autowired
    private CrmBannerService bannerService;

    /**
     * 查询所有banner
     *
     * @return {@link ReturnEntity}
     */
    @ApiOperation("查询所有banner")
    @GetMapping
    public ReturnEntity getAllBanner() {
        return ReturnEntity.success().data("bannerList", bannerService.selectAllBanner());
    }

}

