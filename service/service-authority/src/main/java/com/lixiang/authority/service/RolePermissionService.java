package com.lixiang.authority.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lixiang.authority.entity.RolePermission;


/**
 * 权限控制[角色-菜单] 服务接口
 *
 * @author lixiang
 * @since 2021-01-26 12:49:36
 */
public interface RolePermissionService extends IService<RolePermission> {

}
