package com.lixiang.authority.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lixiang.authority.entity.Permission;
import com.lixiang.authority.entity.RolePermission;
import com.lixiang.authority.entity.User;
import com.lixiang.authority.helper.MemuHelper;
import com.lixiang.authority.helper.PermissionHelper;
import com.lixiang.authority.mapper.PermissionMapper;
import com.lixiang.authority.service.PermissionService;
import com.lixiang.authority.service.RolePermissionService;
import com.lixiang.authority.service.UserService;
import com.lixiang.commonutils.enums.DelEnums;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 权限管理[菜单] 服务实现类
 *
 * @author lixiang
 * @since 2021-01-26 12:52:22
 */
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements PermissionService {

    @Autowired
    private PermissionMapper permissionMapper;

    @Autowired
    private RolePermissionService rolePermissionService;

    @Autowired
    private UserService userService;

    /**
     * 获取菜单树
     *
     * @return 菜单树集合
     */
    @Override
    public List<Permission> getPermissionTree() {
        // 0、创建list集合，用于数据最终封装
        List<Permission> resultList = new ArrayList<>();

        // 1、查询菜单表所有数据
        QueryWrapper<Permission> wrapper = new QueryWrapper<Permission>();
        wrapper.eq("is_deleted", 0);
        wrapper.orderByAsc("id");
        List<Permission> permissionDBList = baseMapper.selectList(wrapper);

        // 2、遍历permissionDBList，递归设置子菜单
        permissionDBList.forEach(permissionDB -> {
            if ("0".equals(permissionDB.getPid())) {
                permissionDB.setLevel(1); // 顶层菜单的level
                // 根据顶层菜单，向里面进行递归查询子菜单，并封装
                resultList.add(getChidrenPer(permissionDB, permissionDBList));
            }
        });

        return resultList;
    }

    /**
     * 递归查找子菜单
     *
     * @param permissionDB 父菜单
     * @param permissionDBList 菜单集合
     * @return 完成查找子菜单的菜单
     */
    private Permission getChidrenPer(Permission permissionDB, List<Permission> permissionDBList) {
        // 1、初始化子菜单集合
        permissionDB.setChildrenList(new ArrayList<>());

        // 2、循环以查找子菜单
        for(Permission nodePer : permissionDBList) {
            if (permissionDB.getId().equals(nodePer.getPid())) { // 父子菜单
                nodePer.setLevel(permissionDB.getLevel() + 1); // 父菜单Level + 1
                // 3、继续递归调用
                Permission nodeChidrenPer = getChidrenPer(nodePer, permissionDBList);
                permissionDB.getChildrenList().add(nodeChidrenPer);
            }
        }

        return permissionDB;
    }

    /**
     * 递归删除菜单
     *
     * @param permissionId 菜单ID
     */
    @Override
    public void removePermissionRecursion(String permissionId) {
        // 1、存放要删除的菜单的id
        List<String> idList = new ArrayList<>();

        // 2、递归查找所有ID
        selectChildListById(permissionId, idList);

        // 3、将当前菜单ID放到idList中
        idList.add(permissionId);

        // 4、统一删除
        baseMapper.deleteBatchIds(idList);
    }

    /**
     * 递归查找指定菜单的所有子菜单ID
     *
     * @param permissionId 菜单ID
     * @param idList 子菜单ID集合
     */
    private void selectChildListById(String permissionId, List<String> idList) {
        // 根据父菜单ID过滤并且只查询“id”这一字段
        QueryWrapper<Permission> wrapper = new QueryWrapper<Permission>().eq("pid", permissionId).select("id");
        List<Permission> childList = baseMapper.selectList(wrapper);

        // 遍历childList，将ID设置到idList，并递归查询
        childList.forEach(child -> {
            idList.add(child.getId());
            selectChildListById(child.getId(), idList);
        });
    }

    /**
     * 给角色分配权限
     *
     * @param roleId 角色ID
     * @param permissionIds 权限ID集合
     */
    @Override
    public void assignForRole(String roleId, String[] permissionIds) {
        // 1、存放保存数据
        List<RolePermission> saveList = new ArrayList<>();

        // 2、遍历菜单ID数组以生成角色-菜单对象
        for (String permissionId : permissionIds) {
            if(StringUtils.isEmpty(permissionId)) {
                continue;
            }
            RolePermission rolePermission = new RolePermission();
            rolePermission.setRoleId(roleId);
            rolePermission.setPermissionId(permissionId);
            rolePermission.setIsDeleted(DelEnums.Normal.value());
            saveList.add(rolePermission);
        }

        // 3、保存到角色菜单关系表
        rolePermissionService.saveBatch(saveList);
    }

    /**
     * 根据角色获取菜单
     *
     * @param roleId 角色ID
     * @return 菜单集合
     */
    @Override
    public List<Permission> getAssignByRole(String roleId) {
        return null;
    }

    /**
     * 根据角色获取菜单值集合
     *
     * @param userId 角色ID
     * @return 菜单集合
     */
    @Override
    public List<String> selectPermissionValueByUserId(String userId) {
        List<String> selectPermissionValueList = null;
        if(this.isSysAdmin(userId)) { // 系统管理员 -> 获取所有权限
            selectPermissionValueList = baseMapper.selectAllPermissionValue();
        } else {
            selectPermissionValueList = baseMapper.selectPermissionValueByUserId(userId);
        }
        return selectPermissionValueList;
    }

    /**
     * 根据用户ID获取菜单信息
     *
     * @param userId 用户ID
     * @return 操作权限值集合
     */
    @Override
    public List<JSONObject> selectPermissionByUserId(String userId) {
        List<Permission> selectPermissionList = null;
        if(this.isSysAdmin(userId)) { // 系统管理员 -> 获取所有权限
            selectPermissionList = baseMapper.selectList(null);
        } else {
            selectPermissionList = baseMapper.selectPermissionByUserId(userId);
        }

        List<Permission> permissionList = PermissionHelper.bulid(selectPermissionList);
        List<JSONObject> resultJsobList = MemuHelper.bulid(permissionList);
        return resultJsobList;
    }

    /**
     * 判断用户是否系统管理员
     *
     * @param userId 用户ID
     * @return true：是，false：不是
     */
    private boolean isSysAdmin(String userId) {
        User user = userService.getById(userId);
        if(null != user && "admin".equals(user.getUsername())) {
            return true;
        }
        return false;
    }

}
