package com.lixiang.authority.helper;


import com.lixiang.authority.entity.Permission;

import java.util.ArrayList;
import java.util.List;

/**
 * 根据权限数据构建菜单数据
 */
public class PermissionHelper {

    /**
     * 使用递归方法建菜单
     *
     * @param treeNodes 菜单树节点
     * @return 结果参数
     */
    public static List<Permission> bulid(List<Permission> treeNodes) {
        List<Permission> trees = new ArrayList<>();
        for (Permission treeNode : treeNodes) {
            if ("0".equals(treeNode.getPid())) {
                treeNode.setLevel(1);
                trees.add(findChildren(treeNode,treeNodes));
            }
        }
        return trees;
    }

    /**
     * 递归查找子节点
     *
     * @param treeNode 节点
     * @param treeNodes 菜单树节点
     * @return 菜单
     */
    public static Permission findChildren(Permission treeNode, List<Permission> treeNodes) {
        treeNode.setChildrenList(new ArrayList<Permission>());

        for (Permission it : treeNodes) {
            if(treeNode.getId().equals(it.getPid())) {
                int level = treeNode.getLevel() + 1;
                it.setLevel(level);
                if (treeNode.getChildrenList() == null) {
                    treeNode.setChildrenList(new ArrayList<>());
                }
                treeNode.getChildrenList().add(findChildren(it,treeNodes));
            }
        }
        return treeNode;
    }

}
