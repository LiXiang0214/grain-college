package com.lixiang.authority.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lixiang.authority.entity.RolePermission;
import com.lixiang.authority.mapper.RolePermissionMapper;
import com.lixiang.authority.service.RolePermissionService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 权限控制[角色-菜单] 服务实现类
 *
 * @author lixiang
 * @since 2021-01-26 12:52:22
 */
@Service
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionMapper, RolePermission> implements RolePermissionService {

    @Resource
    private RolePermissionMapper aclRolePermissionMapper;

}
