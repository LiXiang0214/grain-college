package com.lixiang.authority.controller;

import com.lixiang.authority.entity.Permission;
import com.lixiang.authority.entity.vo.PermissionVO;
import com.lixiang.authority.enums.ApiPath;
import com.lixiang.authority.service.PermissionService;
import com.lixiang.commonutils.basic.ReturnEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 权限控制[菜单] 控制层
 *
 * @author lixiang
 * @since 2021-01-26 12:49:31
 */
@Api(tags = "权限控制[菜单]管理")
@RestController
@RequestMapping(ApiPath.PERMISSION)
@CrossOrigin //解决跨域
public class PermissionController {

    @Autowired
    private PermissionService permissionService;

    /**
     * 新增菜单
     *
     * @param permissionVO 新增参数
     * @return {@link ReturnEntity}
     */
    @ApiOperation(value = "新增菜单")
    @PostMapping("/insert")
    public ReturnEntity insert(@RequestBody PermissionVO permissionVO) {
        Permission permission = new Permission();
        BeanUtils.copyProperties(permissionVO, permission);
        if (this.permissionService.save(permission)) {
            return ReturnEntity.success().desc("新增菜单成功！");
        }
        return ReturnEntity.fail().desc("新增菜单失败！");
    }

    /**
     * 更新菜单
     *
     * @param permissionId 菜单ID
     * @param permissionVO 更新参数
     * @return {@link ReturnEntity}
     */
    @ApiOperation(value = "更新菜单")
    @PostMapping("/{permissionId}/update")
    public ReturnEntity updateSelective(@ApiParam("菜单ID") @PathVariable("permissionId")String permissionId,
                                        @RequestBody PermissionVO permissionVO) {
        Permission permission = permissionService.getById(permissionId);
        BeanUtils.copyProperties(permissionVO, permission);
        if (this.permissionService.updateById(permission)) {
            return ReturnEntity.success().desc("更新菜单成功！");
        }
        return ReturnEntity.fail().desc("更新菜单失败！");
    }

    /**
     * 获取菜单树
     *
     * @return {@link ReturnEntity}
     */
    @ApiOperation(value = "获取菜单树")
    @GetMapping("/tree")
    public ReturnEntity<List<Permission>> getPermissionTree() {
        List<Permission> permissionTreeList =  permissionService.getPermissionTree();
        return ReturnEntity.success().data("permissionTree", permissionTreeList);
    }

    /**
     * 递归删除菜单
     *
     * @param permissionId 菜单ID
     * @return {@link ReturnEntity}
     */
    @ApiOperation(value = "递归删除菜单")
    @DeleteMapping("/{permissionId}/remove-recursion")
    public ReturnEntity removePermissionRecursion(@ApiParam("菜单ID") @PathVariable("permissionId")String permissionId) {
        permissionService.removePermissionRecursion(permissionId);
        return ReturnEntity.success().desc("删除菜单成功！");
    }

    /**
     * 为角色分配权限
     *
     * @param roleId 角色ID
     * @param permissionIds 权限ID集合
     * @return {@link ReturnEntity}
     */
    @ApiOperation(value = "为角色分配权限")
    @PostMapping("/assignForRole")
    public ReturnEntity assignForRole(@ApiParam("角色ID") @RequestParam String roleId,
                                      @ApiParam("权限ID集合") @RequestParam String[] permissionIds) {
        permissionService.assignForRole(roleId, permissionIds);
        return ReturnEntity.success().desc("分配权限成功！");
    }

    /**
     * 根据角色获取菜单
     *
     * @param roleId 角色ID
     * @return {@link ReturnEntity}
     */
    @ApiOperation(value = "根据角色获取菜单")
    @GetMapping("/{roleId}/getAssignByRole")
    public ReturnEntity<List<Permission>> getAssignByRole(@ApiParam("角色ID") @PathVariable("roleId")String roleId) {
        List<Permission> permissionList = permissionService.getAssignByRole(roleId);
        return ReturnEntity.success().data("permissionList", permissionList);
    }

}
