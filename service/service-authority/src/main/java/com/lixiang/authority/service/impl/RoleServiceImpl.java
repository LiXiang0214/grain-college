package com.lixiang.authority.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lixiang.authority.entity.Role;
import com.lixiang.authority.entity.UserRole;
import com.lixiang.authority.entity.vo.RoleQueryVO;
import com.lixiang.authority.mapper.RoleMapper;
import com.lixiang.authority.service.RoleService;
import com.lixiang.authority.service.UserRoleService;
import com.lixiang.commonutils.enums.DelEnums;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 权限控制[角色] 服务实现类
 *
 * @author lixiang
 * @since 2021-01-26 12:52:22
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

    @Resource
    private RoleMapper aclRoleMapper;

    @Resource
    private UserRoleService userRoleService;

    @Resource
    private RoleService roleService;

    /**
     * 获取角色分页列表
     *
     * @param queryVO 查询参数
     * @return 列表信息
     */
    @Override
    public Map roleList(RoleQueryVO queryVO) {
        Map<String, Object> roleMap = new HashMap<>();
        // 1、创建page对象
        Page<Role> rolePage = new Page<>(queryVO.getPage(), queryVO.getLimit());

        // 2、查询条件
        QueryWrapper<Role> roleWrapper = new QueryWrapper<>();
        roleWrapper.eq("isDeleted", DelEnums.Normal.value());
        if(!StringUtils.isEmpty(queryVO.getKeyWord())) {
            roleWrapper.and(wrapper ->
                    wrapper.like("roleName", queryVO.getKeyWord()).or()
                            .like("remark", queryVO.getKeyWord()));
        }

        // 3、排序
        roleWrapper.orderByAsc("gmtCreate");

        // 4、调用方法实现条件查询分页
        page(rolePage, roleWrapper);
        long total = rolePage.getTotal(); // 总记录数
        List<Role> roleList = rolePage.getRecords(); // 数据list集合
        roleMap.put("total", total);
        roleMap.put("rows", roleList);

        return roleMap;
    }

    /**
     * 获取用户角色信息
     * 查询所有角色结合及用户拥有的角色集合
     *
     * @param userId 用户ID
     * @return 用户角色信息
     */
    @Override
    public Map<String, Object> getAssignInfoByUserId(String userId) {
        // 1、查询所有角色
        List<Role> allRoleList = roleService.list(new QueryWrapper<Role>().eq("isDeleted", DelEnums.Normal.value()));

        // 2、查询用户拥有的角色的ID集合
        List<UserRole> userRoleList = userRoleService.list(
                new QueryWrapper<UserRole>()
                        .eq("user_id", userId)
                        .eq("isDeleted", DelEnums.Normal.value())
                        .select("role_id"));
        List<String> userRoleIdList = userRoleList.stream().map(userRole -> userRole.getRoleId()).collect(Collectors.toList());

        // 3、用户拥有的角色集合
        List<Role> assignRoleList = allRoleList.stream().filter(role -> {
            return userRoleIdList.contains(role.getId());
        }).collect(Collectors.toList());

        Map<String, Object> assignInfoMap = new HashMap<>();
        assignInfoMap.put("allRoleList", allRoleList);
        assignInfoMap.put("assignRoleList", assignRoleList);

        return assignInfoMap;
    }

    /**
     * 为用户分配角色
     *
     * @param userId 用户ID
     * @param roleIds 角色ID集合
     */
    @Override
    public void assignForUser(String userId, String[] roleIds) {
        List<UserRole> userRoleList = new ArrayList<>();

        // 遍历角色ID集合以生成角色
        for (String roleId : roleIds) {
            if(StringUtils.isEmpty(roleId)) {
                continue;
            }
            UserRole userRole = new UserRole();
            userRole.setUserId(userId);
            userRole.setRoleId(roleId);
            userRole.setIsDeleted(DelEnums.Normal.value());
            userRoleList.add(userRole);
        }

        userRoleService.saveBatch(userRoleList);
    }

    /**
     * 获取用户角色集合
     *
     * @param userId 用户ID
     * @return 角色集合
     */
    @Override
    public List<Role> listRoleByUserId(String userId) {
        // 查询用户拥有的角色的ID集合
        List<UserRole> userRoleList = userRoleService.list(
                new QueryWrapper<UserRole>()
                        .eq("user_id", userId)
                        .eq("isDeleted", DelEnums.Normal.value())
                        .select("role_id"));
        List<String> roleIdList = userRoleList.stream().map(userRole -> userRole.getRoleId()).collect(Collectors.toList());

        // 查询角色信息
        if (roleIdList.size() == 0) {
            return new ArrayList<>();
        }
        return baseMapper.selectBatchIds(roleIdList);
    }

}
