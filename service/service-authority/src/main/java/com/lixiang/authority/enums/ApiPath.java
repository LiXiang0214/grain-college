package com.lixiang.authority.enums;

/**
 * 路径地址配置
 *
 * @author lixiang
 */
public class ApiPath {

	/**
	 * authority根地址
	 */
	public static final String ROOT = "/authorityservice";

	/**
	 * 权限控制[菜单]管理地址
	 */
	public static final String PERMISSION = ROOT + "/permission";

	/**
	 * 权限控制[角色]管理地址
	 */
	public static final String ROLE = ROOT + "/role";

	/**
	 * 权限控制[用户]管理地址
	 */
	public static final String USER = ROOT + "/user";

	/**
	 * 权限控制[index]管理地址
	 */
	public static final String INDEX = ROOT + "/index";

	protected ApiPath() {

    }

}
