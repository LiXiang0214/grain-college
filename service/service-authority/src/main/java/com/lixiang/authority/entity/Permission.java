package com.lixiang.authority.entity;

import java.util.List;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.lixiang.servicebase.basic.BasicEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 权限控制[菜单] 实体类
 *
 * @author lixiang
 * @since 2021-01-26 12:49:31
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("acl_permission")
@Api(tags = "权限控制[菜单]")
public class Permission extends BasicEntity {

        @ApiModelProperty(value = "ID")
        @TableId(value = "id", type = IdType.ID_WORKER_STR)
        private String id;

        @ApiModelProperty(value = "所属上级")
        private String pid;

        @ApiModelProperty(value = "名称")
        private String name;

        @ApiModelProperty(value = "类型(菜单 -> 1，按钮 -> 2)")
        private Integer type;

        @ApiModelProperty(value = "权限值")
        private String permissionValue;

        @ApiModelProperty(value = "访问路径")
        private String path;

        @ApiModelProperty(value = "组件路径")
        private String component;

        @ApiModelProperty(value = "图标")
        private String icon;

        @ApiModelProperty(value = "状态(禁止 -> 0，正常 -> 1)")
        private Integer status;

        @ApiModelProperty(value = "逻辑删除(已删除 -> 1，未删除 -> 0)")
        private Object isDeleted;


        @ApiModelProperty(value = "子菜单集合")
        @TableField(exist = false)
        private List<Permission> childrenList;

        @ApiModelProperty(value = "层级")
        @TableField(exist = false)
        private Integer level;

        @ApiModelProperty(value = "是否选中")
        @TableField(exist = false)
        private boolean isSelect;

}
