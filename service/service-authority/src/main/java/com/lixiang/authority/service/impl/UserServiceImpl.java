package com.lixiang.authority.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lixiang.authority.entity.User;
import com.lixiang.authority.entity.vo.UserQueryVO;
import com.lixiang.authority.mapper.UserMapper;
import com.lixiang.authority.service.UserService;
import com.lixiang.commonutils.enums.DelEnums;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 权限控制[用户] 服务实现类
 *
 * @author lixiang
 * @since 2021-01-26 12:52:22
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Resource
    private UserMapper aclUserMapper;

    /**
     * 获取用户分页列表
     *
     * @param queryVO 查询参数
     * @return 列表
     */
    @Override
    public Map userList(UserQueryVO queryVO) {
        Map<String, Object> userMap = new HashMap<>();

        // 1、创建page对象
        Page<User> userPage = new Page<>(queryVO.getPage(), queryVO.getLimit());

        // 2、查询条件
        QueryWrapper<User> userWrapper = new QueryWrapper<>();
        userWrapper.eq("isDeleted", DelEnums.Normal.value());
        if(!StringUtils.isEmpty(queryVO.getKeyWord())) {
            userWrapper.and(wrapper ->
                    wrapper.like("nickName", queryVO.getKeyWord()).or()
                            .like("token", queryVO.getKeyWord()));
        }

        // 3、排序
        userWrapper.orderByAsc("gmtCreate");

        // 4、条件分页查询
        page(userPage, userWrapper);
        long total = userPage.getTotal(); // 总记录数
        List<User> userList = userPage.getRecords(); // 数据list集合
        userMap.put("total", total);
        userMap.put("rows", userList);

        return userMap;
    }

    /**
     * 根据用户名查询用户信息
     *
     * @param userName 用户名
     * @return 用户信息
     */
    @Override
    public User selectByUsername(String userName) {
        return baseMapper.selectOne(new QueryWrapper<User>().eq("username", userName));
    }

}
