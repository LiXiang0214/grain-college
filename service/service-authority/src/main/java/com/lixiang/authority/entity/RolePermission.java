package com.lixiang.authority.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.lixiang.servicebase.basic.BasicEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 权限控制[角色-菜单] 实体类
 *
 * @author lixiang
 * @since 2021-01-26 12:49:36
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("acl_role_permission")
@Api(tags = "角色-菜单")
public class RolePermission extends BasicEntity {

        @ApiModelProperty(value = "ID")
        @TableId(value = "id", type = IdType.ID_WORKER_STR)
        private String id;

        @ApiModelProperty(value = "角色ID")
        private String roleId;

        @ApiModelProperty(value = "菜单ID")
        private String permissionId;

        @ApiModelProperty(value = "逻辑删除(已删除 -> 1，未删除 -> 0)")
        private Object isDeleted;

}
