package com.lixiang.authority.controller;

import com.alibaba.fastjson.JSONObject;
import com.lixiang.authority.enums.ApiPath;
import com.lixiang.authority.service.IndexService;
import com.lixiang.commonutils.basic.ReturnEntity;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 权限控制[index] 控制层
 * 用户退出、获取菜单等
 *
 * @author lixiang
 * @since 2021-02-01 16:25:36
 */
@Api(tags = "权限控制[index]管理")
@RestController
@RequestMapping(ApiPath.INDEX)
@CrossOrigin //解决跨域
public class IndexController {

    @Autowired
    private IndexService indexService;

    /**
     * 根据token中的用户名获取用户信息
     *
     * @return 用户信息
     */
    @GetMapping("/userInfo")
    public ReturnEntity userInfo(){
        // 获取当前登录用户用户名
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Map<String, Object> userInfo = indexService.getUserInfo(username);
        return ReturnEntity.success().data(userInfo);
    }

    /**
     * 根据token中的用户名获取菜单
     *
     * @return 菜单
     */
    @GetMapping("/userMenu")
    public ReturnEntity userMenu(){
        // 获取当前登录用户用户名
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        List<JSONObject> permissionList = indexService.getUserMenu(username);
        return ReturnEntity.success().data("permissionList", permissionList);
    }

    /**
     * 退出登录
     *
     * @return 返回信息
     */
    @PostMapping("/logout")
    public ReturnEntity logout(){
        return ReturnEntity.success().desc("退出登录成功!");
    }

}
