package com.lixiang.authority.entity.vo;

import com.lixiang.servicebase.basic.BaseQueryVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 查询用户
 */
@Data
@Api(tags = "查询用户")
public class UserQueryVO extends BaseQueryVO {

    @ApiModelProperty(value = "用户昵称/签名")
    private String keyWord;

}
