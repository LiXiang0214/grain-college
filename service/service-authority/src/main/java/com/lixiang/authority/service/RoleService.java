package com.lixiang.authority.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lixiang.authority.entity.Role;
import com.lixiang.authority.entity.vo.RoleQueryVO;

import java.util.List;
import java.util.Map;


/**
 * 权限控制[角色] 服务层
 *
 * @author lixiang
 * @since 2021-01-26 12:49:36
 */
public interface RoleService extends IService<Role> {

    /**
     * 获取角色分页列表
     *
     * @param queryVO 查询参数
     * @return 列表信息
     */
    Map roleList(RoleQueryVO queryVO);

    /**
     * 获取用户角色信息
     * 查询所有角色结合及用户拥有的角色集合
     *
     * @param userId 用户ID
     * @return 用户角色信息
     */
    Map<String, Object> getAssignInfoByUserId(String userId);

    /**
     * 为用户分配角色
     *
     * @param userId 用户ID
     * @param roleIds 角色ID集合
     */
    void assignForUser(String userId, String[] roleIds);

    /**
     * 获取用户角色集合
     *
     * @param userId 用户ID
     * @return 角色集合
     */
    List<Role> listRoleByUserId(String userId);

}
