package com.lixiang.authority.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.lixiang.authority.entity.Role;
import com.lixiang.authority.entity.User;
import com.lixiang.authority.service.IndexService;
import com.lixiang.authority.service.PermissionService;
import com.lixiang.authority.service.RoleService;
import com.lixiang.authority.service.UserService;
import com.lixiang.commonutils.enums.ResultCode;
import com.lixiang.servicebase.exceptionhandler.GuliException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 权限控制[index] 业务实现层
 * 用户退出、获取菜单等
 */
@Service
public class IndexServiceImpl implements IndexService {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private PermissionService permissionService;

    @Autowired
    private RedisTemplate redisTemplate;


    /**
     * 根据token中的用户名获取用户信息
     *
     * @return 用户信息
     */
    @Override
    public Map<String, Object> getUserInfo(String userName) {
        // 1、用户基本信息
        User user = userService.selectByUsername(userName);
        if (StringUtils.isEmpty(user)) {
            throw new GuliException(ResultCode.ERROR, "用户不存在！");
        }

        // 2、根据用户id获取角色
        List<Role> roleList = roleService.listRoleByUserId(user.getId());
        List<String> roleNameList = roleList.stream().map(role -> role.getRoleName()).collect(Collectors.toList());
        if(roleNameList.size() == 0) {
            //前端框架必须返回一个角色，否则报错，如果没有角色，返回一个空角色
            roleNameList.add("");
        }

        // 3、根据用户id获取操作权限值集合
        List<String> permissionValueList = permissionService.selectPermissionValueByUserId(user.getId());
        // 存到redis中
        redisTemplate.opsForValue().set(userName, permissionValueList);

        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("userName", user.getUsername());
        resultMap.put("avatar", "https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif");
        resultMap.put("roleList", roleList);
        resultMap.put("permissionValueList", permissionValueList);

        return resultMap;
    }

    @Override
    public List<JSONObject> getUserMenu(String userName) {
        User user = userService.selectByUsername(userName);

        // 根据用户id获取用户菜单权限
        return permissionService.selectPermissionByUserId(user.getId());
    }

}
