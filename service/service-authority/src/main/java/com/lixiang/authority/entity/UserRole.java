package com.lixiang.authority.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.lixiang.servicebase.basic.BasicEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 权限控制[用户-角色] 实体类
 *
 * @author lixiang
 * @since 2021-01-26 12:49:36
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("acl_user_role")
@Api(tags = "用户-角色")
public class UserRole extends BasicEntity {

        @ApiModelProperty(value = "ID")
        @TableId(value = "id", type = IdType.ID_WORKER_STR)
        private String id;

        @ApiModelProperty(value = "角色id")
        private String roleId;

        @ApiModelProperty(value = "用户id")
        private String userId;

        @ApiModelProperty(value = "逻辑删除(已删除 -> 1，未删除 -> 0)")
        private Object isDeleted;

}
