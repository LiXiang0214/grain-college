package com.lixiang.authority.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lixiang.authority.entity.UserRole;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;


/**
 * 权限控制[用户-角色] Mapper层
 *
 * @author lixiang
 * @Time 2021-01-26 12:49:36
 */
@Mapper
@Component
public interface UserRoleMapper extends BaseMapper<UserRole> {

}
