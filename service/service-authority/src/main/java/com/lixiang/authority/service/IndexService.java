package com.lixiang.authority.service;

import com.alibaba.fastjson.JSONObject;

import java.util.List;
import java.util.Map;

/**
 * 权限控制[index] 业务层
 * 用户退出、获取菜单等
 */
public interface IndexService {

    /**
     * 根据token中的用户名获取用户信息
     *
     * @param username 用户名
     * @return 用户信息
     */
    Map<String, Object> getUserInfo(String username);

    /**
     * 根据token中的用户名获取菜单
     *
     * @param username 用户名
     * @return 菜单
     */
    List<JSONObject> getUserMenu(String username);

}
