package com.lixiang.authority;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * 权限菜单管理 控制层
 */
@SpringBootApplication
@MapperScan("com.lixiang.authority.service.mapper")
@EnableDiscoveryClient  //nacos注册
@ComponentScan("com.lixiang") // 扫描
public class AclApplication {

    /**
     * Main 方法
     *
     * @param args 参数
     */
    public static void main(String[] args) {
        SpringApplication.run(AclApplication.class, args);
    }

}
