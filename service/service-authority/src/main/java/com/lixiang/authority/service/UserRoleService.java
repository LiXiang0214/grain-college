package com.lixiang.authority.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lixiang.authority.entity.UserRole;


/**
 * 权限控制[用户-角色] 服务层
 *
 * @author lixiang
 * @since 2021-01-26 12:49:36
 */
public interface UserRoleService extends IService<UserRole> {

}
