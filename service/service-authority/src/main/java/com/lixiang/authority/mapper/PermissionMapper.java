package com.lixiang.authority.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lixiang.authority.entity.Permission;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * 权限控制[菜单] Mapper层
 *
 * @author lixiang
 * @Time 2021-01-26 12:49:31
 */
@Mapper
@Component
public interface PermissionMapper extends BaseMapper<Permission> {

    /**
     * 获取所有权限Value
     *
     * @return 所有权限Value
     */
    List<String> selectAllPermissionValue();

    /**
     * 根据用户ID获取权限Value
     *
     * @param userId 用户ID
     * @return 权限Value
     */
    List<String> selectPermissionValueByUserId(String userId);

    /**
     * 根据用户ID获取权限集合
     *
     * @param userId 用户ID
     * @return 权限集合
     */
    List<Permission> selectPermissionByUserId(String userId);

}
