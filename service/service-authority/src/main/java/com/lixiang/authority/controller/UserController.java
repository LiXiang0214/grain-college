package com.lixiang.authority.controller;

import com.lixiang.authority.entity.User;
import com.lixiang.authority.entity.vo.RoleVO;
import com.lixiang.authority.entity.vo.UserQueryVO;
import com.lixiang.authority.entity.vo.UserVO;
import com.lixiang.authority.enums.ApiPath;
import com.lixiang.authority.service.RoleService;
import com.lixiang.authority.service.UserService;
import com.lixiang.commonutils.basic.ReturnEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;


/**
 * 权限控制[用户] 控制层
 *
 * @author lixiang
 * @since 2021-01-26 12:49:36
 */
@Api(tags = "权限控制[用户]管理")
@RestController
@RequestMapping(ApiPath.USER)
@CrossOrigin //解决跨域
public class UserController {

    @Autowired
    private UserService userService;

    @Resource
    private RoleService roleService;

    /**
     * 新增用户
     *
     * @param userVO 新增参数
     * @return {@link ReturnEntity}
     */
    @ApiOperation(value = "新增用户")
    @PostMapping("/insert")
    public ReturnEntity insert(@RequestBody UserVO userVO) {
        User user = new User();
        BeanUtils.copyProperties(userVO, user);
        if (this.userService.save(user)) {
            return ReturnEntity.success().desc("新增用户成功！");
        }
        return ReturnEntity.fail().desc("新增用户失败！");
    }

    /**
     * 更新用户
     *
     * @param userId 用户ID
     * @param roleVO 更新参数
     * @return {@link ReturnEntity}
     */
    @ApiOperation(value = "更新用户")
    @PostMapping("/{userId}/update")
    public ReturnEntity updateSelective(@ApiParam("用户ID") @PathVariable("userId") String userId,
                                        @RequestBody RoleVO roleVO) {
        User user = userService.getById(userId);
        BeanUtils.copyProperties(roleVO, user);
        if (this.userService.updateById(user)) {
            return ReturnEntity.success().desc("更新用户成功！");
        }
        return ReturnEntity.fail().desc("更新用户失败！");
    }

    /**
     * 根据用户ID删除用户
     *
     * @param userId 用户ID
     * @return {@link ReturnEntity}
     */
    @ApiOperation(value = "根据ID删除用户")
    @DeleteMapping("/{userId}/remove")
    public ReturnEntity remove(@ApiParam("用户ID") @PathVariable("userId") String userId) {
        if (userService.removeById(userId)) {
            return ReturnEntity.success().desc("删除用户成功！");
        }
        return ReturnEntity.fail().desc("删除用户失败！");
    }

    /**
     * 批量删除用户
     *
     * @param userIdList 用户ID集合
     * @return {@link ReturnEntity}
     */
    @ApiOperation(value = "批量删除用户")
    @DeleteMapping("/batchRemove")
    public ReturnEntity batchRemove(@RequestBody List<String> userIdList) {
        if (userService.removeByIds(userIdList)) {
            return ReturnEntity.success().desc("删除用户成功！");
        }
        return ReturnEntity.fail().desc("删除用户失败！");
    }

    /**
     * 根据用户ID获取用户详情
     *
     * @param userId 用户ID
     * @return {@link ReturnEntity}
     */
    @ApiOperation(value = "根据角色ID获取用户详情")
    @GetMapping("/{userId}/getUser")
    public ReturnEntity getUser(@ApiParam("用户ID") @PathVariable("userId") String userId) {
        return ReturnEntity.success().data("user", userService.getById(userId));
    }

    /**
     * 获取用户分页列表
     *
     * @param queryVO 查询参数
     * @return 列表
     */
    @ApiOperation(value = "获取用户分页列表")
    @GetMapping("/userList")
    public ReturnEntity userList(UserQueryVO queryVO) {
        return ReturnEntity.success().data(userService.userList(queryVO));
    }

    /**
     * 获取用户角色信息
     * 查询所有角色结合及用户拥有的角色集合
     *
     * @param userId 用户ID
     * @return 用户角色信息
     */
    @ApiOperation(value = "获取用户角色信息")
    @GetMapping("/{userId}/assignInfo")
    public ReturnEntity assignInfo(@ApiParam("用户ID") @PathVariable("userId") String userId) {
        Map<String, Object> roleMap = roleService.getAssignInfoByUserId(userId);
        return ReturnEntity.success().data(roleMap);
    }

    /**
     * 为用户分配角色
     *
     * @param userId 用户ID
     * @param roleIds 角色ID集合
     * @return {@link ReturnEntity}
     */
    @ApiOperation(value = "为用户分配角色")
    @PostMapping("/{userId}/assignForUser")
    public ReturnEntity assignForUser(@ApiParam("用户ID") @RequestParam String userId,
                                      @ApiParam("角色ID集合") @RequestParam String[] roleIds) {
        roleService.assignForUser(userId, roleIds);
        return ReturnEntity.success().desc("分配角色成功！");
    }

}
