package com.lixiang.authority.controller;

import com.lixiang.authority.entity.Role;
import com.lixiang.authority.entity.vo.RoleQueryVO;
import com.lixiang.authority.entity.vo.RoleVO;
import com.lixiang.authority.enums.ApiPath;
import com.lixiang.authority.service.RoleService;
import com.lixiang.commonutils.basic.ReturnEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 权限控制[角色] 控制层
 *
 * @author lixiang
 * @since 2021-01-26 12:49:36
 */
@Api(tags = "权限控制[角色]管理")
@RestController
@RequestMapping(ApiPath.ROLE)
@CrossOrigin //解决跨域
public class RoleController {

    @Autowired
    private RoleService roleService;

    /**
     * 新增角色
     *
     * @param roleVO 新增参数
     * @return {@link ReturnEntity}
     */
    @ApiOperation(value = "新增角色")
    @PostMapping("/insert")
    public ReturnEntity insert(@RequestBody RoleVO roleVO) {
        Role role = new Role();
        BeanUtils.copyProperties(roleVO, role);
        if (this.roleService.save(role)) {
            return ReturnEntity.success().desc("新增角色成功！");
        }
        return ReturnEntity.fail().desc("新增角色失败！");
    }

    /**
     * 更新角色
     *
     * @param roleId 角色ID
     * @param roleVO 更新参数
     * @return {@link ReturnEntity}
     */
    @ApiOperation(value = "更新角色")
    @PostMapping("/{roleId}/update")
    public ReturnEntity updateSelective(@ApiParam("角色ID") @PathVariable("roleId") String roleId,
                                        @RequestBody RoleVO roleVO) {
        Role role = roleService.getById(roleId);
        BeanUtils.copyProperties(roleVO, role);
        if (this.roleService.updateById(role)) {
            return ReturnEntity.success().desc("更新角色成功！");
        }
        return ReturnEntity.fail().desc("更新角色失败！");
    }

    /**
     * 根据角色ID删除角色
     * @param roleId 角色ID
     * @return {@link ReturnEntity}
     */
    @ApiOperation(value = "根据角色ID删除角色")
    @DeleteMapping("/{roleId}/remove")
    public ReturnEntity remove(@ApiParam("角色ID") @PathVariable("roleId") String roleId) {
        if (roleService.removeById(roleId)) {
            return ReturnEntity.success().desc("删除角色成功！");
        }
        return ReturnEntity.fail().desc("删除角色失败！");
    }

    /**
     * 批量删除角色
     *
     * @param roleIdList 角色ID列表
     * @return {@link ReturnEntity}
     */
    @ApiOperation(value = "批量删除角色")
    @DeleteMapping("/batchRemove")
    public ReturnEntity batchRemove(@RequestBody List<String> roleIdList) {
        if (roleService.removeByIds(roleIdList)) {
            return ReturnEntity.success().desc("删除角色成功！");
        }
        return ReturnEntity.fail().desc("删除角色失败！");
    }

    /**
     * 根据角色ID获取角色详情
     *
     * @param roleId 角色ID
     * @return {@link ReturnEntity}
     */
    @ApiOperation(value = "根据角色ID获取角色详情")
    @GetMapping("/{roleId}/getRole")
    public ReturnEntity getRole(@ApiParam("角色ID") @PathVariable("roleId") String roleId) {
        return ReturnEntity.success().data("item", roleService.getById(roleId));
    }

    /**
     * 获取角色分页列表
     *
     * @param queryVO 查询参数
     * @return 列表
     */
    @ApiOperation(value = "获取角色分页列表")
    @GetMapping("/roleList")
    public ReturnEntity roleList(RoleQueryVO queryVO) {
        return ReturnEntity.success().data(roleService.roleList(queryVO));
    }

}
