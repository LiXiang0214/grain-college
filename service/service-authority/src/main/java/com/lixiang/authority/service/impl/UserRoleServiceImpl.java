package com.lixiang.authority.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lixiang.authority.entity.UserRole;
import com.lixiang.authority.mapper.UserRoleMapper;
import com.lixiang.authority.service.UserRoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 权限控制[用户-角色] 服务实现类
 *
 * @author lixiang
 * @since 2021-01-26 12:52:22
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {

    @Resource
    private UserRoleMapper aclUserRoleMapper;

}
