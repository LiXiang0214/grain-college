package com.lixiang.authority.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lixiang.authority.entity.Permission;

import java.util.List;

/**
 * 权限管理[菜单] 服务层
 *
 * @author lixiang
 * @since 2021-01-26 12:49:31
 */
public interface PermissionService extends IService<Permission> {

    /**
     * 获取菜单树
     *
     * @return 菜单树集合
     */
    List<Permission> getPermissionTree();

    /**
     * 递归删除菜单
     *
     * @param permissionId 菜单ID
     */
    void removePermissionRecursion(String permissionId);

    /**
     * 给角色分配权限
     *
     * @param roleId 角色ID
     * @param permissionIds 权限ID集合
     */
    void assignForRole(String roleId, String[] permissionIds);

    /**
     * 根据角色获取菜单
     *
     * @param roleId 角色ID
     * @return 菜单列表集合
     */
    List<Permission> getAssignByRole(String roleId);

    /**
     * 根据用户ID获取操作权限值集合
     *
     * @param userId 用户ID
     * @return 操作权限值集合
     */
    List<String> selectPermissionValueByUserId(String userId);

    /**
     * 根据用户ID获取菜单信息
     *
     * @param userId 用户ID
     * @return 操作权限值集合
     */
    List<JSONObject> selectPermissionByUserId(String userId);

}
