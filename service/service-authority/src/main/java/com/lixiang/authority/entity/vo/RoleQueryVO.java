package com.lixiang.authority.entity.vo;

import com.lixiang.servicebase.basic.BaseQueryVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 查询角色
 */
@Data
@Api(tags = "查询角色")
public class RoleQueryVO extends BaseQueryVO {

    @ApiModelProperty(value = "角色名称/备注")
    private String keyWord;

}
