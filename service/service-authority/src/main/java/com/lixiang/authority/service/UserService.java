package com.lixiang.authority.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lixiang.authority.entity.User;
import com.lixiang.authority.entity.vo.UserQueryVO;

import java.util.Map;


/**
 * 权限控制[用户] 服务层
 *
 * @author lixiang
 * @since 2021-01-26 12:49:36
 */
public interface UserService extends IService<User> {

    /**
     * 获取用户分页列表
     *
     * @param queryVO 查询参数
     * @return 列表
     */
    Map userList(UserQueryVO queryVO);

    /**
     * 根据用户名查询用户信息
     *
     * @param userName 用户名
     * @return 用户信息
     */
    User selectByUsername(String userName);

}
