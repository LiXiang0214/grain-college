package com.lixiang.authority.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.lixiang.servicebase.basic.BasicEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 权限控制[用户] 实体类
 *
 * @author lixiang
 * @since 2021-01-26 12:49:36
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("acl_user")
@Api(tags = "用户")
public class User extends BasicEntity {

        @ApiModelProperty(value = "ID")
        @TableId(value = "id", type = IdType.ID_WORKER_STR)
        private String id;

        @ApiModelProperty(value = "微信openid/用户名[唯一]")
        private String username;

        @ApiModelProperty(value = "密码")
        private String password;

        @ApiModelProperty(value = "昵称")
        private String nickName;

        @ApiModelProperty(value = "用户头像")
        private String salt;

        @ApiModelProperty(value = "用户签名")
        private String token;

        @ApiModelProperty(value = "逻辑删除(已删除 -> 1，未删除 -> 0)")
        private Object isDeleted;

}
