package com.lixiang.eduservice.service.impl;

import com.lixiang.eduservice.entity.EduCourseDescription;
import com.lixiang.eduservice.mapper.EduCourseDescriptionMapper;
import com.lixiang.eduservice.service.EduCourseDescriptionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 课程简介 业务层实现
 *
 * @author lixiang
 * @since 2020-08-27
 */
@Service
public class EduCourseDescriptionServiceImpl extends ServiceImpl<EduCourseDescriptionMapper, EduCourseDescription> implements EduCourseDescriptionService {

}
