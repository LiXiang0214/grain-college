package com.lixiang.eduservice.controller;


import com.lixiang.commonutils.basic.ReturnEntity;
import com.lixiang.eduservice.enums.ApiPath;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

/**
 * 登录 控制器
 *
 * @author lixiang
 * @since 2020-08-10
 */
@Api(tags = "登录管理")
@RestController
@RequestMapping(ApiPath.LOGIN)
@CrossOrigin //解决跨域
public class LoginController {

    /**
     * 登录
     *
     * @return {@link ReturnEntity}
     */
    @ApiOperation("登录")
    @PostMapping
    public ReturnEntity listTeachers() {
        return ReturnEntity.success().data("token", "admin").desc("登录成功");
    }

    /**
     * 用户信息
     *
     * @return {@link ReturnEntity}
     */
    @ApiOperation("用户信息")
    @GetMapping("/info")
    public ReturnEntity info() {
        return ReturnEntity.success()
                .data("roles", "[admin]")
                .data("name", "admin")
                .data("avatar", "https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif");
    }

}

