package com.lixiang.eduservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lixiang.commonutils.enums.ResultCode;
import com.lixiang.servicebase.exceptionhandler.GuliException;
import com.lixiang.eduservice.entity.EduChapter;
import com.lixiang.eduservice.entity.EduVideo;
import com.lixiang.eduservice.entity.dto.ChapterDTO;
import com.lixiang.eduservice.entity.dto.VideoDTO;
import com.lixiang.eduservice.mapper.EduChapterMapper;
import com.lixiang.eduservice.service.EduChapterService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lixiang.eduservice.service.EduVideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 课程章节 业务层实现
 *
 * @author lixiang
 * @since 2020-08-27
 */
@Service
public class EduChapterServiceImpl extends ServiceImpl<EduChapterMapper, EduChapter> implements EduChapterService {

    @Autowired
    private EduVideoService videoService;

    @Override
    @Transactional(rollbackFor = {Exception.class})
    public boolean deleteChapter(String chapterId) {
        // 根据章节id 查询小节数
        QueryWrapper<EduVideo> videoWrapper = new QueryWrapper<>();
        videoWrapper.eq("chapter_id", chapterId);
        if (videoService.count(videoWrapper) > 0) {
            throw new GuliException(ResultCode.ERROR, "该章节下有小节，不能删除！");
        } else {
            // 删除成功：1>0 --> true  删除失败：0>0 --> false
            return baseMapper.deleteById(chapterId) > 0;
        }
    }

    @Override
    public List<ChapterDTO> getChapterAndVideoByCourseId(String courseId) {
        // 存放结果集合
        List<ChapterDTO> chapterDTOList = new ArrayList<>();

        // 1、查询所有章节
        QueryWrapper<EduChapter> chapterWrapper = new QueryWrapper<>();
        chapterWrapper.eq("course_id", courseId);
        chapterWrapper.orderByAsc("gmt_create");
        List<EduChapter> chapterDBList = baseMapper.selectList(chapterWrapper);

        // 2、查询所有小节
        QueryWrapper<EduVideo> videoWrapper = new QueryWrapper<>();
        videoWrapper.eq("course_id", courseId);
        videoWrapper.orderByAsc("sort");
        List<EduVideo> videoDBList = videoService.list(videoWrapper);

        // 遍历章节
        chapterDBList.forEach(chapterDB -> {
            ChapterDTO chapterDTO = new ChapterDTO();
            chapterDTO.setId(chapterDB.getId());
            chapterDTO.setTitle(chapterDB.getTitle());
            // 过滤出该章节下的小节
            List<EduVideo> thisVideoDBList = videoDBList
                    .stream()
                    .filter(videoDB -> videoDB.getChapterId().equals(chapterDB.getId()))
                    .collect(Collectors.toList());
            List<VideoDTO> children = new ArrayList<>();
            // 遍历小节，放到DTO中
            thisVideoDBList.forEach(videoDB -> {
                VideoDTO videoDTO = new VideoDTO();
                videoDTO.setId(videoDB.getId());
                videoDTO.setTitle(videoDB.getTitle());
                videoDTO.setVideoSourceId(videoDB.getVideoSourceId());
                videoDTO.setIsFree(videoDB.getIsFree());
                children.add(videoDTO);
            });
            chapterDTO.setChildren(children);
            chapterDTOList.add(chapterDTO);
        });

        return chapterDTOList;
    }

    @Override
    @Transactional(rollbackFor = {Exception.class})
    public void removeChapterByCourseId(String courseId) {
        QueryWrapper<EduChapter> chapterWrapper = new QueryWrapper<>();
        chapterWrapper.eq("course_id", courseId);
        baseMapper.delete(chapterWrapper);
    }

}
