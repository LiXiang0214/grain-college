package com.lixiang.eduservice.controller;

import com.lixiang.commonutils.basic.ReturnEntity;
import com.lixiang.eduservice.entity.EduChapter;
import com.lixiang.eduservice.entity.dto.ChapterDTO;
import com.lixiang.eduservice.entity.vo.ChapterVO;
import com.lixiang.eduservice.enums.ApiPath;
import com.lixiang.eduservice.service.EduChapterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 课程章节 控制器
 *
 * @author lixiang
 * @since 2020-08-27
 */
@Api(tags = "课程[章节管理]")
@RestController
@RequestMapping(ApiPath.CHAPTER)
@CrossOrigin //解决跨域
public class ChapterController {

    @Autowired
    private EduChapterService chapterService;

    /**
     * 添加章节
     *
     * @param chapterVO {@link ChapterVO}
     * @return {@link ReturnEntity}
     */
    @ApiOperation("添加章节")
    @PostMapping
    public ReturnEntity save(@RequestBody ChapterVO chapterVO) {
        EduChapter chapter = new EduChapter();
        BeanUtils.copyProperties(chapterVO, chapter);
        chapterService.save(chapter);
        return ReturnEntity.success();
    }

    /**
     * 删除章节
     *
     * @param chapterId 章节ID
     * @return {@link ReturnEntity}
     */
    @ApiOperation("删除章节")
    @DeleteMapping("{chapterId}")
    public ReturnEntity deleteChapter(@ApiParam("章节ID") @PathVariable("chapterId") String chapterId) {
        if(chapterService.deleteChapter(chapterId)) {
            return ReturnEntity.success();
        } else {
            return ReturnEntity.fail();
        }
    }

    /**
     * 更新章节
     *
     * @param chapterId 章节ID
     * @param chapterVO {@link ChapterVO}
     * @return {@link ReturnEntity}
     */
    @ApiOperation("更新章节")
    @PostMapping("{chapterId}")
    public ReturnEntity updateChapter(@ApiParam("章节ID") @PathVariable("chapterId") String chapterId,
                                      @RequestBody ChapterVO chapterVO) {
        EduChapter chapter = chapterService.getById(chapterId);
        chapter.setTitle(chapterVO.getTitle());
        chapter.setSort(chapterVO.getSort());
        chapterService.updateById(chapter);
        return ReturnEntity.success();
    }

    /**
     * 查询章节详情
     *
     * @param chapterId 章节ID
     * @return {@link ReturnEntity}
     */
    @ApiOperation("查询章节详情")
    @GetMapping("{chapterId}")
    public ReturnEntity getChapterInfo(@ApiParam("章节ID") @PathVariable("chapterId") String chapterId) {
        EduChapter eduChapter = chapterService.getById(chapterId);
        return ReturnEntity.success().data("chapter", eduChapter);
    }

    /**
     * 根据课程id进行查询课程大纲列表
     *
     * @param courseId 课程ID
     * @return {@link ReturnEntity}
     */
    @ApiOperation("查询章节&小节")
    @GetMapping("getChapterVideo/{courseId}")
    public ReturnEntity getChapterVideo(@ApiParam("课程ID") @PathVariable("courseId") String courseId) {
        List<ChapterDTO> list = chapterService.getChapterAndVideoByCourseId(courseId);
        return ReturnEntity.success().data("allChapterVideo",list);
    }

}

