package com.lixiang.eduservice.client;

import com.lixiang.commonutils.basic.ReturnEntity;
import io.swagger.annotations.ApiParam;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * 调用Vod服务配置
 */
@FeignClient(name = "service-vod", fallback = VodFileDegradeFeignClient.class) //调用的服务名称
@Component
public interface VodClient { //定义调用的方法路径

    /**
     * 根据视频ID删除阿里云视频
     *
     * @param id 视频ID
     * @return {@link ReturnEntity}
     */
    @DeleteMapping("/vodservice/video/{id}")
    ReturnEntity removeAlyVideo(@ApiParam("视频ID") @PathVariable("id") String id);

    /**
     * 删除多个阿里云视频的方法
     *
     * @param videoIdList 多个视频ID的List
     * @return {@link ReturnEntity}
     */
    @DeleteMapping("/vodservice/video/deleteBatch")
    ReturnEntity deleteBatch(@RequestParam("videoIdList") List<String> videoIdList);

}
