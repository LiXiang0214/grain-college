package com.lixiang.eduservice.enums;

/**
 * 课程状态 枚举
 * @Auther: lixiang
 * @Date: 2020/8/27
 */
public enum CourseStatusEnums {
	Senior("Draft", "未发布"),
	Chief("Normal", "已发布");

	private final String value;
	private final String desc;

	CourseStatusEnums(String value, String desc) {
		this.value = value;
		this.desc = desc;
	}

    /**
     * 获取描述
     *
     * @param value 值
     * @return desc
     */
    public static String getDesc(String value) {
        for (CourseStatusEnums enumValue : values()) {
            if (enumValue.value().equals(value)) {
                return enumValue.desc();
            }
        }
        return null;
    }

	/**
	 * 获取值
	 *
	 * @return value
	 */
	public String value() {
		return value;
	}

    /**
     * 获取描述
     *
     * @return desc
     */
    public String desc() {
        return desc;
    }

}
