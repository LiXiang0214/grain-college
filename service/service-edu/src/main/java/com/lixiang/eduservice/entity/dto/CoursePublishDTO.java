package com.lixiang.eduservice.entity.dto;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 确认发布课程时返回的数据
 */
@Data
@Api(tags = "确认发布课程时返回的数据")
public class CoursePublishDTO {

    @ApiModelProperty(value = "课程ID")
    private String id;

    @ApiModelProperty(value = "课程名")
    private String title;

    @ApiModelProperty(value = "封面")
    private String cover;

    @ApiModelProperty(value = "价格")
    private String price;//只用于显示

    @ApiModelProperty(value = "描述")
    private String description;

    @ApiModelProperty(value = "课时数")
    private Integer lessonNum;

    @ApiModelProperty(value = "一级分类")
    private String firstLevelSubject;

    @ApiModelProperty(value = "二级分类")
    private String secondLevelSubject;

    @ApiModelProperty(value = "讲师名")
    private String teacherName;

    @ApiModelProperty(value = "课程状态 Draft未发布  Normal已发布")
    private String status;

}
