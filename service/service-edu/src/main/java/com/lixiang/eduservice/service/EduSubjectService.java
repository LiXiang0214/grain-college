package com.lixiang.eduservice.service;

import com.lixiang.eduservice.entity.EduSubject;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 课程分类 业务层
 *
 * @author lixiang
 * @since 2020-08-27
 */
public interface EduSubjectService extends IService<EduSubject> {

    /**
     * 导入课程分类
     *
     * @param file 上传的execl文件
     * @param subjectService {@link EduSubjectService}
     */
    void importSubject(MultipartFile file, EduSubjectService subjectService);

    /**
     * 课程分类列表(树形)[递归]
     *
     * @return 课程分类集合
     */
    List<EduSubject> listSubjectTreeRecursion();

    /**
     * 课程分类列表(树形)[Map集合]
     *
     * @return 课程分类集合
     */
    List<EduSubject> listSubjectTreeMap();

    /**
     * 判断是否存在子分类
     *
     * @param subjectId 分类ID
     * @return true：存在，false：不存在
     */
    boolean isExistsChildren(String subjectId);

}
