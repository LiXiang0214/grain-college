package com.lixiang.eduservice.entity.dto;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 *  课程章节DTO
 */
@Data
@Api(tags = "课程章节DTO")
public class ChapterDTO {

    @ApiModelProperty(value = "章节ID")
    private String id;

    @ApiModelProperty(value = "章节标题")
    private String title;

    @ApiModelProperty(value = "小节集合")
    private List<VideoDTO> children = new ArrayList<>();

}
