package com.lixiang.eduservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lixiang.eduservice.entity.EduTeacher;
import com.lixiang.eduservice.entity.vo.TeacherQueryVO;
import com.lixiang.eduservice.mapper.EduTeacherMapper;
import com.lixiang.eduservice.service.EduTeacherService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 讲师管理 业务层实现
 *
 * @author lixiang
 * @since 2020-08-10
 */
@Service
public class EduTeacherServiceImpl extends ServiceImpl<EduTeacherMapper, EduTeacher> implements EduTeacherService {

    @Override
    public Map<String, Object> pageQueryTeacher(TeacherQueryVO teacherQueryVO) {
        Map<String, Object> teacherMap = new HashMap<>();
        //创建page对象
        Page<EduTeacher> pageTeacher = new Page<>(teacherQueryVO.getPage(), teacherQueryVO.getLimit());

        //构建条件
        QueryWrapper<EduTeacher> wrapper = new QueryWrapper<>();
        if(!StringUtils.isEmpty(teacherQueryVO.getName())) {
            wrapper.like("name", teacherQueryVO.getName());
        }
        if(!StringUtils.isEmpty(teacherQueryVO.getLevel())) {
            wrapper.eq("level", teacherQueryVO.getLevel());
        }
        if(!StringUtils.isEmpty(teacherQueryVO.getBeginTime())) {
            wrapper.ge("gmt_create", teacherQueryVO.getBeginTime());
        }
        if(!StringUtils.isEmpty(teacherQueryVO.getEndTime())) {
            wrapper.le("gmt_create", teacherQueryVO.getEndTime());
        }

        // 排序
        wrapper.orderByAsc("sort");

        //调用方法实现条件查询分页
        page(pageTeacher, wrapper);
        long total = pageTeacher.getTotal(); // 总记录数
        List<EduTeacher> teacherList = pageTeacher.getRecords(); // 数据list集合
        teacherMap.put("total", total);
        teacherMap.put("rows", teacherList);
        return teacherMap;
    }

}
