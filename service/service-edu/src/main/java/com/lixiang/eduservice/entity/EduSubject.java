package com.lixiang.eduservice.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import com.lixiang.eduservice.enums.SubjectLevelEnums;
import com.lixiang.servicebase.basic.BasicEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

/**
 * 课程分类
 *
 * @author lixiang
 * @since 2020-08-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Api(tags = "课程分类")
public class EduSubject extends BasicEntity {

    @ApiModelProperty(value = "课程分类ID")
    @TableId(value = "id", type = IdType.ID_WORKER_STR)
    private String id;

    @ApiModelProperty(value = "课程类别名称")
    private String title;

    @ApiModelProperty(value = "父分类ID")
    private String parentId;

    @ApiModelProperty(value = "父分类名称")
    @TableField(exist = false)
    private String parentSubjectTitle;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "分类层级：1->一级,2->二级")
    private Integer subjectLevel;

    @ApiModelProperty("分类层级说明")
    @TableField(exist = false)
    private String subjectLevelDesc;

    @ApiModelProperty(value = "子分类集合")
    @TableField(exist = false)
    private List<EduSubject> children = new ArrayList<>();

    public String getSubjectLevelDesc() {
        return SubjectLevelEnums.getDesc(this.subjectLevel);
    }

}
