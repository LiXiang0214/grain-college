package com.lixiang.eduservice.controller;

import com.lixiang.commonutils.basic.ReturnEntity;
import com.lixiang.eduservice.enums.ApiPath;
import com.lixiang.eduservice.service.FrontService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 前台[首页] 控制器
 *
 * @author lixiang
 * @since 2020-09-14
 */
@Api(tags = "前台[首页]")
@RestController
@RequestMapping(ApiPath.INDEX)
@CrossOrigin //解决跨域
public class FrontIndexController {

    @Autowired
    private FrontService frontService;

    /**
     * 首页热门课程信息
     *
     * @return {@link ReturnEntity}
     */
    @ApiOperation("热门课程信息")
    @GetMapping("popularCourse")
    public ReturnEntity popularCourse() {
        return ReturnEntity.success().data("courseList", frontService.queryPopularCourse());
    }

    /**
     * 首页热门教师信息
     *
     * @return {@link ReturnEntity}
     */
    @ApiOperation("热门教师信息")
    @GetMapping("popularTeacher")
    public ReturnEntity popularTeacher() {
        return ReturnEntity.success().data("teacherList", frontService.queryPopularTeacher());
    }

}

