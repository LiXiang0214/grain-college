package com.lixiang.eduservice.entity.dto;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * 课程小节DTO
 */
@Data
@Api(tags = "课程小节DTO")
public class VideoDTO {

    @ApiModelProperty(value = "小节ID")
    private String id;

    @ApiModelProperty(value = "小节标题")
    private String title;

    @ApiModelProperty(value = "视频id")
    private String videoSourceId;

    @ApiModelProperty(value = "是否可以试听：0收费 1免费")
    private Integer isFree;

}
