package com.lixiang.eduservice.service;

import com.lixiang.eduservice.entity.EduCourse;
import com.lixiang.eduservice.entity.EduTeacher;
import com.lixiang.eduservice.entity.vo.FrontCourseQueryVO;
import com.lixiang.servicebase.basic.BaseQueryVO;
import com.lixiang.servicebase.entity.dto.CourseInfoDTO;

import java.util.List;
import java.util.Map;

/**
 * 前台 业务层
 *
 * @author lixiang
 * @since 2020-08-10
 */
public interface FrontService {

    /**
     * 查询前8条热门课程
     *
     * @return 课程列表
     */
    List<EduCourse> queryPopularCourse();

    /**
     * 查询前4条热门名师
     *
     * @return 教师列表
     */
    List<EduTeacher> queryPopularTeacher();

    /**
     * 分页查询讲师
     *
     * @param baseQueryVO {@link BaseQueryVO}
     * @return 教师列表信息集合
     */
    Map<String, Object> teacherList(BaseQueryVO baseQueryVO);

    /**
     * 分页查询课程
     *
     * @param courseQueryVO {@link FrontCourseQueryVO}
     * @return 课程信息列表集合
     */
    Map<String, Object> courseList(FrontCourseQueryVO courseQueryVO);

    /**
     * 讲师详情及课程
     *
     * @param teacherId 讲师ID
     * @return 讲师详情及课程信息集合
     */
    Map<String, Object> teacherInfo(String teacherId);

    /**
     * 根据课程id查询课程完整信息
     *
     * @param id 课程id
     * @return {@link CourseInfoDTO}
     */
    CourseInfoDTO courseCompleteInfo(String id);

}
