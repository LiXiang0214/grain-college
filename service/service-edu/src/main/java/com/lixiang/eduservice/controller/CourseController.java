package com.lixiang.eduservice.controller;

import com.lixiang.commonutils.basic.ReturnEntity;
import com.lixiang.eduservice.entity.EduCourse;
import com.lixiang.eduservice.entity.vo.CourseInfoVO;
import com.lixiang.eduservice.entity.dto.CoursePublishDTO;
import com.lixiang.eduservice.entity.vo.CourseQueryVO;
import com.lixiang.eduservice.enums.ApiPath;
import com.lixiang.eduservice.enums.CourseStatusEnums;
import com.lixiang.eduservice.service.EduCourseService;
import com.lixiang.eduservice.service.FrontService;
import com.lixiang.servicebase.entity.dto.CourseInfoDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 课程管理 控制器
 *
 * @author lixiang
 * @since 2020-08-27
 */
@Api(tags = "课程[信息管理]")
@RestController
@RequestMapping(ApiPath.COURSE)
@CrossOrigin //解决跨域
public class CourseController {

    @Autowired
    private EduCourseService courseService;
    @Autowired
    private FrontService frontService;

    /**
     * 新增课程
     *
     * @param courseInfoVO {@link CourseInfoVO}
     * @return {@link ReturnEntity}
     */
    @ApiOperation("新增课程")
    @PostMapping
    public ReturnEntity save(@RequestBody CourseInfoVO courseInfoVO) {
        String courseId = courseService.saveCourseInfo(courseInfoVO);
        return ReturnEntity.success().data("courseId", courseId);
    }

    /**
     * 删除课程
     *
     * @param courseId 课程ID
     * @return {@link ReturnEntity}
     */
    @ApiOperation("删除课程")
    @DeleteMapping("{courseId}")
    public ReturnEntity deleteCourse(@ApiParam("课程ID") @PathVariable("courseId") String courseId) {
        if(courseService.deleteCourse(courseId)) {
            return ReturnEntity.success();
        } else {
            return ReturnEntity.fail();
        }
    }

    /**
     * 更新课程
     *
     * @param courseId 课程ID
     * @param courseInfoVO {@link CourseInfoVO}
     * @return {@link ReturnEntity}
     */
    @ApiOperation("更新课程")
    @PostMapping("{courseId}")
    public ReturnEntity update(@ApiParam("课程ID") @PathVariable("courseId") String courseId,
                               @RequestBody CourseInfoVO courseInfoVO) {
        courseService.updateCourseInfo(courseId, courseInfoVO);
        return ReturnEntity.success();
    }

    /**
     * 查看课程信息
     *
     * @param courseId 课程ID
     * @return {@link ReturnEntity}
     */
    @ApiOperation("查看课程信息")
    @GetMapping("{courseId}")
    public ReturnEntity getCourse(@ApiParam("课程ID") @PathVariable("courseId") String courseId) {
        CourseInfoVO courseInfoVO = courseService.getCourse(courseId);
        return ReturnEntity.success().data("courseInfo", courseInfoVO);
    }

    /**
     * 查询指定日期系统课程数
     *
     * @return {@link ReturnEntity}
     */
    @GetMapping("/countCourse")
    public Integer countCourse() {
        return courseService.countCourse();
    }

    /**
     * 条件查询课程列表
     *
     * @param courseQueryVO {@link CourseQueryVO}
     * @return {@link ReturnEntity}
     */
    @ApiOperation("条件查询课程列表")
    @PutMapping
    public ReturnEntity list(@RequestBody(required = false) CourseQueryVO courseQueryVO) {
        Map<String, Object> courseMap = courseService.pageQueryCourse(courseQueryVO);
        return ReturnEntity.success().data(courseMap);
    }

    /**
     * 查询课程确认信息
     *
     * @param courseId 课程ID
     * @return {@link ReturnEntity}
     */
    @ApiOperation("课程确认信息")
    @GetMapping("publishInfo/{courseId}")
    public ReturnEntity getPublishCourseInfo(@ApiParam("课程ID") @PathVariable("courseId") String courseId) {
        CoursePublishDTO publishCourseInfo = courseService.publishCourseInfo(courseId);
        return ReturnEntity.success().data("publishCourseInfo", publishCourseInfo);
    }

    /**
     * 保存课程
     *
     * @param courseId 课程ID
     * @return {@link ReturnEntity}
     */
    @ApiOperation("保存课程")
    @PostMapping("saveCourse/{courseId}")
    public ReturnEntity saveCourse(@ApiParam("课程ID") @PathVariable("courseId") String courseId) {
        EduCourse eduCourse = courseService.getById(courseId);
        eduCourse.setStatus(CourseStatusEnums.Senior.value()); // 未发布
        courseService.updateById(eduCourse);
        return ReturnEntity.success();
    }

    /**
     * 课程最终发布
     *
     * @param courseId 课程ID
     * @return {@link ReturnEntity}
     */
    @ApiOperation("课程最终发布")
    @PostMapping("publishCourse/{courseId}")
    public ReturnEntity publishCourse(@ApiParam("课程ID") @PathVariable("courseId") String courseId) {
        EduCourse eduCourse = courseService.getById(courseId);
        eduCourse.setStatus(CourseStatusEnums.Chief.value()); // 发布
        courseService.updateById(eduCourse);
        return ReturnEntity.success();
    }

    /**
     * 根据课程id查询课程完整信息
     *
     * @param id 课程id
     * @return {@link CourseInfoDTO}
     */
    @ApiOperation("课程完整信息")
    @GetMapping("courseCompleteInfo/{id}")
    public CourseInfoDTO courseCompleteInfo(@ApiParam("课程id") @PathVariable("id") String id) {
        return frontService.courseCompleteInfo(id);
    }

}
