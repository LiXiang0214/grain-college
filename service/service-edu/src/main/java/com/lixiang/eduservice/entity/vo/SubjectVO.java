package com.lixiang.eduservice.entity.vo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 保存课程分类
 */
@Data
@Api(tags = "保存课程分类")
public class SubjectVO {

    @ApiModelProperty(value = "课程类别名称")
    private String title;

    @ApiModelProperty(value = "父分类ID")
    private String parentId;

    @ApiModelProperty(value = "排序字段")
    private Integer sort;

    @ApiModelProperty(value = "分类层级：1->一级,2->二级")
    private Integer subjectLevel;

}
