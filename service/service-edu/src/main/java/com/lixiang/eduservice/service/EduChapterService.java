package com.lixiang.eduservice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lixiang.eduservice.entity.EduChapter;
import com.lixiang.eduservice.entity.dto.ChapterDTO;

import java.util.List;

/**
 * 课程章节 业务层
 *
 * @author lixiang
 * @since 2020-08-27
 */
public interface EduChapterService extends IService<EduChapter> {

    /**
     * 删除章节
     *
     * @param chapterId 章节ID
     * @return 是否删除
     */
    boolean deleteChapter(String chapterId);

    /**
     * 课程id进行查询课程大纲列表
     *
     * @param courseId 课程ID
     * @return {@link List<ChapterDTO>}
     */
    List<ChapterDTO> getChapterAndVideoByCourseId(String courseId);

    /**
     * 根据课程ID删除章节
     *
     * @param courseId 课程ID
     */
    void removeChapterByCourseId(String courseId);

}
