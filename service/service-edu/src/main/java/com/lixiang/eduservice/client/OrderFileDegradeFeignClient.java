package com.lixiang.eduservice.client;

import org.springframework.stereotype.Component;


/**
 * 调用服务 熔断降级
 */
@Component
public class OrderFileDegradeFeignClient implements OrderClient {

    @Override
    public boolean isBuy(String memberId, String courseId) {
        return false;
    }
}
