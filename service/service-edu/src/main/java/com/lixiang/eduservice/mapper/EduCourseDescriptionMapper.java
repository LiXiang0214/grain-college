package com.lixiang.eduservice.mapper;

import com.lixiang.eduservice.entity.EduCourseDescription;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 课程简介 DAO层
 *
 * @author lixiang
 * @since 2020-08-27
 */
public interface EduCourseDescriptionMapper extends BaseMapper<EduCourseDescription> {

}
