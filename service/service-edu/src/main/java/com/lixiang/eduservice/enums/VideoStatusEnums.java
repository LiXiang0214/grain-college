package com.lixiang.eduservice.enums;

/**
 * 课程小节上传 状态
 *
 * @Auther: lixiang
 * @Date: 2020/8/27
 */
public enum VideoStatusEnums {
	Empty("Empty", "未上传"),
	Transcoding("Transcoding", "转码中"),
	Normal("Normal", "正常");

	private final String value;
	private final String desc;

	VideoStatusEnums(String value, String desc) {
		this.value = value;
		this.desc = desc;
	}

    /**
     * 获取描述
     *
     * @param value 值
     * @return desc
     */
    public static String getDesc(String value) {
        for (VideoStatusEnums enumValue : values()) {
            if (enumValue.value().equals(value)) {
                return enumValue.desc();
            }
        }
        return null;
    }

	/**
	 * 获取值
	 *
	 * @return value
	 */
	public String value() {
		return value;
	}

    /**
     * 获取描述
     *
     * @return desc
     */
    public String desc() {
        return desc;
    }

}
