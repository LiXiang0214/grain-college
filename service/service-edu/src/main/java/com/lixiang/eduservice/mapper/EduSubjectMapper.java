package com.lixiang.eduservice.mapper;

import com.lixiang.eduservice.entity.EduSubject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * 课程分类 DAO层
 *
 * @author lixiang
 * @since 2020-08-27
 */
public interface EduSubjectMapper extends BaseMapper<EduSubject> {

    /**
     * 查询所有分类
     *
     * @return 分类列表
     */
    List<EduSubject> selectAllSubjectList();
}
