package com.lixiang.eduservice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lixiang.eduservice.entity.EduChapter;

/**
 * 课程章节 DAO层
 *
 * @author lixiang
 * @since 2020-08-27
 */
public interface EduChapterMapper extends BaseMapper<EduChapter> {

}
