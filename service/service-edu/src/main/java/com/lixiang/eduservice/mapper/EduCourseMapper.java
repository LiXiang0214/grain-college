package com.lixiang.eduservice.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lixiang.eduservice.entity.EduCourse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lixiang.eduservice.entity.dto.CoursePublishDTO;
import com.lixiang.eduservice.entity.vo.CourseQueryVO;
import com.lixiang.servicebase.entity.dto.CourseInfoDTO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 课程 DAO层
 *
 * @author lixiang
 * @since 2020-08-27
 */
@Component
public interface EduCourseMapper extends BaseMapper<EduCourse> {

    /**
     * 根据课程id查询课程确认信息
     *
     * @param courseId 课程ID
     * @return {@link CoursePublishDTO}
     */
    CoursePublishDTO getPublishCourseInfo(String courseId);

    /**
     * 条件查询课程列表
     *
     * @param eduCoursePage 分页
     * @param courseQueryVO {@link CourseQueryVO}
     * @return 课程列表
     */
    List<EduCourse> pageQueryCourse(Page<EduCourse> eduCoursePage, @Param("courseQueryVO") CourseQueryVO courseQueryVO);

    /**
     * 根据课程id查询课程完整信息
     *
     * @param courseId 课程id
     * @return {@link CourseInfoDTO}
     */
    CourseInfoDTO courseCompleteInfo(String courseId);

    /**
     * 查询系统课程数
     *
     * @return 课程数
     */
    Integer countCourse();

    /**
     * 查询指定分类下课程数
     *
     * @param subjectId 分类ID
     * @return 课程数
     */
    Integer countCourseBySubjectId(String subjectId);

}
