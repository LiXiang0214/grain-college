package com.lixiang.eduservice.enums;

/**
 * 讲师头衔 枚举
 * @Auther: lixiang
 * @Date: 2020/8/27
 */
public enum TeacherLevelEnums {
	Senior(1, "高级讲师"),
	Chief(2, "首席讲师");

	private final Integer value;
	private final String desc;

	TeacherLevelEnums(Integer value, String desc) {
		this.value = value;
		this.desc = desc;
	}

    /**
     * 获取描述
     *
     * @param value 值
     * @return desc
     */
    public static String getDesc(Integer value) {
        for (TeacherLevelEnums enumValue : values()) {
            if (enumValue.value().equals(value)) {
                return enumValue.desc();
            }
        }
        return null;
    }

	/**
	 * 获取值
	 *
	 * @return value
	 */
	public Integer value() {
		return value;
	}

    /**
     * 获取描述
     *
     * @return desc
     */
    public String desc() {
        return desc;
    }

}
