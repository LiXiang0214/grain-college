package com.lixiang.eduservice.client;

import io.swagger.annotations.ApiParam;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * 调用订单服务配置
 */
@FeignClient(name = "service-order", fallback = OrderFileDegradeFeignClient.class) //调用的服务名称
@Component
public interface OrderClient { //定义调用的方法路径

    /**
     * 根据用户ID和课程ID判断课程是否已购买
     *
     * @param memberId 用户ID
     * @param courseId 课程ID
     * @return 订单信息
     */
    @GetMapping("/orderservice/order/isBuy/{memberId}/{courseId}")
    boolean isBuy(@ApiParam("用户ID") @PathVariable("memberId") String memberId,
                  @ApiParam("课程ID") @PathVariable("courseId") String courseId);

}
