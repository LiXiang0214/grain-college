package com.lixiang.eduservice.entity.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.Api;
import lombok.Data;

/**
 * 课程分类导入Execl实体
 */
@Data
@Api(tags = "课程分类导入Execl实体")
public class SubjectExeclVO {

    @ExcelProperty(index = 0)
    private String firstSubjectTitle;

    @ExcelProperty(index = 1)
    private Integer firstSubjectSort;

    @ExcelProperty(index = 2)
    private String secondSubjectTitle;

    @ExcelProperty(index = 3)
    private Integer secondSubjectSort;

}
