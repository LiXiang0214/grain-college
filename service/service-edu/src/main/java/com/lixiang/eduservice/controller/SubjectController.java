package com.lixiang.eduservice.controller;


import com.lixiang.commonutils.basic.ReturnEntity;
import com.lixiang.commonutils.enums.ResultCode;
import com.lixiang.eduservice.entity.EduSubject;
import com.lixiang.eduservice.entity.vo.SubjectVO;
import com.lixiang.eduservice.service.EduCourseService;
import com.lixiang.servicebase.exceptionhandler.GuliException;
import com.lixiang.eduservice.enums.ApiPath;
import com.lixiang.eduservice.service.EduSubjectService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 课程分类管理 控制器
 *
 * @author lixiang
 * @since 2020-08-27
 */
@Api(tags = "课程[分类管理]")
@RestController
@RequestMapping(ApiPath.SUBJECT)
@CrossOrigin //解决跨域
public class SubjectController {

    @Autowired
    private EduSubjectService subjectService;
    @Autowired
    private EduCourseService courseService;

    /**
     * 导入课程分类
     *
     * @param file 上传的execl文件
     * @return {@link ReturnEntity}
     */
    @ApiOperation("导入课程分类")
    @PostMapping("/importSubject")
    public ReturnEntity importSubject(MultipartFile file) {
        if (StringUtils.isEmpty(file)) {
            throw new GuliException(ResultCode.ERROR, "上传文件不能为空");
        }
        subjectService.importSubject(file, subjectService);
        return ReturnEntity.success().desc("导入成功！");
    }

    /**
     * 课程分类列表(树形)
     *
     * @return {@link ReturnEntity}
     */
    @ApiOperation("课程分类列表(树形)")
    @GetMapping
    public ReturnEntity<List<EduSubject>> listSubjectTree() {
//        return ReturnEntity.success().data("list", subjectService.listSubjectTree_recursion());
        return ReturnEntity.success().data("list", subjectService.listSubjectTreeMap());
    }

    /**
     * 新增分类
     *
     * @param subjectVO {@link SubjectVO}
     * @return {@link ReturnEntity}
     */
    @ApiOperation("新增分类")
    @PostMapping
    public ReturnEntity save(@RequestBody SubjectVO subjectVO) {
        EduSubject subject = new EduSubject();
        BeanUtils.copyProperties(subjectVO, subject);
        if(subjectService.save(subject)) {
            return ReturnEntity.success().desc("保存成功");
        } else {
            return ReturnEntity.fail().desc("保存失败");
        }
    }

    /**
     * 删除分类
     *
     * @param subjectId 分类ID
     * @return {@link ReturnEntity}
     */
    @ApiOperation("删除分类")
    @DeleteMapping("{subjectId}")
    public ReturnEntity delete(@ApiParam("分类ID") @PathVariable("subjectId") String subjectId) {
        // 1、验证子分类
        if (subjectService.isExistsChildren(subjectId)) {
            throw new GuliException(ResultCode.ERROR, "分类下存在子分类，不可删除！");
        }
        // 2、验证课程数
        if (courseService.isExistsCourse(subjectId)) {
            throw new GuliException(ResultCode.ERROR, "分类下存在课程，不可删除！");
        }
        // 3、删除
        if(subjectService.removeById(subjectId)) {
            return ReturnEntity.success().desc("删除成功");
        } else {
            return ReturnEntity.fail().desc("删除失败");
        }
    }

    /**
     * 更新分类
     *
     * @param subjectId 分类ID
     * @param subjectVO {@link SubjectVO}
     * @return {@link ReturnEntity}
     */
    @ApiOperation("更新分类")
    @PutMapping("{subjectId}")
    public ReturnEntity update(@ApiParam("分类ID") @PathVariable("subjectId") String subjectId,
                               @RequestBody SubjectVO subjectVO) {
        EduSubject subjectDB = subjectService.getById(subjectId);
        BeanUtils.copyProperties(subjectVO, subjectDB);
        if(subjectService.updateById(subjectDB)) {
            return ReturnEntity.success().desc("更新成功");
        } else {
            return ReturnEntity.fail().desc("更新失败");
        }
    }

    /**
     * 查看分类详情
     *
     * @param subjectId 分类ID
     * @return {@link EduSubject}
     */
    @ApiOperation("查看分类详情")
    @GetMapping("{subjectId}")
    public ReturnEntity<EduSubject> get(@ApiParam("分类ID") @PathVariable("subjectId") String subjectId) {
        return ReturnEntity.success().data("subject", subjectService.getById(subjectId));
    }

}

