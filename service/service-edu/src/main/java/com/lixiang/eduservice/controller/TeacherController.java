package com.lixiang.eduservice.controller;

import com.lixiang.commonutils.basic.ReturnEntity;
import com.lixiang.commonutils.enums.DelEnums;
import com.lixiang.eduservice.entity.EduTeacher;
import com.lixiang.eduservice.entity.vo.TeacherQueryVO;
import com.lixiang.eduservice.entity.vo.TeacherVO;
import com.lixiang.eduservice.enums.ApiPath;
import com.lixiang.eduservice.service.EduTeacherService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 讲师管理控制器
 *
 * @author lixiang
 * @since 2020-08-10
 */
@Api(tags = "讲师信息管理")
@RestController
@RequestMapping(ApiPath.TEACHER)
@CrossOrigin //解决跨域
public class TeacherController {

    @Autowired
    private EduTeacherService teacherService;

    /**
     * 新增讲师
     *
     * @param teacherVO {@link TeacherVO}
     * @return {@link ReturnEntity}
     */
    @ApiOperation("新增讲师")
    @PostMapping
    public ReturnEntity save(@RequestBody TeacherVO teacherVO) {
        EduTeacher eduTeacher = new EduTeacher();
        BeanUtils.copyProperties(teacherVO, eduTeacher);
        eduTeacher.setIsDeleted(DelEnums.Normal.value());
        if(teacherService.save(eduTeacher)) {
            return ReturnEntity.success().desc("保存成功");
        } else {
            return ReturnEntity.fail().desc("保存失败");
        }
    }

    /**
     * 逻辑删除讲师信息
     *
     * @param id 讲师ID
     * @return {@link ReturnEntity}
     */
    @ApiOperation("删除讲师信息")
    @DeleteMapping("{id}")
    public ReturnEntity delete(@ApiParam("讲师ID") @PathVariable("id") String id) {
        if(teacherService.removeById(id)) {
            return ReturnEntity.success().desc("删除成功");
        } else {
            return ReturnEntity.fail().desc("删除失败");
        }
    }

    /**
     * 更新讲师信息
     *
     * @param id 讲师ID
     * @param teacherVO {@link TeacherVO}
     * @return {@link ReturnEntity}
     */
    @ApiOperation("更新讲师信息")
    @PutMapping("{id}")
    public ReturnEntity update(@ApiParam("讲师ID") @PathVariable("id") String id,
                               @RequestBody TeacherVO teacherVO) {
        EduTeacher eduTeacherDB = teacherService.getById(id);
        BeanUtils.copyProperties(teacherVO, eduTeacherDB);
        if(teacherService.updateById(eduTeacherDB)) {
            return ReturnEntity.success().desc("更新成功");
        } else {
            return ReturnEntity.fail().desc("更新失败");
        }
    }

    /**
     * 查看讲师详情
     *
     * @param id 讲师ID
     * @return {@link EduTeacher}
     */
    @ApiOperation("查看讲师详情")
    @GetMapping("{id}")
    public ReturnEntity get(@ApiParam("讲师ID") @PathVariable("id") String id) {
        return ReturnEntity.success().data("teacher", teacherService.getById(id));
    }

    /**
     * 获取所有讲师
     *
     * @return {@link ReturnEntity}
     */
    @ApiOperation("获取所有讲师")
    @GetMapping()
    public ReturnEntity findAllTeacher() {
        List<EduTeacher> teacherList = teacherService.list(null);
        return ReturnEntity.success().data("teacherList", teacherList);
    }

    /**
     * 条件分页查询讲师列表
     *
     * @param teacherQueryVO {@link TeacherQueryVO}
     * @return {@link ReturnEntity} 讲师列表
     */
    @ApiOperation("条件分页查询讲师列表")
    @PutMapping("pageQuery")
    public ReturnEntity pageQueryTeacher(@RequestBody(required = false) TeacherQueryVO teacherQueryVO) {
        Map<String, Object> teachersMap = teacherService.pageQueryTeacher(teacherQueryVO);
        return ReturnEntity.success().data(teachersMap);
    }

}

