package com.lixiang.eduservice.client;

import com.lixiang.commonutils.basic.ReturnEntity;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 调用服务 熔断降级
 */
@Component
public class VodFileDegradeFeignClient implements VodClient {

    @Override
    public ReturnEntity removeAlyVideo(String id) {
        return ReturnEntity.fail().desc("删除视频出错了");
    }

    @Override
    public ReturnEntity deleteBatch(List<String> videoIdList) {
        return ReturnEntity.fail().desc("删除多个视频出错了");
    }

}
