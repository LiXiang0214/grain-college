package com.lixiang.eduservice.enums;

/**
 * 分类层级 枚举
 *
 * @Auther: lixiang
 * @Date: 2020/11/08
 */
public enum SubjectLevelEnums {
	ROOT(0, "根分类"),
	FATHER(1, "一级分类"),
	CHILD(2, "二级分类");

	private final Integer value;
	private final String desc;

	SubjectLevelEnums(Integer value, String desc) {
		this.value = value;
		this.desc = desc;
	}

    /**
     * 获取描述
     *
     * @param value 值
     * @return desc
     */
    public static String getDesc(Integer value) {
        for (SubjectLevelEnums enumValue : values()) {
            if (enumValue.value().equals(value)) {
                return enumValue.desc();
            }
        }
        return null;
    }

	/**
	 * 获取值
	 *
	 * @return value
	 */
	public Integer value() {
		return value;
	}

    /**
     * 获取描述
     *
     * @return desc
     */
    public String desc() {
        return desc;
    }

}
