package com.lixiang.eduservice.service.impl;

import com.alibaba.excel.EasyExcel;
import com.lixiang.eduservice.entity.EduSubject;
import com.lixiang.eduservice.listener.SubjectExcelListener;
import com.lixiang.eduservice.mapper.EduSubjectMapper;
import com.lixiang.eduservice.service.EduSubjectService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lixiang.eduservice.entity.vo.SubjectExeclVO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 课程分类 业务层实现
 *
 * @author lixiang
 * @since 2020-08-27
 */
@Service
public class EduSubjectServiceImpl extends ServiceImpl<EduSubjectMapper, EduSubject> implements EduSubjectService {

    /**
     * 导入课程分类
     *
     * @param file 上传的execl文件
     * @param subjectService {@link EduSubjectService}
     */
    @Override
    @Transactional(rollbackFor = {Exception.class})
    public void importSubject(MultipartFile file, EduSubjectService subjectService) {
        try {
            // 获取到输入的文件（会创建临时文件）
            InputStream subjectFile = file.getInputStream();
            // 调用方法读
            // 由于不能被 spring 管理，要每次读取excel都要new以实例化监听器
            EasyExcel.read(subjectFile, SubjectExeclVO.class, new SubjectExcelListener(subjectService))
                    .sheet().doRead();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * 课程分类列表(树形)[Map集合]
     *
     * @return 课程分类集合
     */
    @Override
    public List<EduSubject> listSubjectTreeMap() {
        // 1、返回集合
        List<EduSubject> rootSubjectList = new ArrayList<>();

        // 2、存放一级分类集合
        List<EduSubject> subjectTreeList = new ArrayList<>();

        // 3、查询所有分类
        List<EduSubject> allSubjectList = allSubjectList();

        // 4、所有分类的Map集合
        Map<String, EduSubject> subjectMap = new HashMap<>();
        for (EduSubject subject : allSubjectList) {
            subjectMap.put(subject.getId(), subject);
        }

        // 5、遍历所有分类
        for (EduSubject subject : allSubjectList) {
            EduSubject children = subject;
            // 根分类放到 rootSubjectList
            if ("00".equals(subject.getParentId())) {
                children.setParentSubjectTitle("根目录");
                rootSubjectList.add(children);
            // 一级分类放到 subjectTreeList
            } else if ("0".equals(children.getParentId())){
                subjectTreeList.add(children);
            // 否则到Map集合中找到该类型的父分类，将该分类放到父分类的子分类集合
            } else {
                EduSubject parentSubject = subjectMap.get(children.getParentId());
                parentSubject.getChildren().add(children);
            }
        }

        // 6、将一级分类集合放到根分类的子分类集合中
        rootSubjectList.get(0).setChildren(subjectTreeList);

        return rootSubjectList;

    }

    /**
     * 课程分类列表(树形)[递归]
     *
     * @return 课程分类集合
     */
    @Override
    public List<EduSubject> listSubjectTreeRecursion() {
        // 1、查询所有分类
        List<EduSubject> allSubjectList = allSubjectList();

        // 2、过滤出根分类，parentId = 00，并赋值子分类
        List<EduSubject> subjectTreeList = allSubjectList.stream().filter(subject ->
                "00".equals(subject.getParentId())
        ).map(subject -> { // map()映射方法，改变每一个category的指定属性
            // 3、调用递归方法，设置子分类，并改变子分类属性后返回
            subject.setChildren(getChildrenCategoryList(subject, allSubjectList));
            return subject;
        }).sorted( // 排序
                Comparator.comparingInt(EduSubject :: getSort)
        ).collect(Collectors.toList());

        return subjectTreeList;
    }

    /**
     * 递归查找指定分类的子分类
     *
     * @param rootSubject 指定分类
     * @param allSubjectList 分类集合
     * @return 子分类集合
     */
    private List<EduSubject> getChildrenCategoryList(EduSubject rootSubject, List<EduSubject> allSubjectList) {
        List<EduSubject> childrenSubjectList = allSubjectList.stream().filter(
                subject -> subject.getParentId().equals(rootSubject.getId())
        ).map(subject -> {
            // 递归调用自己，设置子分类
            subject.setChildren(getChildrenCategoryList(subject, allSubjectList));
            return subject;
        }).sorted( // 排序
                Comparator.comparingInt(EduSubject::getSort)
        ).collect(Collectors.toList());

        return childrenSubjectList;
    }


    /**
     * 判断是否存在子分类
     *
     * @param subjectId 分类ID
     * @return true：存在，false：不存在
     */
    @Override
    public boolean isExistsChildren(String subjectId) {
        // 1、查询所有分类
        List<EduSubject> allSubjectList = allSubjectList();

        // 2、父分类
        EduSubject fatherSubject = baseMapper.selectById(subjectId);

        // 3、遍历分类Map集合
        List<EduSubject> childrenList = allSubjectList.stream().filter(
                subject -> fatherSubject.getId().equals(subject.getParentId())
        ).collect(Collectors.toList());

        return childrenList.size() > 0;
    }

    /**
     * 查询所有分类
     *
     * @return 所有分类
     */
    public List<EduSubject> allSubjectList(){
        return baseMapper.selectAllSubjectList();
    }

}
