package com.lixiang.eduservice.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.lixiang.commonutils.enums.DelEnums;
import com.lixiang.servicebase.basic.BasicEntity;
import com.lixiang.eduservice.enums.TeacherLevelEnums;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * 讲师
 *
 * @author lixiang
 * @since 2020-08-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Api(tags = "讲师")
public class EduTeacher extends BasicEntity {

    @ApiModelProperty(value = "讲师ID")
    @TableId(value = "id", type = IdType.ID_WORKER_STR)
    private String id;

    @ApiModelProperty(value = "讲师姓名")
    private String name;

    @ApiModelProperty(value = "讲师简介")
    private String intro;

    @ApiModelProperty(value = "讲师资历,一句话说明讲师")
    private String career;

    @ApiModelProperty(value = "头衔 1高级讲师 2首席讲师")
    private Integer level;

    @ApiModelProperty("头衔说明")
    @TableField(exist = false)
    private String levelDesc;

    @ApiModelProperty(value = "讲师头像")
    private String avatar;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @TableLogic
    @ApiModelProperty(value = "逻辑删除 1（true）已删除， 0（false）未删除")
    private Integer isDeleted;

    @ApiModelProperty("删除状态说明")
    @TableField(exist = false)
    private String isDeletedDesc;

    public String getIsDeletedDesc() {
        return DelEnums.getDesc(this.isDeleted);
    }

    public String getLevelDesc() {
        return TeacherLevelEnums.getDesc(this.level);
    }

}
