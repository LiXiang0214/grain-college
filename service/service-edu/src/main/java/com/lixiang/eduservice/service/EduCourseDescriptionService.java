package com.lixiang.eduservice.service;

import com.lixiang.eduservice.entity.EduCourseDescription;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 课程简介 业务层
 *
 * @author lixiang
 * @since 2020-08-27
 */
public interface EduCourseDescriptionService extends IService<EduCourseDescription> {

}
