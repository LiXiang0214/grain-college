package com.lixiang.eduservice.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lixiang.commonutils.enums.ResultCode;
import com.lixiang.eduservice.enums.SubjectLevelEnums;
import com.lixiang.servicebase.exceptionhandler.GuliException;
import com.lixiang.eduservice.entity.EduSubject;
import com.lixiang.eduservice.service.EduSubjectService;
import com.lixiang.eduservice.entity.vo.SubjectExeclVO;
import org.springframework.util.StringUtils;

/**
 * 课程分类EasyExecl监听类
 */
public class SubjectExcelListener extends AnalysisEventListener<SubjectExeclVO> {

    private EduSubjectService subjectService;
    public SubjectExcelListener(EduSubjectService subjectService) {
        this.subjectService = subjectService;
    }

    /**
     * 解析分类，每解析一行会回调invoke()方法
     *
     * @param subjectExeclVO {@link SubjectExeclVO}
     * @param analysisContext {@link AnalysisContext}
     */
    @Override
    public void invoke(SubjectExeclVO subjectExeclVO, AnalysisContext analysisContext) {
        if (StringUtils.isEmpty(subjectExeclVO)) {
            throw new GuliException(ResultCode.ERROR, "文件不能为空");
        }

        // 添加一级分类
        EduSubject firstSubject = this.getFirstSubject(subjectService, subjectExeclVO.getFirstSubjectTitle());
        if (StringUtils.isEmpty(firstSubject)) { // 判重，无相同一级分类，添加
            firstSubject = new EduSubject();
            firstSubject.setTitle(subjectExeclVO.getFirstSubjectTitle());
            firstSubject.setParentId("0");
            firstSubject.setSort(subjectExeclVO.getFirstSubjectSort());
            firstSubject.setSubjectLevel(SubjectLevelEnums.FATHER.value());
            subjectService.save(firstSubject);
        }

        // 父分类ID
        String parentId = firstSubject.getId();

        // 添加二级分类
        EduSubject secondSubject = this.getSecondSubject(subjectService, subjectExeclVO.getSecondSubjectTitle(), parentId);
        if (StringUtils.isEmpty(secondSubject)) { // 判重，无相同二级分类，添加
            secondSubject = new EduSubject();
            secondSubject.setParentId(parentId);
            secondSubject.setTitle(subjectExeclVO.getSecondSubjectTitle());
            secondSubject.setSort(subjectExeclVO.getSecondSubjectSort());
            secondSubject.setSubjectLevel(SubjectLevelEnums.CHILD.value());
            subjectService.save(secondSubject);
        }

    }

    /**
     * 查询一级分类
     *
     * @param subjectService {@link EduSubjectService}
     * @param subjectTitle 一级分类title
     * @return 一级分类
     */
    public EduSubject getFirstSubject(EduSubjectService subjectService, String subjectTitle) {
        QueryWrapper<EduSubject> wrapper = new QueryWrapper<>();
        wrapper.eq("title", subjectTitle);
        wrapper.eq("parent_id", "0");
        EduSubject firstSubject = subjectService.getOne(wrapper);
        return firstSubject;
    }

    /**
     * 查询二级分类
     *
     * @param subjectService {@link EduSubjectService}
     * @param subjectTitle 二级分类title
     * @param parentId 父分类ID
     * @return 二级分类
     */
    public EduSubject getSecondSubject(EduSubjectService subjectService, String subjectTitle, String parentId) {
        QueryWrapper<EduSubject> wrapper = new QueryWrapper<>();
        wrapper.eq("title", subjectTitle);
        wrapper.eq("parent_id", parentId);
        EduSubject secondSubject = subjectService.getOne(wrapper);
        return secondSubject;
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) { }

}
