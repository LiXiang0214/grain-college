package com.lixiang.eduservice.entity.vo;

import com.lixiang.servicebase.basic.BaseQueryVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 查询课程
 */
@Data
@Api(tags = "查询课程")
public class CourseQueryVO extends BaseQueryVO {

    @ApiModelProperty(value = "课程名称[模糊查询]")
    private String title;

    @ApiModelProperty(value = "课程状态 Draft未发布  Normal已发布")
    private String status;

    @ApiModelProperty(value = "讲师姓名[模糊查询]")
    private String teacherName;

    @ApiModelProperty(value = "查询开始时间", example = "2019-01-01 10:10:10")
    private String beginTime;

    @ApiModelProperty(value = "查询结束时间", example = "2019-12-01 10:10:10")
    private String endTime;

}
