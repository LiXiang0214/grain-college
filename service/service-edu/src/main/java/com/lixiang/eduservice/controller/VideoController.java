package com.lixiang.eduservice.controller;

import com.lixiang.commonutils.basic.ReturnEntity;
import com.lixiang.eduservice.entity.EduVideo;
import com.lixiang.eduservice.entity.vo.VideoVO;
import com.lixiang.eduservice.enums.ApiPath;
import com.lixiang.eduservice.service.EduVideoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 课程小节 控制器
 *
 * @author lixiang
 * @since 2020-08-27
 */
@Api(tags = "课程[小节管理]")
@RestController
@RequestMapping(ApiPath.VIDEO)
@CrossOrigin //解决跨域
public class VideoController {

    @Autowired
    private EduVideoService videoService;

    /**
     * 新增小节
     *
     * @param videoVO {@link VideoVO}
     * @return {@link ReturnEntity}
     */
    @ApiOperation("新增小节")
    @PostMapping
    public ReturnEntity save(@RequestBody VideoVO videoVO) {
        EduVideo video = new EduVideo();
        BeanUtils.copyProperties(videoVO, video);
        if(videoService.save(video)) {
            return ReturnEntity.success().desc("保存成功");
        } else {
            return ReturnEntity.fail().desc("保存失败");
        }
    }

    /**
     * 删除小节(同时删除视频)
     *
     * @param id 小节ID
     * @return {@link ReturnEntity}
     */
    @ApiOperation("删除小节(同时删除视频)")
    @DeleteMapping("{id}")
    public ReturnEntity deleteVideo(@ApiParam("小节ID") @PathVariable("id") String id) {
        if(videoService.deleteVideo(id)) {
            return ReturnEntity.success();
        } else {
            return ReturnEntity.fail();
        }
    }

    /**
     * 更新小节
     *
     * @param id 小节ID
     * @param videoVO {@link VideoVO}
     * @return {@link ReturnEntity}
     */
    @ApiOperation("更新小节")
    @PostMapping("{id}")
    public ReturnEntity upload(@ApiParam("小节ID") @PathVariable("id") String id,
                               @RequestBody VideoVO videoVO) {
        EduVideo video = videoService.getById(id);
        BeanUtils.copyProperties(videoVO, video);
        if(videoService.updateById(video)) {
            return ReturnEntity.success().desc("保存成功");
        } else {
            return ReturnEntity.fail().desc("保存失败");
        }
    }

    /**
     * 获取小节信息
     *
     * @param id 小节ID
     * @return {@link ReturnEntity}
     */
    @ApiOperation("获取小节信息")
    @GetMapping("{id}")
    public ReturnEntity upload(@ApiParam("小节ID") @PathVariable("id") String id) {
        return ReturnEntity.success().data("video", videoService.getById(id));
    }

}

