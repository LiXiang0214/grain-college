package com.lixiang.eduservice.mapper;

import com.lixiang.eduservice.entity.EduTeacher;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Component;

/**
 * 讲师管理 DAO层
 *
 * @author lixiang
 * @since 2020-08-10
 */
@Component
public interface EduTeacherMapper extends BaseMapper<EduTeacher> {

}
