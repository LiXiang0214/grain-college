package com.lixiang.eduservice.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lixiang.commonutils.enums.DelEnums;
import com.lixiang.commonutils.enums.ResultCode;
import com.lixiang.servicebase.exceptionhandler.GuliException;
import com.lixiang.eduservice.entity.EduCourse;
import com.lixiang.eduservice.entity.EduCourseDescription;
import com.lixiang.eduservice.entity.vo.CourseInfoVO;
import com.lixiang.eduservice.entity.dto.CoursePublishDTO;
import com.lixiang.eduservice.entity.vo.CourseQueryVO;
import com.lixiang.eduservice.mapper.EduCourseMapper;
import com.lixiang.eduservice.service.EduChapterService;
import com.lixiang.eduservice.service.EduCourseDescriptionService;
import com.lixiang.eduservice.service.EduCourseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lixiang.eduservice.service.EduVideoService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 课程 业务层实现
 *
 * @author lixiang
 * @since 2020-08-27
 */
@Service
public class EduCourseServiceImpl extends ServiceImpl<EduCourseMapper, EduCourse> implements EduCourseService {

    @Autowired
    private EduCourseDescriptionService courseDescriptionService;
    @Autowired
    private EduChapterService chapterService;
    @Autowired
    private EduVideoService videoService;


    /**
     * 新增课程
     *
     * @param courseInfoVO {@link CourseInfoVO}
     * @return 返回课程id，为了后面添加大纲使用
     */
    @Override
    @Transactional(rollbackFor = {Exception.class})
    public String saveCourseInfo(CourseInfoVO courseInfoVO) {
        // 1、向课程表添加课程基本信息
        EduCourse course = new EduCourse();
        BeanUtils.copyProperties(courseInfoVO, course);
        course.setIsDeleted(DelEnums.Normal.value());
        int insert = baseMapper.insert(course);
        if(insert == 0) {
            //添加失败
            throw new GuliException(ResultCode.ERROR, "添加课程信息失败");
        }
        //获取添加之后课程id
        String courseId = course.getId();

        // 2、向课程简介表添加课程简介
        EduCourseDescription courseDescription = new EduCourseDescription();
        courseDescription.setDescription(courseInfoVO.getDescription());
        // 设置描述id就是课程id
        courseDescription.setId(courseId);
        courseDescriptionService.save(courseDescription);

        return courseId;
    }

    /**
     * 更新课程
     *
     * @param courseId 课程ID
     * @param courseInfoVO {@link CourseInfoVO}
     */
    @Override
    @Transactional(rollbackFor = {Exception.class})
    public void updateCourseInfo(String courseId, CourseInfoVO courseInfoVO) {
        // 1、更新课程基本信息
        EduCourse course = baseMapper.selectById(courseId);
        BeanUtils.copyProperties(courseInfoVO, course);
        int update = baseMapper.updateById(course);
        if(update == 0) {
            // 更新失败
            throw new GuliException(ResultCode.ERROR, "更新课程信息失败");
        }

        // 2、更新课程简介
        EduCourseDescription description = new EduCourseDescription();
        description.setDescription(courseInfoVO.getDescription());
        // 设置描述id = 课程id
        description.setId(courseId);
        courseDescriptionService.updateById(description);

    }

    /**
     * 查看课程信息
     *
     * @param courseId 课程ID
     * @return {@link EduCourse}
     */
    @Override
    public CourseInfoVO getCourse(String courseId) {
        //1 查询课程表
        CourseInfoVO courseInfoVO = new CourseInfoVO();
        EduCourse courseDB = baseMapper.selectById(courseId);
        BeanUtils.copyProperties(courseDB, courseInfoVO);
        //2 查询描述表
        courseInfoVO.setDescription(courseDescriptionService.getById(courseId).getDescription());

        return courseInfoVO;
    }

    /**
     * 查询系统课程数
     *
     * @return 课程数
     */
    @Override
    public Integer countCourse() {
        return baseMapper.countCourse();
    }

    /**
     * 根据课程id查询课程确认信息
     *
     * @param courseId 课程ID
     * @return {@link CoursePublishDTO}
     */
    @Override
    public CoursePublishDTO publishCourseInfo(String courseId) {
        //调用mapper
        CoursePublishDTO publishCourseInfo = baseMapper.getPublishCourseInfo(courseId);
        return publishCourseInfo;
    }

    /**
     * 删除课程
     *
     * @param courseId 课程ID
     * @return 删除与否
     */
    @Override
    @Transactional(rollbackFor = {Exception.class})
    public boolean deleteCourse(String courseId) {
        // 1、根据课程ID删除小节
        videoService.removeVideoByCourseId(courseId);

        // 2、根据课程ID删除章节
        chapterService.removeChapterByCourseId(courseId);

        // 3、根据课程ID删除描述(两个的ID一样的)
        courseDescriptionService.removeById(courseId);

        // 4、根据课程ID删除课程本身
        // 删除成功：1>0 --> true  删除失败：0>0 --> false
        return baseMapper.deleteById(courseId) > 0;
    }

    /**
     * 条件查询课程列表
     *
     * @param courseQueryVO {@link CourseQueryVO}
     * @return 课程列表
     */
    @Override
    public Map<String, Object> pageQueryCourse(CourseQueryVO courseQueryVO) {
        Map<String, Object> courseMap = new HashMap<>();
        Page<EduCourse> eduCoursePage = new Page<>(courseQueryVO.getPage(), courseQueryVO.getLimit());
        List<EduCourse> courseList = baseMapper.pageQueryCourse(eduCoursePage, courseQueryVO);
        long total = eduCoursePage.getTotal(); // 总记录数
        courseMap.put("total", total);
        courseMap.put("courseList", courseList);
        return courseMap;
    }

    /**
     * 判断分类下是否存在课程
     *
     * @param subjectId 分类ID
     * @return true：存在，false：不存在
     */
    @Override
    public boolean isExistsCourse(String subjectId) {
        return baseMapper.countCourseBySubjectId(subjectId) > 0;
    }

}
