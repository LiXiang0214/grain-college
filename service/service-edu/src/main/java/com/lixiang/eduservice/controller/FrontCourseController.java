package com.lixiang.eduservice.controller;

import com.lixiang.commonutils.basic.ReturnEntity;
import com.lixiang.commonutils.util.JwtUtils;
import com.lixiang.eduservice.client.OrderClient;
import com.lixiang.eduservice.entity.dto.ChapterDTO;
import com.lixiang.eduservice.entity.vo.CourseQueryVO;
import com.lixiang.eduservice.entity.vo.FrontCourseQueryVO;
import com.lixiang.eduservice.enums.ApiPath;
import com.lixiang.eduservice.service.EduChapterService;
import com.lixiang.eduservice.service.FrontService;
import com.lixiang.servicebase.entity.dto.CourseInfoDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * 前台[课程] 控制器
 *
 * @author lixiang
 * @since 2020-09-14
 */
@Api(tags = "前台[课程]")
@RestController
@RequestMapping(ApiPath.FRONTCOURSE)
@CrossOrigin //解决跨域
public class FrontCourseController {

    @Autowired
    private FrontService frontService;
    @Autowired
    private EduChapterService chapterService;
    @Autowired
    private OrderClient orderClient;

    /**
     * 分页查询课程
     *
     * @param courseQueryVO {@link CourseQueryVO}
     * @return {@link ReturnEntity}
     */
    @ApiOperation("分页查询课程")
    @PutMapping
    public ReturnEntity courseList(@RequestBody FrontCourseQueryVO courseQueryVO) {
        return ReturnEntity.success().data(frontService.courseList(courseQueryVO));
    }

    /**
     * 根据课程id查询课程完整信息及章节小节
     *
     * @param courseId 课程id
     * @param request {@link HttpServletRequest}
     * @return {@link ReturnEntity}
     */
    @ApiOperation("课程完整信息及章节小节")
    @GetMapping("courseAndChapterInfo/{courseId}")
    public ReturnEntity courseAndChapterInfo(@ApiParam("课程id") @PathVariable("courseId") String courseId,
                                             HttpServletRequest request) {
        // 课程完整信息
        CourseInfoDTO courseInfoDTO = frontService.courseCompleteInfo(courseId);

        //根据课程id查询章节和小节
        List<ChapterDTO> chapterVideoList = chapterService.getChapterAndVideoByCourseId(courseId);

        // 查询用户是否已购买
        String memberId = JwtUtils.getMemberIdByJwtToken(request);
        boolean isBuy = false;
        if (!StringUtils.isEmpty(memberId)) {
            isBuy = orderClient.isBuy(memberId, courseId);
        }

        return ReturnEntity.success()
                .data("courseWebDTO", courseInfoDTO)
                .data("chapterVideoList", chapterVideoList)
                .data("isBuy", isBuy);
    }

}

