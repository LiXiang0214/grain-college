package com.lixiang.eduservice.controller;

import com.lixiang.commonutils.basic.ReturnEntity;
import com.lixiang.eduservice.enums.ApiPath;
import com.lixiang.eduservice.service.FrontService;
import com.lixiang.servicebase.basic.BaseQueryVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 前台[讲师] 控制器
 *
 * @author lixiang
 * @since 2020-09-14
 */
@Api(tags = "前台[讲师]")
@RestController
@RequestMapping(ApiPath.FRONTTEACHER)
@CrossOrigin //解决跨域
public class FrontTeacherController {

    @Autowired
    private FrontService frontService;

    /**
     * 分页查询讲师
     *
     * @param baseQueryVO {@link BaseQueryVO}
     * @return {@link ReturnEntity}
     */
    @ApiOperation("分页查询讲师")
    @PutMapping
    public ReturnEntity teacherList(@RequestBody BaseQueryVO baseQueryVO) {
        return ReturnEntity.success().data(frontService.teacherList(baseQueryVO));
    }

    /**
     * 讲师详情及课程
     *
     * @param teacherId 讲师ID
     * @return {@link ReturnEntity}
     */
    @ApiOperation("讲师详情及课程")
    @GetMapping("{teacherId}")
    public ReturnEntity teacherInfo(@ApiParam("讲师ID") @PathVariable("teacherId") String teacherId) {
        return ReturnEntity.success().data(frontService.teacherInfo(teacherId));
    }

}

