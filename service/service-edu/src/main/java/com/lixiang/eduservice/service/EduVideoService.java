package com.lixiang.eduservice.service;

import com.lixiang.eduservice.entity.EduVideo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 课程小节 业务层
 *
 * @author lixiang
 * @since 2020-08-27
 */
public interface EduVideoService extends IService<EduVideo> {

    /**
     * 删除小节(同时删除视频)
     *
     * @param id 小节ID
     * @return 删除与否
     */
    boolean deleteVideo(String id);

    /**
     * 根据课程ID删除小节
     *
     * @param courseId 课程ID
     */
    void removeVideoByCourseId(String courseId);

}
