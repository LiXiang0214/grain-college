package com.lixiang.eduservice.enums;

/**
 * 路径地址配置
 *
 * @author lixiang
 */
public class ApiPath {

	/**
	 * edu根地址
	 */
	public static final String EDU = "/eduservice";

	/**
	 * 讲师地址
	 */
	public static final String TEACHER = EDU + "/edu-teacher";

	/**
	 * 科目管理地址
	 */
	public static final String SUBJECT = EDU + "/edu-subject";

	/**
	 * 课程管理地址
	 */
	public static final String COURSE = EDU + "/edu-course";

	/**
	 * 课程章节地址
	 */
	public static final String CHAPTER = EDU + "/edu-chapter";

	/**
	 * 课程视频地址
	 */
	public static final String VIDEO = EDU + "/edu-video";

	/**
	 * 前台数据
	 */
	public static final String FRONT = EDU + "/front";

	/**
	 * 前台[首页]地址
	 */
	public static final String INDEX = FRONT + "/index";

	/**
	 * 前台[讲师]地址
	 */
	public static final String FRONTTEACHER = FRONT + "/teacher";

	/**
	 * 前台[课程]地址
	 */
	public static final String FRONTCOURSE = FRONT + "/course";

	/**
	 * 登录
	 */
	public static final String LOGIN = EDU + "/login";


	protected ApiPath() {

    }

}
