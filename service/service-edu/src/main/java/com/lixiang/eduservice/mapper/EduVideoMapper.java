package com.lixiang.eduservice.mapper;

import com.lixiang.eduservice.entity.EduVideo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 课程小节 DAO层
 *
 * @author lixiang
 * @since 2020-08-27
 */
public interface EduVideoMapper extends BaseMapper<EduVideo> {

}
