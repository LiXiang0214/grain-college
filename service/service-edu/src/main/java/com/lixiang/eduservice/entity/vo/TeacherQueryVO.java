package com.lixiang.eduservice.entity.vo;

import com.lixiang.servicebase.basic.BaseQueryVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 查询教师
 */
@Data
@Api(tags = "查询教师")
public class TeacherQueryVO extends BaseQueryVO {

    @ApiModelProperty(value = "教师名称[模糊查询]")
    private String name;

    @ApiModelProperty(value = "头衔：1 -> 高级讲师，2 -> 首席讲师")
    private Integer level;

    @ApiModelProperty(value = "查询开始时间", example = "2019-01-01 10:10:10")
    private String beginTime;

    @ApiModelProperty(value = "查询结束时间", example = "2019-12-01 10:10:10")
    private String endTime;

}
