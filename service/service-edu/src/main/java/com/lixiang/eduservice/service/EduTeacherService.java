package com.lixiang.eduservice.service;

import com.lixiang.eduservice.entity.EduTeacher;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lixiang.eduservice.entity.vo.TeacherQueryVO;

import java.util.Map;

/**
 * 讲师管理 业务层
 *
 * @author lixiang
 * @since 2020-08-10
 */
public interface EduTeacherService extends IService<EduTeacher> {

    /**
     * 条件分页查询教师列表
     *
     * @param teacherQueryVO {@link TeacherQueryVO}
     * @return 教师列表信息
     */
    Map<String, Object> pageQueryTeacher(TeacherQueryVO teacherQueryVO);

}
