package com.lixiang.eduservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lixiang.eduservice.entity.EduCourse;
import com.lixiang.eduservice.entity.EduTeacher;
import com.lixiang.eduservice.entity.vo.FrontCourseQueryVO;
import com.lixiang.eduservice.mapper.EduCourseMapper;
import com.lixiang.eduservice.mapper.EduTeacherMapper;
import com.lixiang.eduservice.service.EduCourseService;
import com.lixiang.eduservice.service.EduTeacherService;
import com.lixiang.eduservice.service.FrontService;
import com.lixiang.servicebase.basic.BaseQueryVO;
import com.lixiang.servicebase.entity.dto.CourseInfoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 前台 业务层实现
 *
 * @author lixiang
 * @since 2020-08-10
 */
@Service
public class FrontServiceImpl implements FrontService {

    @Autowired
    private EduCourseService courseService;
    @Autowired
    private EduTeacherService teacherService;
    @Autowired
    private EduTeacherMapper teacherMapper;
    @Autowired
    private EduCourseMapper courseMapper;

    /**
     * 查询前8条热门课程
     *
     * @return 课程列表
     */
    @Cacheable(value = "index", key = "'popularCourse'")
    @Override
    public List<EduCourse> queryPopularCourse() {
        return courseService.list(new QueryWrapper<EduCourse>()
                .orderByDesc("view_count") // 对应数据库字段
                .last("limit 8"));
    }

    /**
     * 查询前4条热门名师
     *
     * @return 名师列表
     */
    @Cacheable(value = "index", key = "'popularTeacher'")
    @Override
    public List<EduTeacher> queryPopularTeacher() {
        return teacherService.list(new QueryWrapper<EduTeacher>()
                .orderByAsc("id")
                .last("limit 4"));
    }

    /**
     * 分页查询讲师
     *
     * @param baseQueryVO {@link BaseQueryVO}
     * @return 教师列表信息
     */
    @Override
    public Map<String, Object> teacherList(BaseQueryVO baseQueryVO) {
        Page<EduTeacher> teacherPage = new Page<>(baseQueryVO.getPage(), baseQueryVO.getLimit());
        QueryWrapper<EduTeacher> teacherWrapper = new QueryWrapper<>();
        teacherWrapper.orderByDesc("sort");
        // 查询
        teacherMapper.selectPage(teacherPage, teacherWrapper);

        //把分页数据获取出来，放到map集合
        Map<String, Object> teacherMap = new HashMap<>();
        teacherMap.put("teacherList", teacherPage.getRecords());
        teacherMap.put("current", teacherPage.getCurrent());
        teacherMap.put("pages", teacherPage.getPages());
        teacherMap.put("size", teacherPage.getSize());
        teacherMap.put("total", teacherPage.getTotal());
        teacherMap.put("hasPrevious", teacherPage.hasPrevious()); // 上一页
        teacherMap.put("hasNext", teacherPage.hasNext()); // 下一页

        return teacherMap;
    }

    /**
     * 讲师详情及课程
     *
     * @param teacherId 讲师ID
     * @return 讲师详情及课程信息集合
     */
    @Override
    public Map<String, Object> teacherInfo(String teacherId) {
        // 1、根据讲师id查询讲师基本信息
        EduTeacher teacher = teacherService.getById(teacherId);

        // 2、根据讲师id查询所讲课程
        QueryWrapper<EduCourse> courseWrapper = new QueryWrapper<EduCourse>().eq("teacher_id",teacherId);
        List<EduCourse> courseList = courseService.list(courseWrapper);

        Map<String, Object> teacherInfoMap = new HashMap<>();
        teacherInfoMap.put("teacherInfo", teacher);
        teacherInfoMap.put("courseList", courseList);

        return teacherInfoMap;
    }

    /**
     * 分页查询课程
     *
     * @param courseQueryVO {@link FrontCourseQueryVO}
     * @return 课程信息列表集合
     */
    @Override
    public Map<String, Object> courseList(FrontCourseQueryVO courseQueryVO) {
        Page<EduCourse> coursePage = new Page<>(courseQueryVO.getPage(), courseQueryVO.getLimit());
        QueryWrapper<EduCourse> courseWrapper = new QueryWrapper<>();
//        courseWrapper.orderByDesc("view_count");
        if (!StringUtils.isEmpty(courseQueryVO.getTitle())) { // 标题模糊查询
            courseWrapper.eq("title", courseQueryVO.getTitle());
        }
        //判断条件值是否为空，不为空拼接
        if(!StringUtils.isEmpty(courseQueryVO.getSubjectParentId())) { //一级分类
            courseWrapper.eq("subject_parent_id",courseQueryVO.getSubjectParentId());
        }
        if(!StringUtils.isEmpty(courseQueryVO.getSubjectId())) { //二级分类
            courseWrapper.eq("subject_id",courseQueryVO.getSubjectId());
        }
        if(!StringUtils.isEmpty(courseQueryVO.getBuyCountSort())) { //关注度
            courseWrapper.orderByDesc("buy_count");
        }
        if (!StringUtils.isEmpty(courseQueryVO.getGmtCreateSort())) { //最新
            courseWrapper.orderByDesc("gmt_create");
        }

        if (!StringUtils.isEmpty(courseQueryVO.getPriceSort())) {//价格
            courseWrapper.orderByDesc("price");
        }
        // 查询
        courseMapper.selectPage(coursePage, courseWrapper);

        //把分页数据获取出来，放到map集合
        Map<String, Object> courseMap = new HashMap<>();
        courseMap.put("courseList", coursePage.getRecords());
        courseMap.put("current", coursePage.getCurrent());
        courseMap.put("pages", coursePage.getPages());
        courseMap.put("size", coursePage.getSize());
        courseMap.put("total", coursePage.getTotal());
        courseMap.put("hasNext", coursePage.hasNext()); // 下一页
        courseMap.put("hasPrevious", coursePage.hasPrevious()); // 上一页

        return courseMap;
    }

    /**
     * 根据课程id查询课程完整信息
     *
     * @param id 课程id
     * @return {@link CourseInfoDTO}
     */
    @Override
    public CourseInfoDTO courseCompleteInfo(String id) {
        return courseMapper.courseCompleteInfo(id);
    }

}
