package com.lixiang.eduservice.service;

import com.lixiang.eduservice.entity.EduCourse;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lixiang.eduservice.entity.vo.CourseInfoVO;
import com.lixiang.eduservice.entity.dto.CoursePublishDTO;
import com.lixiang.eduservice.entity.vo.CourseQueryVO;

import java.util.Map;

/**
 * 课程 业务层
 *
 * @author lixiang
 * @since 2020-08-27
 */
public interface EduCourseService extends IService<EduCourse> {

    /**
     * 新增课程
     *
     * @param courseInfoVO {@link CourseInfoVO}
     * @return 返回课程id，为了后面添加大纲使用
     */
    String saveCourseInfo(CourseInfoVO courseInfoVO);

    /**
     * 更新课程
     *
     * @param courseId 课程ID
     * @param courseInfoVO {@link CourseInfoVO}
     */
    void updateCourseInfo(String courseId, CourseInfoVO courseInfoVO);

    /**
     * 查看课程信息
     *
     * @param courseId 课程ID
     * @return {@link EduCourse}
     */
    CourseInfoVO getCourse(String courseId);

    /**
     * 查询系统课程数
     *
     * @return 课程数
     */
    Integer countCourse();

    /**
     * 根据课程id查询课程确认信息
     *
     * @param courseId 课程ID
     * @return {@link CoursePublishDTO}
     */
    CoursePublishDTO publishCourseInfo(String courseId);

    /**
     * 删除课程
     *
     * @param courseId 课程ID
     * @return 删除与否
     */
    boolean deleteCourse(String courseId);

    /**
     * 条件查询课程列表
     *
     * @param courseQueryVO {@link CourseQueryVO}
     * @return 课程列表
     */
    Map<String, Object> pageQueryCourse(CourseQueryVO courseQueryVO);

    /**
     * 判断分类下是否存在课程
     *
     * @param subjectId 分类ID
     * @return true：存在，false：不存在
     */
    boolean isExistsCourse(String subjectId);

}
