package com.lixiang.eduservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lixiang.commonutils.basic.ReturnEntity;
import com.lixiang.commonutils.enums.ResultCode;
import com.lixiang.servicebase.exceptionhandler.GuliException;
import com.lixiang.eduservice.client.VodClient;
import com.lixiang.eduservice.entity.EduVideo;
import com.lixiang.eduservice.mapper.EduVideoMapper;
import com.lixiang.eduservice.service.EduVideoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 课程小节 业务层实现
 *
 * @author lixiang
 * @since 2020-08-27
 */
@Service
public class EduVideoServiceImpl extends ServiceImpl<EduVideoMapper, EduVideo> implements EduVideoService {

    @Autowired
    private VodClient vodClient;

    @Override
    @Transactional(rollbackFor = {Exception.class})
    public boolean deleteVideo(String id) {
        // 根据小节id获取视频id
        EduVideo eduVideo = getById(id);
        String videoSourceId = eduVideo.getVideoSourceId();
        // 判断小节里面是否有视频id
        if(!StringUtils.isEmpty(videoSourceId)) {
            // 根据视频id，远程调用实现视频删除
            ReturnEntity deleteVideoResult = vodClient.removeAlyVideo(videoSourceId);
            if(deleteVideoResult.getCode() == ResultCode.ERROR) {
                throw new GuliException(ResultCode.ERROR, "删除视频失败，熔断器...");
            }
        }
        // 删除小节
        // 删除成功：1>0 --> true  删除失败：0>0 --> false
        return baseMapper.deleteById(id) > 0;
    }

    @Override
    @Transactional(rollbackFor = {Exception.class})
    public void removeVideoByCourseId(String courseId) {
        //1 根据课程id查询课程所有的视频id
        QueryWrapper<EduVideo> videoWrapper = new QueryWrapper<>();
        videoWrapper.eq("course_id",courseId);
        videoWrapper.select("video_source_id");
        List<EduVideo> videoList = baseMapper.selectList(videoWrapper);

        // 2、List<EduVideo>变成List<String>
        List<String> videoIds = new ArrayList<>();
        videoList.forEach(video -> {
            videoIds.add(video.getVideoSourceId());
        });

        // 3、根据多个视频id删除多个视频
        if(videoIds.size()>0) {
            vodClient.deleteBatch(videoIds);
        }

        // 3、删除小节信息
        QueryWrapper<EduVideo> wrapper = new QueryWrapper<>();
        wrapper.eq("course_id", courseId);
        baseMapper.delete(wrapper);

    }

}
